<?php

namespace BlueBeetle\Press;

class Form extends Base {
	private static $instance = null;

	/**
	 * @var Cache|null
	 */
	var $cache = null;

	/**
	 * @var Config
	 */
	private $config;

	var $errors = array();

	var $jobs_table;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		$this->cache      = Cache::get_instance();
		$this->config     = Config::get_instance();
		$this->jobs_table = $this->db()->prefix . Press::TABLE_JOB;

		// Not used now since the Job application logic is moved Ajax/Job.php file.
		//add_action( 'get_header', array( $this, 'get_header' ) );
	}

	// Not used now since the Job application logic is moved Ajax/Job.php file.
	/* public function get_header() {
		$current_template = $this->get_common()->get_current_template();

		if ( $current_template != 'page-templates/job-landing-page.php' ) {
			return;
		}

		if ( $_SERVER['REQUEST_METHOD'] !== 'POST' ) {
			return;
		}

		$posted_job_id = filter_input( INPUT_POST, 'job_id', FILTER_VALIDATE_INT, array(
			"options" => array(
				"default" => 0,
			)
		) );

		if ( $posted_job_id == 0 ) {
			wp_redirect( get_permalink() );
			exit;
		}

		$post_full_name = filter_input( INPUT_POST, 'full_name', FILTER_SANITIZE_STRING );
		$post_email     = filter_input( INPUT_POST, 'email', FILTER_SANITIZE_STRING );

		if ( empty( $post_full_name ) ) {
			$this->errors[] = "Full name cannot be empty";
		}

		if ( empty( $post_email ) ) {
			$this->errors[] = "Email cannot be empty";
		}

		if ( empty( $_FILES['file']['tmp_name'] ) ) {
			$this->errors[] = "File cannot be empty";
		}

		if ( ! empty( $this->errors ) ) {
			return;
		}

		$allowed = array( "application/pdf" );
		if ( ! in_array( $_FILES['file']['type'], $allowed ) ) {
			$this->errors[] = "Only PDF document allowed";
		}

		if ( ! empty( $this->errors ) ) {
			return;
		}

		$file_id = Common::get_instance()->get_uuid();

		$filename         = 'resume/' . $posted_job_id . '/' . $file_id . '.pdf';
		$upload_file_name = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . $filename;
		$upload_folder    = dirname( $upload_file_name );
		if ( ! file_exists( $upload_folder ) ) {
			wp_mkdir_p( $upload_folder );
		}

		@move_uploaded_file( $_FILES['file']['tmp_name'], $upload_file_name );

		$data                 = array();
		$data['job_id']       = $posted_job_id;
		$data['full_name']    = $post_full_name;
		$data['email']        = $post_email;
		$data['resume']       = $filename;
		$data['lang_code']    = $this->get_lang_code();
		$data['date_created'] = current_time( 'mysql' );
		$this->db()->insert( $this->jobs_table, $data );

		$data['job_caption']   = get_field( 'caption', $posted_job_id );
		$data['uploaded_file'] = $upload_file_name;

		$this->send_email( $data );

		$redirect_url = get_the_permalink() . '?job_id=' . $posted_job_id . '&success=1';
		wp_redirect( $redirect_url );
		exit;
	}

	private function send_email( $data = array() ) {

		$email = Email::get_instance();


		if ( empty( $email->SMTP_host ) ) {
			return true;
		}

		$email->reset_last_error();

		$variables = array( 'full_name', 'job_caption', 'email' );

		//Send Email to Admin
		$admin_email_from_name = $this->config->get( 'job_admin_email_from_name' );
		$admin_delivery_email  = $this->config->get( 'job_admin_delivery_email' );

		$admin_email_subject = $this->config->get( 'job_admin_email_subject' );
		$admin_email_subject = $this->replace_variables( $admin_email_subject, $variables, $data );

		$admin_email_body = $this->config->get( 'job_admin_email_body' );
		$admin_email_body = $this->replace_variables( $admin_email_body, $variables, $data );

		$message = new \Swift_Message();
		if ( ! empty( $email->sender ) ) {
			$message->setSender( $email->sender, $admin_email_from_name );
			$message->setFrom( $email->sender, $admin_email_from_name );
		}
		if ( ! empty( $email->receiver ) ) {
			$message->setTo( $email->receiver );
		}

		foreach ( $admin_delivery_email as $email_info ) {
			$message->setTo( $email_info['email'], $email_info['name'] );
		}
		$message->setSubject( $admin_email_subject );
		$message->setBody( $admin_email_body, 'text/html' );
		if ( ! empty( $data['uploaded_file'] ) ) {
			$message->attach(
				\Swift_Attachment::fromPath( $data['uploaded_file'] )
			);
		}

		try {
			$email->getEmailClient()->send( $message );
		} catch ( \Exception $e ) {
			$email->last_error = $e->getMessage();

			return false;
		}

		//Send Email to User
		$user_email_from_name  = $this->config->get( 'job_user_email_from_name' );
		$user_email_from_email = $this->config->get( 'job_user_email_from_email' );
		$user_email_subject    = $this->config->get( 'job_user_email_subject' );
		$user_email_body       = $this->config->get( 'job_user_email_body' );

		$user_email_subject = $this->replace_variables( $user_email_subject, $variables, $data );
		$user_email_body    = $this->replace_variables( $user_email_body, $variables, $data );

		$message = new \Swift_Message();

		if ( ! empty( $email->sender ) ) {
			$message->setSender( $email->sender );
			if ( $email->sender == $user_email_from_email ) {
				$message->setFrom( $user_email_from_email, $user_email_from_name );
			} else {
				$message->setFrom( $email->sender, $user_email_from_name );
				$message->setReplyTo( $user_email_from_email, $user_email_from_name );
			}
		}

		if ( ! empty( $email->receiver ) ) {
			$message->setTo( $email->receiver );
		} else {
			$message->setTo( $data['email'], $data['full_name'] );
		}
		$message->setSubject( $user_email_subject );
		$message->setBody( $user_email_body, 'text/html' );

		try {
			$email->getEmailClient()->send( $message );
		} catch ( \Exception $e ) {
			$email->set_last_error( $e->getMessage() );

			return false;
		}

		return true;
	}

	private function replace_variables( $content = '', $variables = array(), $data = array() ) {
		if ( ! empty( $variables ) ) {
			foreach ( $variables as $variable ) {
				$replace_value = '';
				if ( isset( $data[ $variable ] ) ) {
					$replace_value = $data[ $variable ];
				}
				$content = str_replace( '[[' . $variable . ']]', $replace_value, $content );
			}
		}

		return $content;
	} */

	/**
	 * @return array
	 */
	public function get_errors() {
		return $this->errors;
	}


}

