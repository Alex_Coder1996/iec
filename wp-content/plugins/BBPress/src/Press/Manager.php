<?php

namespace BlueBeetle\Press;


use BlueBeetle\Press\Manager\ManagerBase;
use BlueBeetle\Press\Manager\ManagerTypeInterface;

class Manager extends Base
{
    private static $instance = null;

    private $module_name = null;
    private $module_action = null;
    private $is_ajax = false;

    private $registered_managers = array();
    private $registered_pages = array();

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        $this->registered_pages = array('toplevel_page_' . BBPRESS_MODULE);

        add_action('init', array($this, 'init'));
        add_action('current_screen', array($this, 'current_screen'));
        add_action('admin_menu', array($this, 'admin_menu'));

        add_action('admin_enqueue_scripts', array($this, 'register_scripts'));
        add_action('admin_enqueue_scripts', array($this, 'register_styles'));
    }

    function init()
    {
        $this->module_name = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_STRING);
        $this->module_action = filter_input(INPUT_GET, 'action', FILTER_SANITIZE_STRING, array("options" => array(
            "default" => 'index',
        )));
        $this->is_ajax = filter_input(INPUT_GET, 'is_ajax', FILTER_VALIDATE_BOOLEAN, array("options" => array(
            "default" => false,
        )));
        $this->module_name = strtolower($this->module_name);
        $this->module_action = strtolower($this->module_action);
        //init Managers
        $this->register_manager();
    }

    public function current_screen()
    {
        $screen = get_current_screen();
        if (!in_array($screen->id, $this->registered_pages)) {
            return;
        }
        if (!isset($this->registered_managers[$this->module_name])) {
            die('Module is not present: ' . $this->module_name);
        }
        /**
         * @var $class_instance ManagerBase
         */
        $class_instance = $this->registered_managers[$this->module_name];
        $class_instance->init_module($this->module_action);
        $capability = $class_instance->get_capability($this->module_action);
        if (!current_user_can($capability)) {
            wp_die('Security - Access Denied!');
        }
        $class_instance->plugin_hooks($this->module_action);
        if ($this->is_ajax) {
            $ajax_class_action = $this->module_action . '_ajax_action';
            if (!method_exists($class_instance, $ajax_class_action)) {
                die('Action is not present: ' . $ajax_class_action);
            }
            $class_instance->$ajax_class_action();
            exit();
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post_class_action = $this->module_action . '_post_action';
            if (method_exists($class_instance, $post_class_action)) {
                $class_instance->$post_class_action();
            }
        }
    }


    function admin_menu()
    {
        //Base Management Pages
        add_menu_page('BBPress', 'BBPress', 'edit_posts', BBPRESS_MODULE, array($this, 'route_manager'), $this->getIcon(), 81);

        if (!empty($this->registered_managers)) {
            /**
             * @var ManagerTypeInterface $class_instance ;
             */
            foreach ($this->registered_managers as $class_instance) {
                $module_slug = $class_instance->get_module_name();
                if ($module_slug != BBPRESS_MODULE) {
                    $this->registered_pages[] = 'bbpress_page_' . $module_slug;
                }
                add_submenu_page(BBPRESS_MODULE,
                    $class_instance->get_page_title(),
                    $class_instance->get_title(),
                    'edit_posts',
                    $module_slug,
                    array($this, 'route_manager'));
            }
        }
    }

    function route_manager()
    {
        /**
         * @var $class_instance ManagerBase
         */
        $class_instance = $this->registered_managers[$this->module_name];
        $class_action = $this->module_action . '_action';
        if (!method_exists($class_instance, $class_action)) {
            die('Action is not present: ' . $class_action);
        }
        $class_instance->create_menus($this->module_action);
        $class_instance->$class_action();
        $class_instance->render_view();
    }

    function register_manager()
    {
        $this->add_manager('Email\Email');
	    $this->add_manager('Contact\Contact');
        $this->add_manager('Cloudfront\Cloudfront');
        $this->add_manager('Settings\Settings');
    }

    function add_manager($class_name)
    {
        global $plugin_page;

        $class_with_ns = 'BlueBeetle\Press\Manager\\' . $class_name;

        if (!class_exists($class_with_ns)) {
            die('Class not found: ' . $class_with_ns);
        }

        /**
         * @var ManagerTypeInterface $class_with_ns ;
         * @var ManagerTypeInterface $class_instance ;
         */
        $class_instance = $class_with_ns::get_instance();
        $module_name = $class_instance->get_module_name();
        if (isset($this->registered_managers[$module_name])) {
            die('Module already present: ' . $module_name);
        }
        $class_instance->init_hooks($this->module_action);
        $this->registered_managers[$module_name] = $class_instance;
    }

    public function getIcon()
    {
        $iconData = file_get_contents(BBPRESS_ASSETS . 'images' . DIRECTORY_SEPARATOR . 'logo.svg');
        return 'data:image/svg+xml;base64,' . base64_encode($iconData);

    }

    public function register_scripts()
    {
        wp_enqueue_script('bbpress_manager_script', plugins_url('/assets/manager/script.js', BBPRESS_FILE), array(), BBPRESS_VERSION, true);
    }

    /**
     * Enqueue and register CSS files here.
     */
    public function register_styles()
    {
        wp_enqueue_style('bbpress_manager_styles', plugins_url('/assets/manager/styles.css', BBPRESS_FILE), array(), BBPRESS_VERSION);
    }
}

