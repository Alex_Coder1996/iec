<?php

namespace BlueBeetle\Press;


class Rewrite extends Base
{

    private static $instance = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {



        //Remove unwanted rewrite rules
        //$this->removeCoreRewriteRules();
    }

    //Remove rewrite rules
    public function removeCoreRewriteRules()
    {
        //Remove rules for posts
        add_filter('post_format_rewrite_rules', array($this, 'clear_rules'));
        add_filter('post_tag_rewrite_rules', array($this, 'clear_rules'));
        add_filter('category_rewrite_rules', array($this, 'clear_rules'));
        add_filter('post_rewrite_rules', array($this, 'clear_rules'));
        add_filter('date_rewrite_rules', array($this, 'clear_rules'));
        add_filter('root_rewrite_rules', array($this, 'clear_rules'));
        add_filter('search_rewrite_rules', array($this, 'clear_rules'));
        add_filter('author_rewrite_rules', array($this, 'clear_rules'));

    }

    //remove all rules passed and send fresh empty array
    public function clear_rules($rules)
    {
        return array();
    }

}

