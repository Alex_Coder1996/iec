<?php

namespace BlueBeetle\Press;


class Press extends Base
{

    private static $instance = null;

    private $plugin_path;

    private $plugin_url;

    const TABLE_COUNTRY = 'custom_countries';

    const TABLE_ENQUIRY = 'custom_enquiry_form';

    const TABLE_JOB = 'custom_jobs';

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {

        $this->plugin_path = plugin_dir_path(BBPRESS_FILE);
        $this->plugin_url = plugin_dir_url(BBPRESS_FILE);

        Config::get_instance();
        Common::get_instance();
        User::get_instance();
        Acf::get_instance();
        Ajax::get_instance();
        Rewrite::get_instance();
        Post::get_instance();
        Seo::get_instance();
        Theme::get_instance();
        // Not used now since the Job application logic is moved Ajax/Job.php file.
        //Form::get_instance();

        if (is_admin()) {
            Admin::get_instance();
            Manager::get_instance();
        }

        register_activation_hook(BBPRESS_FILE, array($this, 'activation'));
        register_deactivation_hook(BBPRESS_FILE, array($this, 'deactivation'));

        add_action('phpmailer_init', array($this, 'enable_smtp_wp_mail'));
    }


    public function enable_smtp_wp_mail($php_mailer)
    {
        Email::get_instance()->enable_smtp_wp_mail($php_mailer);
    }

    public function get_plugin_url()
    {
        return $this->plugin_url;
    }

    public function get_plugin_path()
    {
        return $this->plugin_path;
    }

    /**
     * Place code that runs at plugin activation here.
     */
    public function activation()
    {

    }

    /**
     * Place code that runs at plugin deactivation here.
     */
    public function deactivation()
    {

    }

}

