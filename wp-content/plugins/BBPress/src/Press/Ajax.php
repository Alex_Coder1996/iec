<?php

namespace BlueBeetle\Press;


class Ajax extends Base
{
    private static $instance = null;

    /**
     * @var null|string
     */
    private $class_name = null;

    /**
     * @var null|string
     */
    private $method_name = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        add_filter('rewrite_rules_array', array($this, 'rewrite_rules_array'));
        add_filter('query_vars', array($this, 'query_vars'));
        add_filter('parse_query', array($this, 'parse_query'));
    }

    public function get_ajax_url($class_name, $method_name)
    {
        $ajax_url = home_url(user_trailingslashit('bbpress_ajax/' . strtolower($class_name) . '/' . strtolower($method_name), 'bbpress_ajax'));
        return $ajax_url;
    }


    //Insert hotel related rewrite rules
    public function rewrite_rules_array($rules)
    {

        $new_rules = array();

        //Custom ajax handling rewrite.
        $new_rules['bbpress_ajax/(.+)/(.+)/?$'] = 'index.php?is_bbpress_ajax=1&bbpress_ajax_class=$matches[1]&bbpress_ajax_method=$matches[2]';

        return $new_rules + $rules;
    }

    //Add hotel related variables to query vars
    public function query_vars($query_variables)
    {
        //Ajax variables
        array_unshift($query_variables, 'is_bbpress_ajax', 'bbpress_ajax_class', 'bbpress_ajax_method');

        return $query_variables;
    }

    /**
     * @param \WP_Query $wp_query
     */
    public function parse_query($wp_query)
    {
        //This function is only targeted on the frontend main loop
        if (is_admin() || !$wp_query->is_main_query()) {
            return;
        }

        if (isset($wp_query->query_vars['is_bbpress_ajax'])) {
            $ajax_class = ucfirst(strtolower($wp_query->query_vars['bbpress_ajax_class']));
            $ajax_method = strtolower($wp_query->query_vars['bbpress_ajax_method']);

            $this->set_class_name($ajax_class);
            $this->set_method_name($ajax_method);
            $this->init_action();
        }

    }

    //Init Action action
    public function init_action()
    {
        if (empty($this->class_name) || empty($this->method_name)) {
            return;
        }

        add_action('pre_get_posts', array($this, 'pre_get_posts'), 1);
    }

    /**
     * @param \WP_Query $wp_query
     */
    public function pre_get_posts($wp_query)
    {
        //This function is only targeted on the frontend main loop
        if (is_admin() || !$wp_query->is_main_query()) {
            return;
        }

        $class_with_ns = '\BlueBeetle\Press\Ajax\\' . $this->class_name;

        if (!class_exists($class_with_ns)) {
            $wp_query->set_404();
            return;
        }

        /**
         * @var $class_instance Base
         */
        $class_instance = $class_with_ns::get_instance();

        if (!method_exists($class_instance, $this->method_name)) {
            $wp_query->set_404();
            return;
        }
        $return_value = call_user_func(array($class_instance, $this->method_name));

        if ($return_value === true) {
            http_response_code(200);
            header('Content-Type: application/json; charset=UTF-8;');
            echo json_encode($class_instance->get_ajax_data());
        } else {
            http_response_code(400);
            header('Content-Type: application/json; charset=UTF-8;');
            echo json_encode($class_instance->get_ajax_error());
        }
        exit(0);
    }

    /**
     * @param string $class_name
     */
    public function set_class_name($class_name)
    {
        $this->class_name = $class_name;
    }

    /**
     * @param string $method_name
     */
    public function set_method_name($method_name)
    {
        $this->method_name = $method_name;
    }
}

