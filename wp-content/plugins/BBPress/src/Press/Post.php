<?php

namespace BlueBeetle\Press;

class Post extends Base
{
    private static $instance = null;

    /**
     * @var Cache|null
     */
    var $cache = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        $this->cache = Cache::get_instance();

        add_action('init', array($this, 'register_custom_posts'), 0);

        add_action('wp_insert_post', array($this, 'save_filter_relations'), 99, 2);
    }

    public function register_custom_posts()
    {

        $this->register_custom_news();
        $this->register_custom_news_types();
        $this->register_custom_news_locations();
        $this->register_custom_partner();
        $this->register_custom_office();
        $this->register_custom_product();
        $this->register_custom_solution();
        $this->register_custom_job();
        $this->register_custom_press_release();
        $this->register_custom_pr_types();
    }

    public function save_filter_relations($post_ID, $post)
    {
        if ($post->post_type != 'product' && $post->post_type != 'solution') {
            return;
        }
        $this->cache->remove_cache('get_ps_filter_counts');
        $this->db()->delete('wp_custom_ps_filter', array('post_id' => $post_ID));

        $ps_filters = get_ps_filters();
        $lang_code = $this->get_lang_code();
        foreach ($ps_filters as $filter_name) {
            $field_name = 'ps_filter_' . $filter_name;
            $filter_values = get_field($field_name, $post_ID);
            if (!empty($filter_values)) {
                foreach ($filter_values as $value) {
                    $insert_data = array(
                        'post_id' => $post_ID,
                        'lang_code' => $lang_code,
                        'filter_name' => $filter_name . '_' . $value,
                        'filter_value' => '1',
                    );
                    $this->db()->insert('wp_custom_ps_filter', $insert_data);
                }
            }
        }
    }

    function register_custom_news()
    {

        $labels = array(
            "name" => __("News", "bbtheme"),
            "singular_name" => __("News", "bbtheme"),
        );

        $args = array(
            "label" => __("News", "bbtheme"),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "news", "with_front" => true),
            "query_var" => true,
            "menu_position" => 21,
            "menu_icon" => "dashicons-media-document",
            "supports" => array("title"),
        );

        register_post_type("news", $args);
    }

    function register_custom_news_types() {

        /**
         * Taxonomy: News Types.
         */

        $labels = array(
            "name" => __( "News Types", "bbtheme" ),
            "singular_name" => __( "News Type", "bbtheme" ),
        );

        $args = array(
            "label" => __( "News Types", "bbtheme" ),
            "labels" => $labels,
            "public" => true,
            "publicly_queryable" => false,
            "hierarchical" => false,
            "show_ui" => true,
            "show_in_menu" => true,
            "show_in_nav_menus" => false,
            "query_var" => false,
            "rewrite" => array( 'slug' => 'news_type', 'with_front' => false, ),
            "show_admin_column" => false,
            "show_in_rest" => false,
            "rest_base" => "news_type",
            "rest_controller_class" => "WP_REST_Terms_Controller",
            "show_in_quick_edit" => false,
        );
        register_taxonomy( "news_type", array( "news" ), $args );
    }

	function register_custom_news_locations() {

		/**
		 * Taxonomy: News Types.
		 */

		$labels = array(
			"name" => __( "News Locations", "bbtheme" ),
			"singular_name" => __( "News Location", "bbtheme" ),
		);

		$args = array(
			"label" => __( "News Locations", "bbtheme" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => false,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => false,
			"query_var" => false,
			"rewrite" => array( 'slug' => 'news_location', 'with_front' => false, ),
			"show_admin_column" => false,
			"show_in_rest" => false,
			"rest_base" => "news_location",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
		);
		register_taxonomy( "news_location", array( "news" ), $args );
	}


    function register_custom_partner()
    {

        /**
         * Post Type: Partners.
         */

        $labels = array(
            "name" => __("Partners", "bbtheme"),
            "singular_name" => __("Partner", "bbtheme"),
        );

        $args = array(
            "label" => __("Partners", "bbtheme"),
            "labels" => $labels,
            "description" => "",
            "public" => false,
            "publicly_queryable" => false,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => false,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => false,
            "exclude_from_search" => true,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => false,
            "query_var" => false,
            "menu_position" => 22,
            "menu_icon" => "dashicons-groups",
            "supports" => array("title"),
        );

        register_post_type("partner", $args);
    }


    function register_custom_office()
    {

        /**
         * Post Type: Offices.
         */

        $labels = array(
            "name" => __("Offices", "bbtheme"),
            "singular_name" => __("Office", "bbtheme"),
        );

        $args = array(
            "label" => __("Offices", "bbtheme"),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "offices", "with_front" => true),
            "query_var" => true,
            "menu_position" => 22,
            "menu_icon" => "dashicons-building",
            "supports" => array("title"),
        );

        register_post_type("office", $args);
    }

    function register_custom_product()
    {

        /**
         * Post Type: Solutions.
         */

        $labels = array(
            "name" => __("Prod & Accs", "bbtheme"),
            "singular_name" => __("Prod & Accs", "bbtheme"),
        );

        $args = array(
            "label" => __("Prod & Accs", "bbtheme"),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "product", "with_front" => true),
            "query_var" => true,
            "menu_position" => 23,
            "menu_icon" => "dashicons-feedback",
            "supports" => array("title"),
        );

        register_post_type("product", $args);
    }

    function register_custom_solution()
    {

        /**
         * Post Type: Solutions.
         */

        $labels = array(
            "name" => __("Solutions", "bbtheme"),
            "singular_name" => __("Solution", "bbtheme"),
        );

        $args = array(
            "label" => __("Solutions", "bbtheme"),
            "labels" => $labels,
            "description" => "",
            "public" => true,
            "publicly_queryable" => true,
            "show_ui" => true,
            "delete_with_user" => false,
            "show_in_rest" => true,
            "rest_base" => "",
            "rest_controller_class" => "WP_REST_Posts_Controller",
            "has_archive" => false,
            "show_in_menu" => true,
            "show_in_nav_menus" => true,
            "exclude_from_search" => false,
            "capability_type" => "post",
            "map_meta_cap" => true,
            "hierarchical" => false,
            "rewrite" => array("slug" => "solution", "with_front" => true),
            "query_var" => true,
            "menu_position" => 24,
            "menu_icon" => "dashicons-buddicons-topics",
            "supports" => array("title"),
        );

        register_post_type("solution", $args);
    }


	function register_custom_job()
	{

		/**
		 * Post Type: Jobs.
		 */

		$labels = array(
			"name" => __("Jobs", "bbtheme"),
			"singular_name" => __("Job", "bbtheme"),
		);

		$args = array(
			"label" => __("Jobs", "bbtheme"),
			"labels" => $labels,
			"description" => "",
			"public" => false,
			"publicly_queryable" => false,
			"show_ui" => true,
			"delete_with_user" => false,
			"show_in_rest" => false,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => false,
			"exclude_from_search" => false,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => false,
			"query_var" => false,
			"menu_position" => 25,
			"menu_icon" => "dashicons-text-page",
			"supports" => array("title"),
		);

		register_post_type("job", $args);
	}

	function register_custom_press_release()
	{

		/**
		 * Post Type: Jobs.
		 */

		$labels = array(
			"name" => __("Press Releases", "bbtheme"),
			"singular_name" => __("Press Release", "bbtheme"),
		);

		$args = array(
			"label" => __("Press Releases", "bbtheme"),
			"labels" => $labels,
			"description" => "",
			"public" => false,
			"publicly_queryable" => false,
			"show_ui" => true,
			"delete_with_user" => false,
			"show_in_rest" => false,
			"rest_base" => "",
			"rest_controller_class" => "WP_REST_Posts_Controller",
			"has_archive" => false,
			"show_in_menu" => true,
			"show_in_nav_menus" => false,
			"exclude_from_search" => true,
			"capability_type" => "post",
			"map_meta_cap" => true,
			"hierarchical" => false,
			"rewrite" => false,
			"query_var" => false,
			"menu_position" => 26,
			"menu_icon" => "dashicons-text-page",
			"supports" => array("title"),
		);

		register_post_type("press-release", $args);
	}

	function register_custom_pr_types() {

		/**
		 * Taxonomy: News Types.
		 */

		$labels = array(
			"name" => __( "Press Release Types", "bbtheme" ),
			"singular_name" => __( "Press Release Type", "bbtheme" ),
		);

		$args = array(
			"label" => __( "Press Release Types", "bbtheme" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => false,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => false,
			"query_var" => false,
			"rewrite" => array( 'slug' => 'pr_type', 'with_front' => false, ),
			"show_admin_column" => false,
			"show_in_rest" => false,
			"rest_base" => "news_type",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
		);
		register_taxonomy( "pr_type", array( "press-release" ), $args );
	}


}

