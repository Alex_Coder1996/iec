<?php

namespace BlueBeetle\Press;


class Admin extends Base
{
    private static $instance = null;

    var $show_hotel_switcher = false;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        //define('ALLOW_UNFILTERED_UPLOADS', true);
        //add_action('upload_mimes', array($this, 'cc_mime_types'));

        add_filter('use_block_editor_for_post_type', 'use_block_editor_for_post_type', 10, 2);
        add_action('admin_menu', array($this, 'post_remove'));

    }

    /* function cc_mime_types($mimes)
    {
        $mimes['svg'] = 'image/svg+xml';
        $mimes['svgz'] = 'image/svg+xml';
        return $mimes;
    } */


    function use_block_editor_for_post_type($is_enabled, $post_type)
    {
        return false;
    }

    function post_remove()
    {
        remove_menu_page('edit.php');
        remove_menu_page( 'edit-comments.php' );

        remove_post_type_support( 'post', 'comments' );
        remove_post_type_support( 'page', 'comments' );
    }


}

