<?php

namespace BlueBeetle\Press;

use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class Common extends Base
{
    private static $instance = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        add_filter('esc_html', array($this, 'esc_html'), 10, 2);
    }

    

    public function esc_html($safe_text, $text)
    {
        $safe_text = str_replace('¢','&cent;',$safe_text);
        $safe_text = str_replace('£','&pound;',$safe_text);
        $safe_text = str_replace('¥','&yen;',$safe_text);
        $safe_text = str_replace('€','&euro;',$safe_text);
        $safe_text = str_replace('©','&copy;',$safe_text);
        $safe_text = str_replace('®','&reg;',$safe_text);
        return $safe_text;
    }

    public function get_uuid()
    {
        try {
            $uuid4 = Uuid::uuid4();
            return $uuid4->toString();
        } catch (UnsatisfiedDependencyException $e) {
            return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
        }
    }

    public function get_current_template()
    {
        global $template;
        $template_directory = get_template_directory() . DIRECTORY_SEPARATOR;
        $current_template = str_replace($template_directory, '', $template);
        return $current_template;
    }

    public function get_countries($lang_code = null, $only_countries = array())
    {
        if (is_null($lang_code)) {
            $lang_code = $this->get_lang_code();
        }
        $country_table_name = $this->db()->prefix . Press::TABLE_COUNTRY;
        $where = " lang_code='{$lang_code}' ";
        if (!empty($only_countries)) {
            $where .= " AND country_code IN ('" . implode("','", $only_countries) . "') ";
        }
        $query = "SELECT * FROM {$country_table_name} WHERE {$where} ORDER BY country_name ASC;";
        $countries = $this->db()->get_results($query, ARRAY_A);
        if (empty($countries)) {
            return array();
        }
        return $countries;
    }

    static public function nl2p($str)
    {
        $str = str_replace(array("\r\n", "\r"), "\n", $str);
        return "<p>\n" . str_replace("\n", "\n</p>\n<p>\n", $str) . "\n</p>";
    }

    static public function extract_value($data, $path = null)
    {
        if (is_object($data)) {
            $data = get_object_vars($data);
        }
        if (!is_array($path)) {
            if (strpos($path, '/') !== 0 && strpos($path, './') === false) {
                $path = explode('.', $path);
            }
        }
        foreach ($path as $i => $key) {
            if (is_numeric($key) && intval($key) > 0 || $key == '0') {
                if (isset ($data [intval($key)])) {
                    $data = $data [intval($key)];
                } else {
                    return null;
                }
            } elseif ($key == '{n}') {
                foreach ($data as $j => $val) {
                    if (is_int($j)) {
                        $tmpPath = array_slice($path, $i + 1);
                        if (empty ($tmpPath)) {
                            $tmp [] = $val;
                        } else {
                            $tmp [] = self::extract_value($val, $tmpPath);
                        }
                    }
                }
                return $tmp;
            } else {
                if (isset ($data [$key])) {
                    $data = $data [$key];
                } else {
                    return null;
                }
            }
        }
        return $data;
    }

    public function sort_post_by_sort_order($posts, $custom_field_name = 'sort_order', $sort_by_order = SORT_ASC, $order_by_type = SORT_NUMERIC)
    {
        if (empty($posts) || !is_array($posts)) {
            return array();
        }

        $sort = array();
        /**
         * @var $post \WP_Post
         */
        foreach ($posts as $post) {
            $sort_order = get_field($custom_field_name, $post->ID);
            if ($sort_order) {
                $sort[] = $sort_order;
            } else {
                $sort[] = 0;
            }
        }
        array_multisort($sort, $sort_by_order, $order_by_type, $posts);
        return $posts;

    }

    public function sort_terms_by_sort_order($terms, $custom_field_name = 'sort_order', $sort_by_order = SORT_ASC, $order_by_type = SORT_NUMERIC)
    {
        if (empty($terms) || !is_array($terms)) {
            return array();
        }

        $sort = array();
        /**
         * @var $term \WP_Term
         */
        foreach ($terms as $term) {
            $sort_order = get_field($custom_field_name, $term->taxonomy . '_' . $term->term_id);
            if ($sort_order) {
                $sort[] = $sort_order;
            } else {
                $sort[] = 0;
            }
        }
        array_multisort($sort, $sort_by_order, $order_by_type, $terms);
        return $terms;

    }

}

