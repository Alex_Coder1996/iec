<?php

acf_add_local_field_group(array(
    'key' => 'group_product_and_solution_filter',
    'title' => 'Prod & Accs Filters',
    'fields' => array(),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'product',
            ),
        ),
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'solution',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

$ps_filters = get_ps_filter_items();
foreach ($ps_filters as $ps_filter_key => $ps_filter_value) {

    $field = array(
        'key' => 'field_ps_filter_' . $ps_filter_key,
        'label' => $ps_filter_value['caption'],
        'name' => 'ps_filter_' . $ps_filter_key,
        'type' => 'checkbox',
        'instructions' => '',
        'required' => (int)$ps_filter_value['required'],
        'conditional_logic' => 0,
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
        ),
        'choices' => array(),
        'allow_custom' => 0,
        'default_value' => array(),
        'layout' => 'horizontal',
        'toggle' => 0,
        'return_format' => 'value',
        'save_custom' => 0,
        'parent' => 'group_product_and_solution_filter'
    );

    foreach ($ps_filter_value['choices'] as $choice) {
        $field['choices'][$choice['key']] = $choice['caption'];
    }

    acf_add_local_field($field);
}


