<?php

namespace BlueBeetle\Press;

class Generic extends Base {
	private static $instance = null;

	/**
	 * @var Cache|null
	 */
	var $cache = null;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		$this->cache = Cache::get_instance();

		add_action( 'save_post', array( $this, 'create_zip_archive' ), 99, 2 );
	}

	/**
	 * @param int $post_ID
	 * @param \WP_Post $post
	 */
	function create_zip_archive( $post_ID, $post ) {
		if ( $post->post_type != 'press-release' ) {
			return;
		}
		$files_for_download = get_field( 'files_for_download', $post_ID );

		$filename      = $this->get_pr_zip_filename( $post );
		$zip_file_name = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . $filename;
		$upload_folder = dirname( $zip_file_name );
		if ( ! file_exists( $upload_folder ) ) {
			wp_mkdir_p( $upload_folder );
		}
		if ( file_exists( $zip_file_name ) ) {
			wp_delete_file( $zip_file_name );
		}
		if ( empty( $files_for_download ) ) {
			return;
		}
		$zipFile = new \PhpZip\ZipFile();

		try {
			foreach ( $files_for_download as $file_index => $file ) {
				$dir_info    = wp_upload_dir( $file['file']['date'] );
				$file_to_add = $dir_info['path'] . DIRECTORY_SEPARATOR . $file['file']['filename'];
				if ( file_exists( $file_to_add ) ) {
					$zipFile->addFile( $file_to_add, str_pad( $file_index + 1, 3, '0', STR_PAD_LEFT ) . '_' . $file['file']['filename'] );
				}
			}

			$zipFile->saveAsFile( $zip_file_name );
		} catch ( \Exception $e ) {
			echo 'Caught exception: ', $e->getMessage(), "\n";
			die();
		}
		$zipFile->close();
	}

	/**
	 * @param \WP_Post $post
	 *
	 * @return string
	 */
	public function get_pr_zip_filename( $post ) {
		$filename = 'press_releases/pr-' . $post->ID . '-' . $post->post_name . '.zip';

		return $filename;
	}

	/**
	 * @param \WP_Post $post
	 *
	 * @return string
	 */
	public function get_pr_zip_url( $post ) {
		$filename      = $this->get_pr_zip_filename( $post );
		$zip_file_name = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . $filename;
		if ( ! file_exists( $zip_file_name ) ) {
			return '';
		}

		return content_url( $filename );
	}

	public function get_homepage_news() {
		$query                     = array();
		$query['post_type']        = 'news';
		$query['posts_per_page']   = '3';
		$query['suppress_filters'] = false;

		$posts = get_posts( $query );

		return $posts;
	}


	/**
	 * @param \WP_Term $news_location_term
	 *
	 * @return array|int[]|\WP_Post[]
	 */
	public function get_regional_news( $news_location_term = null ) {
		if ( empty( $news_location_term ) ) {
			return [];
		}
		$query                     = array();
		$query['post_type']        = 'news';
		$query['posts_per_page']   = '20';

		$query['tax_query'] = array(
			array(
				'taxonomy' => $news_location_term->taxonomy,
				'field'    => 'id',
				'terms'    => [$news_location_term->term_id]
			)
		);
		//$query['suppress_filters'] = false;
		$query['sort_column']          = 'menu_order';
		$query['order']            = 'DESC';

		$posts = get_posts( $query );

		return $posts;
	}

	public function get_taxonomy_news_type_terms() {
		$terms = get_terms( [
			'taxonomy'   => 'news_type',
			'hide_empty' => false,
		] );

		return $this->get_common()->sort_terms_by_sort_order( $terms );
	}

	public function get_taxonomy_pr_type_terms() {
		$terms = get_terms( [
			'taxonomy'   => 'pr_type',
			'hide_empty' => false,
		] );

		return $this->get_common()->sort_terms_by_sort_order( $terms );
	}

	public function get_offices() {
		global $sitepress;
		$curr_lang_code = ICL_LANGUAGE_CODE;
		$lang='en';
		$sitepress->switch_lang($lang);

		$query                     = array();
		$query['post_type']        = 'office';
		$query['posts_per_page']   = '999';
		$query['suppress_filters'] = false;
		$query['orderby']          = 'menu_order';
		$query['order']            = 'ASC';

		$posts = get_posts( $query );

		return $posts;
	}

	public function get_jobs() {
		$query                     = array();
		$query['post_type']        = 'job';
		$query['posts_per_page']   = '999';
		$query['suppress_filters'] = false;

		$posts = get_posts( $query );

		return $posts;
	}

	public function get_press_releases() {
		$query                     = array();
		$query['post_type']        = 'press-release';
		$query['posts_per_page']   = '999';
		$query['suppress_filters'] = false;

		$posts = get_posts( $query );

		return $posts;
	}

	public function get_banner_links( $links ) {
		$ret_val = [];
		if ( empty( $links ) ) {
			return $ret_val;
		}

		foreach ( $links as $link ) {
			$caption = $link['caption'];
			if ( empty( $caption ) ) {
				$caption = get_field( 'caption', $link['page']->ID );
			}
			$link      = [
				'icon'    => $link['icon'],
				'caption' => $caption,
				'active'  => ( $link['is_active'] ) ? 1 : 0,
				'url'     => get_the_permalink( $link['page']->ID )
			];
			$ret_val[] = $link;
		}

		return $ret_val;
	}

	public function get_page_children_links( $page = null ) {
		$links = [];
		if ( empty( $page ) ) {
			return $links;
		}

		$query                     = array();
		$query['post_type']        = 'page';
		$query['posts_per_page']   = '999';
		$query['post_parent']      = $page->ID;
		$query['suppress_filters'] = false;
		$query['orderby']          = 'menu_order';
		$query['order']            = 'ASC';
		$posts                     = get_posts( $query );

		if ( ! empty( $posts ) ) {
			$current_page_id = get_queried_object_id();
			foreach ( $posts as $post ) {
				$caption = get_field( 'caption', $post->ID );
				$link    = [
					'caption' => $caption,
					'active'  => ( $current_page_id == $post->ID ) ? 1 : 0,
					'url'     => get_the_permalink( $post->ID )
				];
				$links[] = $link;
			}
		}

		return $links;
	}

	public function get_ps_filter_counts() {
		$cache_count = $this->cache->get_cache( 'get_ps_filter_counts' );
		if ( ! is_null( $cache_count ) ) {
			return $cache_count;
		}

		$return_val = [];
		$lang_code  = $this->get_lang_code();
		$query      = "SELECT count(*) as row_count,filter_name FROM wp_custom_ps_filter WHERE lang_code='{$lang_code}' GROUP BY filter_name;";
		$row_counts = $this->db()->get_results( $query, ARRAY_A );
		if ( ! empty( $row_counts ) ) {
			foreach ( $row_counts as $row ) {
				$return_val[ $row['filter_name'] ] = $row['row_count'];
			}
		}

		$this->cache->write_cache( 'get_ps_filter_counts', $return_val, 60 * 60 );

		return $return_val;
	}

}

