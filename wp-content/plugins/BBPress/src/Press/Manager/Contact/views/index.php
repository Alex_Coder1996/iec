<br class="clear">
<form action="<?php echo $module->generate_link('delete_entries', false); ?>" method="POST">
    <div class="wp-clearfix">
        <input type="submit" value="Delete Entries" style="float: right;">
    </div>
    <table id="contacts_table" class="display">
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Type</th>
            <th>Full Name</th>
            <th>Company</th>
            <th>Email</th>
            <th>Message</th>
            <th>Country</th>
            <th>Location</th>
            <th>Dated</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</form>

<script language="JavaScript">
    var myTable = '';
    function format ( d ) {
        return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
            '<tr>'+
            '<td>Message:</td>'+
            '<td>'+d.message+'</td>'+
            '</tr>'+
            '</table>';
    }
    jQuery(function () {
        var contactTable = jQuery('#contacts_table').DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            searching: true,
            pageLength: 10,
            paging: true,
            ajax: {
                url: "<?php echo $module->generate_link('index', true); ?>",
                type: 'GET',
                global: true,
                complete: function (e, d) {
                },
                error: function (xhr, error, thrown) {
                }
            },
            columnDefs: [
                {"className": "dt-left", "targets": 0, "orderable": false},
                {"className": "dt-left", "targets": 1, "orderable": false},
                {"className": "dt-left", "targets": 2, "orderable": false},
                {"className": "dt-left", "targets": 3, "orderable": false},
                {"className": "dt-left", "targets": 4, "orderable": false},
                {"className": "dt-left", "targets": 5, "orderable": false},
                {"className": "dt-left mDetail", "targets": 6, "orderable": false},
                {"className": "dt-left", "targets": 7, "orderable": false},
                {"className": "dt-left", "targets": 8, "orderable": false},
                {"className": "dt-left", "targets": 9, "orderable": false},
            ],
            columns: [
                {data: "select"},
                {data: "id"},
                {data: "form_type"},
                {data: "full_name"},
                {data: "company"},
                {data: "email"},
                {data: "message_short"},
                {data: "country"},
                {data: "location"},
                {data: "dated"},
            ],
        });

        jQuery('#contacts_table tbody').on('click', 'td.mDetail', function () {
            var tr = jQuery(this).closest('tr');
            var row = contactTable.row( tr );
            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( format(row.data()) ).show();
                tr.addClass('shown');
            }
        } );
    });
</script>
<style>
    td.mDetail {
        cursor: pointer;
    }
</style>
