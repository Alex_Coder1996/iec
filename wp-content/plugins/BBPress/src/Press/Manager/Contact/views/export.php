<br class="clear">

<ol>
    <?php foreach ($links as $link): ?>
        <li><a href="<?php echo $link['link']; ?>"><?php echo $link['caption']; ?></a></li>
    <?php endforeach; ?>
</ol>
