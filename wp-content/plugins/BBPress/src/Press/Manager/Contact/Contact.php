<?php

namespace BlueBeetle\Press\Manager\Contact;


use BlueBeetle\Press\Manager\ManagerBase;
use BlueBeetle\Press\Manager\ManagerTypeInterface;
use BlueBeetle\Press\Press;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Contact extends ManagerBase implements ManagerTypeInterface
{
    private static $instance = null;

    private $table_name = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct()
    {
        $this->table_name = $this->db()->prefix . Press::TABLE_ENQUIRY;
    }


    public function create_menus($action)
    {
        $this->add_tab_menu('List', 'index');
        $this->add_tab_menu('Export', 'export');
    }

    public function init_hooks($action)
    {

    }

    public function get_capability($action)
    {
        return parent::get_capability($action);
    }

    public function plugin_hooks($action)
    {
        $this->set_page_caption('Contact');
        if ($action == 'index') {
            wp_enqueue_script('bbpress_datatable_script',
                plugins_url('/assets/lib/DataTables/datatables.min.js', BBPRESS_FILE), array(), '1.10.18', true);
            wp_enqueue_style('bbpress_datatable_styles',
                plugins_url('/assets/lib/DataTables/datatables.min.css', BBPRESS_FILE), array(), '1.10.18');
        }
    }

    function index_action()
    {
        //show index
    }

    function index_ajax_action()
    {

        $draw = filter_input(INPUT_GET, 'draw', FILTER_SANITIZE_NUMBER_INT);
        $start = filter_input(INPUT_GET, 'start', FILTER_SANITIZE_NUMBER_INT);
        $length = filter_input(INPUT_GET, 'length', FILTER_SANITIZE_NUMBER_INT);
        $search   = filter_input(INPUT_GET, 'search', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        if ($length < 0) {
            $length = 10;
        }

        $searchQuery = '';
        if (isset($search['value']) && !empty($search['value'])) {
            $searchText = $search['value'];
            $searchQuery = " WHERE form_type LIKE '%{$searchText}%' ";
            $searchQuery .= " OR full_name LIKE '%{$searchText}%' ";
            $searchQuery .= " OR company LIKE '%{$searchText}%' ";
            $searchQuery .= " OR email LIKE '%{$searchText}%' ";
            $searchQuery .= " OR country LIKE '%{$searchText}%' ";
            $searchQuery .= " OR interest LIKE '%{$searchText}%' ";
            $searchQuery .= " OR message LIKE '%{$searchText}%' ";
            $searchQuery .= " OR location LIKE '%{$searchText}%' ";
        }


        $query = "SELECT count(*) FROM {$this->table_name} {$searchQuery};";
        $user_count = $this->db()->get_var($query);


        $query = "SELECT * FROM {$this->table_name} {$searchQuery} ORDER BY date_created DESC  LIMIT {$start},{$length};";
        $user_list = $this->db()->get_results($query, ARRAY_A);

        $list = array();
        if (!empty($user_list)) {
            foreach ($user_list as $user) {
                $list[] = array(
                    'select' => "<input type='checkbox' name='select[]' value='" . $user['id'] . "'/>",
                    'id' => $user['id'],
                    'form_type' => $user['form_type'],
                    'full_name' => $user['full_name'],
                    'company' => $user['company'],
                    'email' => $user['email'],
                    'country' => $user['country'],
                    'interest' => $user['interest'],
                    'message_short' => mb_strimwidth($user['message'], 0, 25, '...'),
                    'message' => $user['message'],
                    'page_url' => $user['page_url'],
                    'referrer_url' => $user['referrer_url'],
                    'location' => empty($user['location']) ? 'global' : $user['location'],
                    'lang_code' => $user['lang_code'],
                    'dated' => $user['date_created']
                );
            }
        }
        $data = array(
            "draw" => $draw,
            "recordsFiltered" => $user_count,
            "recordsTotal" => $user_count,
            "data" => $list
        );
        $this->send_ajax_data($data);
    }


    public function export_action()
    {
        $export_links = array();
        $export_links[] = [
            'caption' => 'Export All Contact Data',
            'link' => $this->generate_link('export', true)
        ];
        $export_links[] = [
            'caption' => 'Export Only Contact Page',
            'link' => $this->generate_link('export', true, ['op' => '1'])
        ];
        $export_links[] = [
            'caption' => 'Export Only Market Page',
            'link' => $this->generate_link('export', true, ['op' => '2'])
        ];
        $export_links[] = [
            'caption' => 'Export Only Office Page',
            'link' => $this->generate_link('export', true, ['op' => '3'])
        ];
        $export_links[] = [
            'caption' => 'Export Only Partner Page',
            'link' => $this->generate_link('export', true, ['op' => '4'])
        ];

        $this->assign('links', $export_links);

    }

    function export_ajax_action()
    {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Form Type');
        $sheet->setCellValue('B1', 'Page URL');
        $sheet->setCellValue('C1', 'Referrer URL');
        $sheet->setCellValue('D1', 'Language');
        $sheet->setCellValue('E1', 'Full Name');
        $sheet->setCellValue('F1', 'Company');
        $sheet->setCellValue('G1', 'Email');
        $sheet->setCellValue('H1', 'Country');
        $sheet->setCellValue('I1', 'Interest');
        $sheet->setCellValue('J1', 'Message');
        $sheet->setCellValue('K1', 'Location');
        $sheet->setCellValue('L1', 'Dated');

        $sheet->getStyle('A1:' . $sheet->getHighestColumn() . '1')->getFont()->setBold(true);
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setWidth(100);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);

        $query_where = " 1 = 1 ";

        $op = filter_input(INPUT_GET, 'op', FILTER_SANITIZE_NUMBER_INT, array(
            "options" => array(
                "default" => 0,
            )
        ));

        $file_name = 'all_data';
        if ($op == 1) {
            $query_where .= " AND form_type ='contact' ";
            $file_name = 'only_contact';
        } else {
            if ($op == 2) {
                $query_where .= " AND form_type ='market-enquiry' ";
                $file_name = 'only_market';
            } else {
                if ($op == 3) {
                    $query_where .= " AND form_type ='office-enquiry' ";
                    $file_name = 'only_office';
                } else {
                    if ($op == 4) {
                        $query_where .= " AND form_type ='become-partner' ";
                        $file_name = 'only_partner';
                    }
                }
            }
        }

        $start = 0;
        $row_number = 2;
        do {
            $query = "SELECT * FROM {$this->table_name} WHERE {$query_where} ORDER BY date_created LIMIT {$start},100;";
            $records = $this->db()->get_results($query, ARRAY_A);
            if (!empty($records)) {
                foreach ($records as $record) {
                    $sheet->setCellValue('A' . $row_number, html_entity_decode($record['form_type']));
                    $sheet->setCellValue('B' . $row_number, html_entity_decode($record['page_url']));
                    $sheet->setCellValue('C' . $row_number, html_entity_decode($record['referrer_url']));
                    $sheet->setCellValue('D' . $row_number, html_entity_decode($record['lang_code']));
                    $sheet->setCellValue('E' . $row_number, html_entity_decode($record['full_name']));
                    $sheet->setCellValue('F' . $row_number, html_entity_decode($record['company']));
                    $sheet->setCellValue('G' . $row_number, html_entity_decode($record['email']));
                    $sheet->setCellValue('H' . $row_number, html_entity_decode($record['country']));
                    $sheet->setCellValue('I' . $row_number, html_entity_decode($record['interest']));
                    $message = html_entity_decode($record['message']);
                    $message = str_replace('&#39;', "'", $message);
                    $sheet->setCellValue('J' . $row_number, $message);
                    $sheet->setCellValue('K' . $row_number, html_entity_decode($record['location']));
                    $sheet->setCellValue('L' . $row_number, html_entity_decode($record['date_created']));

                    $row_number++;
                }
            }
            $start = $start + 100;
        } while (!empty($records));

        $export_file_name = date('Ymd') . '_contact_' . $file_name . '.xlsx';
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="' . $export_file_name . '"');
        header('Cache-Control: max-age=0');
        $writer->save("php://output");
    }

    function delete_entries_action()
    {
        $deleteIds = $_POST['select'];
        if (isset($deleteIds) && count($deleteIds) > 0) {
            $deleteIdsString = implode(',', $deleteIds);
            $query = "Delete FROM {$this->table_name} where id in(" . $deleteIdsString . ");";
            $this->db()->query($query);
        }
        echo '<script>window.location="' . $this->generate_link('index', false) . '"</script>';
    }


    /**
     * @return string
     */
    public function get_title()
    {
        return 'Contact';
    }

    /**
     * @return string
     */
    public function get_page_title()
    {
        return 'Contact';
    }

    /**
     * @return string
     */
    public function get_name()
    {
        return 'contact';
    }

}


