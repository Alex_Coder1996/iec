<?php

namespace BlueBeetle\Press\Manager\Cloudfront;


use Aws\Api\DateTimeResult;
use BlueBeetle\Press\Config;
use BlueBeetle\Press\Manager\ManagerBase;
use BlueBeetle\Press\Manager\ManagerTypeInterface;
use Aws\CloudFront\CloudFrontClient;
use Aws\Exception\AwsException;
use Aws\Credentials\Credentials;

class Cloudfront extends ManagerBase implements ManagerTypeInterface
{
    private static $instance = null;

    private $aws_key = null;
    private $aws_secret_key = null;
    private $aws_distribution_id = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function create_menus($action)
    {
        $this->add_tab_menu('Invalidation', 'index');
        $this->add_tab_menu('Create', 'create');
        $this->add_tab_menu('Amazon Settings', 'settings', 'manage_options');
    }

    public function init_hooks($action)
    {

    }

    public function get_capability($action)
    {
        if ($action == 'settings') {
            return 'manage_options';
        }
        return parent::get_capability($action);
    }

    public function plugin_hooks($action)
    {

        $config = Config::get_instance();
        $this->aws_key = $config->get('bbpress_cloudfront_access_key');
        $this->aws_secret_key = $config->get('bbpress_cloudfront_access_secret_key');
        $this->aws_distribution_id = $config->get('bbpress_cloudfront_distribution_id');

        $this->set_page_caption('Amazon Cloudfront');
        if ($action == 'index') {
            wp_enqueue_script('bbpress_datatable_script', plugins_url('/assets/lib/DataTables/datatables.min.js', BBPRESS_FILE), array(), '1.10.18', true);
            wp_enqueue_style('bbpress_datatable_styles', plugins_url('/assets/lib/DataTables/datatables.min.css', BBPRESS_FILE), array(), '1.10.18');
        } elseif ($action == 'settings') {
            include_once 'includes/settings-form.php';
            acf_form_head();
        }
    }

    function index_action()
    {
        //show index
    }

    function index_ajax_action()
    {
        if (is_null($this->aws_key) || is_null($this->aws_secret_key) || is_null($this->aws_distribution_id)) {
            $data = array(
                "draw" => "1",
                "recordsFiltered" => "0",
                "recordsTotal" => "0",
                "data" => array()
            );
            $this->send_ajax_data($data);
        }

        $credentials = new Credentials($this->aws_key, $this->aws_secret_key);

        $client = new CloudFrontClient([
            'version' => 'latest',
            'region' => 'us-east-2',
            'credentials' => $credentials
        ]);

        try {
            $result = $client->listInvalidations([
                'DistributionId' => $this->aws_distribution_id,
                'MaxItems' => 10,
            ]);
            $items = $result['InvalidationList']['Items'];
            $records = array();
            foreach ($items as $item) {
                $record = array(
                    'id' => $item['Id'],
                    'create_time' => (string)$item['CreateTime'],
                    'status' => $item['Status'],
                );
                $records[] = $record;
            }
            $data = array(
                "draw" => "1",
                "recordsFiltered" => "10",
                "recordsTotal" => "10",
                "data" => $records
            );
            $this->send_ajax_data($data);
        } catch (AwsException $e) {
            $data['error'] = $e->getMessage();
            $this->send_ajax_data($data, true);
        }
    }

    function create_action()
    {
        $this->assign('post_url', $this->generate_link('create'));
        $this->assign('invalidation_reference', md5(time()));

    }

    function create_post_action()
    {
        if (is_null($this->aws_key) || is_null($this->aws_secret_key) || is_null($this->aws_distribution_id)) {
            $this->add_error_message('AWS not configured');
            return;
        }

        $invalidation_path = filter_input(INPUT_POST, 'invalidation_path', FILTER_SANITIZE_STRING);
        $invalidation_reference = filter_input(INPUT_POST, 'invalidation_reference', FILTER_SANITIZE_STRING);

        if (empty($invalidation_path) || empty($invalidation_reference)) {
            $this->add_error_message('Invalid Data Submitted');
            return;
        }


        $credentials = new Credentials($this->aws_key, $this->aws_secret_key);

        $client = new CloudFrontClient([
            'version' => 'latest',
            'region' => 'us-east-2',
            'credentials' => $credentials
        ]);

        try {
            $result = $client->createInvalidation([
                'DistributionId' => $this->aws_distribution_id,
                'InvalidationBatch' => [
                    'CallerReference' => $invalidation_reference,
                    'Paths' => [
                        'Items' => [$invalidation_path],
                        'Quantity' => 1,
                    ],
                ]
            ]);
            $redirect_url = $this->generate_link('index');
            wp_redirect($redirect_url);
            exit;
        } catch (AwsException $e) {
            $this->add_error_message($e->getMessage());
            return;
        }
    }

    function settings_action()
    {
        $form_settings = array(
            'post_id' => 'option',
            'field_groups' => array('group_bb_press_cloudfront_settings'),
            'form' => true,
            'submit_value' => 'Save Settings',
            'updated_message' => 'Saved!'
        );
        $this->assign('form_settings', $form_settings);
    }

    /**
     * @return string
     */
    public function get_title()
    {
        return 'Cloudfront';
    }

    /**
     * @return string
     */
    public function get_page_title()
    {
        return 'Cloudfront';
    }

    /**
     * @return string
     */
    public function get_name()
    {
        return 'cloudfront';
    }

}

