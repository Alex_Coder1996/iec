<form method="post" id="cloudfront_invalidation_form" action="<?php echo $post_url; ?>" novalidate="novalidate">
    <table class="form-table">
        <tbody>
        <tr>
            <th scope="row"><label for="test_email_id">Invalidation Path</label></th>
            <td>
                <input type="text" style="width: 500px;" name="invalidation_path" placeholder="/*" value="/wp-content/themes/bbtheme/*" />
            </td>
        </tr>

        </tbody>
    </table>
    <input type="hidden" name="invalidation_reference" value="<?php echo $invalidation_reference; ?>" />
    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Create Invalidation Request">
    </p>
</form>
