<br class="clear">
<table id="invalidation_table" class="display">
    <thead>
    <tr>
        <th>ID</th>
        <th>CreateTime</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>


<script language="JavaScript">
    jQuery(function () {
        jQuery('#invalidation_table').DataTable({
            ordering: false,
            processing: true,
            serverSide: true,
            searching: false,
            pageLength: 10,
            paging: false,
            ajax: {
                url: "<?php echo $module->generate_link('index', true); ?>",
                type: 'GET',
                global: true,
                complete: function (e, d) {

                },
                error: function (xhr, error, thrown) {
                    alert('Check your AWS settings!');
                }
            },
            columnDefs: [
                {"width": "30%", "className": "dt-left", "targets": 0, "orderable": false},
                {"width": "30%", "className": "dt-left", "targets": 1, "orderable": false},
                {"width": "40%", "className": "dt-left", "targets": 2, "orderable": false},
            ],
            columns: [
                {data: "id"},
                {data: "create_time"},
                {data: "status"},
            ],
        });
    });
</script>
