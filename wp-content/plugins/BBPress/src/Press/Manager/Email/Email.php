<?php

namespace BlueBeetle\Press\Manager\Email;


use BlueBeetle\Press\Manager\ManagerBase;
use BlueBeetle\Press\Manager\ManagerTypeInterface;

class Email extends ManagerBase implements ManagerTypeInterface
{
    private static $instance = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function create_menus($action)
    {
        $this->add_tab_menu('Test SMTP', 'index');
        $this->add_tab_menu('SMTP Settings', 'settings', 'manage_options');
    }

    public function init_hooks($action)
    {

    }

    public function get_capability($action)
    {
        if ($action == 'settings') {
            return 'manage_options';
        }
        return parent::get_capability($action);
    }

    public function plugin_hooks($action)
    {
        $this->set_page_caption('BBPress Email');
        if ($action == 'settings') {
            include_once 'includes/settings-form.php';
            acf_form_head();
        }
    }

    function index_action()
    {
        $admin_email = $this->get_current_user()->user_email;
        $this->assign('admin_email', $admin_email);
    }

    function index_ajax_action()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['email'])) {
            $admin_email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

            if (is_email($admin_email)) {
                $debug_data = \BlueBeetle\Press\Email::get_instance()->sent_test_email($admin_email);
                echo $debug_data;
            } else {
                echo 'Invalid email!';
            }
        }

    }

    function settings_action()
    {
        $form_settings = array(
            'post_id' => 'option',
            'field_groups' => array('group_bb_press_email_settings'),
            'form' => true,
            'submit_value' => 'Save Settings',
            'updated_message' => 'Saved!'
        );
        $this->assign('form_settings', $form_settings);
    }

    /**
     * @return string
     */
    public function get_title()
    {
        return 'Email';
    }

    /**
     * @return string
     */
    public function get_page_title()
    {
        return 'Email';
    }

    /**
     * @return string
     */
    public function get_name()
    {
        return '';
    }

}

