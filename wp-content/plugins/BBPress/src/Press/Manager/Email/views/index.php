<form method="post" id="test_email_form" novalidate="novalidate">
    <table class="form-table">
        <tbody>
        <tr>
            <th scope="row"><label for="test_email_id">Email ID</label></th>
            <td><input name="email" type="text" id="test_email_id" value="<?php echo $admin_email; ?>"
                       class="regular-text"></td>
        </tr>

        </tbody>
    </table>

    <div class="email_debug">
        <p>Email Debug Info</p>
        <pre></pre>
    </div>

    <p class="submit">
        <input type="submit" name="submit" id="submit" class="button button-primary" value="Send Test Email">
    </p>
</form>

<script language="JavaScript">
    jQuery(function () {
        jQuery('#test_email_form').on('submit',function(e){
            e.preventDefault();
            var data = jQuery("#test_email_form").serializeArray();
            var url = "<?php echo $module->generate_link('index', true); ?>";
            jQuery('div.bb_ajax_indicator').show();
            jQuery('div.email_debug').hide();
            var posting = jQuery.post(url,data);
            posting.done(function (data) {
                jQuery('div.email_debug pre').html(data);
                jQuery('div.email_debug').show();
            });
            posting.fail(function () {
            });
            posting.always(function () {
                jQuery('div.bb_ajax_indicator').hide();
            });
        });
    });
</script>
