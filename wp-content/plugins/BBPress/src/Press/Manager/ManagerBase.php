<?php

namespace BlueBeetle\Press\Manager;


class ManagerBase
{

    public $current_action = null;

    public $header_view = null;

    public $module_view = null;

    public $footer_view = null;

    private $action_menus = null;

    private $page_caption = null;

    private $tab_menus = null;

    private $view_variables = null;

    private $normal_messages = null;

    private $error_messages = null;

    private $current_user = null;

    public function init_module($action)
    {
        $this->current_action = $action;
        $this->page_caption = $action;
        $this->view_variables = array();

        $this->header_view = BBPRESS_ASSETS . 'manager' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'header.php';
        $this->footer_view = BBPRESS_ASSETS . 'manager' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'footer.php';
        $this->module_view = $this->get_module_dir() . 'views' . DIRECTORY_SEPARATOR . $action . '.php';

        $this->action_menus = array();
        $this->tab_menus = array();

        $this->normal_messages = array();
        $this->error_messages = array();
        $this->current_user = wp_get_current_user();
    }

    /**
     * @return \wpdb
     */
    public function db()
    {
        global $wpdb;

        return $wpdb;
    }

    public function get_capability($action)
    {
        return 'edit_posts';
    }

    public function create_menus($action)
    {

    }

    public function add_action_menu($title, $action, $capability = 'edit_posts', $query_variables = array())
    {
        $this->add_menu('action', $title, $action, $capability, $query_variables);
    }

    public function add_tab_menu($title, $action, $capability = 'edit_posts', $query_variables = array())
    {
        $this->add_menu('tab', $title, $action, $capability, $query_variables);
    }

    public function add_message($message)
    {
        $this->normal_messages[] = $message;
    }

    public function add_error_message($message)
    {
        $this->error_messages[] = $message;
    }

    private function add_menu($type, $title, $action, $capability, $query_variables = array())
    {
        if (!current_user_can($capability)) {
            return;
        }
        $menu = array(
            'title' => $title,
            'action' => $action,
            'current' => ($action == $this->current_action),
            'link' => $this->generate_link($action, false, $query_variables),
        );
        if ($type == 'action') {
            $this->action_menus[] = $menu;
        } else {
            $this->tab_menus[] = $menu;
        }
    }

    final public function assign($name, $value)
    {
        $this->view_variables[$name] = $value;
    }

    public function generate_link($action, $is_ajax = false, $query_variables = array())
    {
        $variables = array();
        $variables['page'] = $this->get_module_name();
        $variables['action'] = $action;
        if ($is_ajax) {
            $variables['is_ajax'] = 1;
        }
        $variables = $variables + $query_variables;

        $link = 'admin.php?' . http_build_query($variables);
        return admin_url($link);
    }

    public function render_view()
    {
        $reserved_variables = array(
            'module' => $this,
            'asset_url' => plugins_url('/assets/', BBPRESS_FILE),
            'page_caption' => $this->page_caption,
            'action_menus' => $this->action_menus,
            'tab_menus' => $this->tab_menus,
            'normal_messages' => $this->normal_messages,
            'error_messages' => $this->error_messages
        );
        $final_variables = $this->view_variables + $reserved_variables;
        extract($final_variables);

        if (!is_null($this->header_view) && file_exists($this->header_view)) {
            include_once($this->header_view);
        }

        if (!is_null($this->module_view) && file_exists($this->module_view)) {
            include_once($this->module_view);
        }

        if (!is_null($this->footer_view) && file_exists($this->footer_view)) {
            include_once($this->footer_view);
        }
    }

    /**
     * @param null $page_caption
     */
    public function set_page_caption($page_caption)
    {
        $this->page_caption = $page_caption;
    }

    final public function assign_variables()
    {

    }

    public function send_ajax_data($data, $is_error = false)
    {
        if ($is_error === false) {
            http_response_code(200);
            header('Content-Type: application/json; charset=UTF-8;');
            echo json_encode($data);
        } else {
            http_response_code(500);
            header('Content-Type: application/json; charset=UTF-8;');
            echo json_encode($data);
        }
        exit;
    }

    /**
     * @return string
     */
    final public function get_current_action()
    {
        return $this->current_action;
    }

    final function get_module_name()
    {
        $module_name = sanitize_title($this->get_name());
        if (!empty($module_name)) {
            return BBPRESS_MODULE . '_' . $module_name;
        }
        return BBPRESS_MODULE;
    }

    final function get_module_dir()
    {
        $reflector = new \ReflectionClass($this);
        $class_filename = $reflector->getFileName();
        return dirname($class_filename) . DIRECTORY_SEPARATOR;
    }

    /**
     * @return \WP_User
     */
    public function get_current_user()
    {
        return $this->current_user;
    }

}

