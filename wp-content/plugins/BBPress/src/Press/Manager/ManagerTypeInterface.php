<?php

namespace BlueBeetle\Press\Manager;


interface ManagerTypeInterface
{

    public static function get_instance();

    public function init_hooks($action);

    public function plugin_hooks($action);

    /**
     * @return string
     */
    public function get_title();

    /**
     * @return string
     */
    public function get_page_title();

    /**
     * @return string
     */
    public function get_name();

}