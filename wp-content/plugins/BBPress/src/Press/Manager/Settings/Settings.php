<?php

namespace BlueBeetle\Press\Manager\Settings;


use Aws\Api\DateTimeResult;
use BlueBeetle\Press\Config;
use BlueBeetle\Press\Manager\ManagerBase;
use BlueBeetle\Press\Manager\ManagerTypeInterface;
use Aws\CloudFront\CloudFrontClient;
use Aws\Exception\AwsException;
use Aws\Credentials\Credentials;

class Settings extends ManagerBase implements ManagerTypeInterface
{
    private static $instance = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function create_menus($action)
    {
        $this->add_tab_menu('Google', 'index', 'manage_options');
        $this->add_tab_menu('Seo', 'seo', 'manage_options');
    }

    public function init_hooks($action)
    {

    }

    public function get_capability($action)
    {
        return 'manage_options';
    }

    public function plugin_hooks($action)
    {

        $this->set_page_caption('Settings');
        include_once 'includes/' . $action . '-form.php';
        acf_form_head();
    }

    function index_action()
    {
        $form_settings = array(
            'post_id' => 'option',
            'field_groups' => array('group_bb_press_google_settings'),
            'form' => true,
            'submit_value' => 'Save Settings',
            'updated_message' => 'Saved!'
        );
        $this->assign('form_settings', $form_settings);
    }


    function seo_action()
    {
        $form_settings = array(
            'post_id' => 'option',
            'field_groups' => array('group_bb_press_seo_settings'),
            'form' => true,
            'submit_value' => 'Save Settings',
            'updated_message' => 'Saved!'
        );
        $this->assign('form_settings', $form_settings);
    }

    /**
     * @return string
     */
    public function get_title()
    {
        return 'Settings';
    }

    /**
     * @return string
     */
    public function get_page_title()
    {
        return 'Settings';
    }

    /**
     * @return string
     */
    public function get_name()
    {
        return 'Settings';
    }

}

