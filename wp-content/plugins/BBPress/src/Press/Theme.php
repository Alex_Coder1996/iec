<?php

namespace BlueBeetle\Press;


class Theme extends Base
{
    private static $instance = null;

    private $template_name = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {

        add_action('get_header', array($this, 'get_header'));
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_styles'));
        add_action('wp_enqueue_scripts', array($this, 'wp_enqueue_scripts'));
        add_filter('body_class', array($this, 'body_class'), 10, 3);

        add_action('init', array($this, 'register_theme_menus'));
        add_action('after_setup_theme', array($this, 'after_setup_theme'));
        add_action('wp_before_admin_bar_render', array($this, 'wp_before_admin_bar_render'));

        add_filter('nav_menu_css_class', array($this, 'nav_menu_css_class'), 10, 4);
        add_filter('nav_menu_link_attributes', array($this, 'nav_menu_link_attributes'), 10, 3);

        add_filter('script_loader_tag', array($this, 'add_defer_async_to_script'), 10, 3);
    }


    public function get_header()
    {
        $current_template = $this->get_common()->get_current_template();

        switch ($current_template) {
            case 'search.php':
                $this->template_name = 'search-result-page';
                break;
            case '404.php':
                $this->template_name = 'not-found-page';
                break;
            case 'page-templates/home-page.php':
                $this->template_name = 'home-page';
                break;
            case 'page-templates/about-landing-page.php':
                $this->template_name = 'about-page';
                break;
            case 'page-templates/about-introduction-page.php':
                $this->template_name = 'introduction-page';
                break;
            case 'page-templates/about-management-page.php':
                $this->template_name = 'management-board-page';
                break;
            case 'page-templates/about-history-page.php':
                $this->template_name = 'history-page';
                break;
            case 'page-templates/about-partners-page.php':
                $this->template_name = 'our-partners-page';
                break;
            case 'page-templates/about-offerings-page.php':
                $this->template_name = 'our-offerings-page';
                break;
            case 'page-templates/offices-landing-page.php':
                $this->template_name = 'regional-offices-page';
                break;
            case 'single-office.php':
                $this->template_name = 'office-detail-page';
                break;
            case 'page-templates/market-landing-page.php':
                $this->template_name = 'vertical-markets-page';
                break;
            case 'page-templates/market-section-page.php':
                $this->template_name = 'maritime-page';
                break;
            case 'page-templates/market-detail-page.php':
                $this->template_name = 'shipping-page';
                break;
            case 'page-templates/sp-landing-page.php':
                $this->template_name = 'solutions-products-page';
                break;
            case 'single-product.php':
            case 'single-solution.php':
                $this->template_name = 'product-detail-page';
                break;
            case 'single-news.php':
                $this->template_name = 'news-event-detail-page';
                break;
            case 'page-templates/vas-landing-page.php':
                $this->template_name = 'value-added-services-page';
                break;
            case 'page-templates/vas-detail-page.php':
                $this->template_name = 'vas-detail-page';
                break;
            case 'page-templates/vas-detail-v2-page.php':
                $this->template_name = 'vas-detail-v2-page';
                break;
            case 'page-templates/news-landing-page.php':
                $this->template_name = 'news-page';
                break;
            case 'page-templates/contact-page.php':
                $this->template_name = 'contact-page';
                break;
            case 'page-templates/become-partner-page.php':
                $this->template_name = 'become-partner-page';
                break;
            case 'page-templates/job-landing-page.php':
                $this->template_name = 'join-our-team-page';
                break;
            case 'page-templates/press-center-page.php':
                $this->template_name = 'press-center-page';
                break;
            case 'page-templates/media-kit-page.php':
                $this->template_name = 'media-kit-page';
                break;
            case 'page-templates/generic-content-page.php':
                $this->template_name = 'content-page';
                break;
            case 'page-templates/support-center-page.php':
                $this->template_name = 'support-center-page';
                break;
            case 'page-templates/iot-page.php':
                $this->template_name = 'iot-page';
                break;
            case 'page-templates/vsat-experience-page.php':
                $this->template_name = 'vsat-experience-page';
                break;
            case 'page-templates/surveillance-portfolio-page.php':
                $this->template_name = 'surveillance-portfolio-page';
                break;
            default:
                $this->template_name = 'generic';
                break;
        }
    }

    public function wp_enqueue_styles()
    {
        $stylesheet_dir_uri = get_stylesheet_directory_uri();
        wp_enqueue_style('bbpress-' . $this->template_name . '-style', $stylesheet_dir_uri . '/css/' . $this->template_name . '.css', array(), BBPRESS_VERSION . '-' . time());
        if (is_rtl()) {
            wp_enqueue_style('bbpress-' . $this->template_name . '-style-ar', $stylesheet_dir_uri . '/css/' . $this->template_name . '-ar.css', array(), BBPRESS_VERSION . '-' . time());
        }
    }

    public function wp_enqueue_scripts()
    {
        wp_enqueue_script('bbpress-' . $this->template_name . '-script', get_template_directory_uri() . '/js/' . $this->template_name . '.js', array(), BBPRESS_VERSION . '-' . time(), true);
    }

    public function body_class($classes)
    {
        $classes = array_filter($classes, function ($var) {
            return (stripos($var, 'page') === false);
        });
        $classes[] = $this->template_name;
        return $classes;
    }

    function register_theme_menus()
    {
        register_nav_menus(
            array(
                'primary-menu' => 'Primary Menu',
                'secondary-menu' => 'Secondary Menu',
                'footer-menu' => 'Footer Menu',
            )
        );
    }

    function nav_menu_css_class($classes, $item, $args, $depth)
    {

        $classes = array_filter($classes, function ($var) {
            if ($var == 'current-menu-item' || $var == 'menu-item-has-children' || $var == 'home' || $var == 'custom-case') {
                return true;
            }
            return false;
        });

        return $classes;
    }


    public function nav_menu_link_attributes($atts, $item, $args)
    {
        if (!empty($atts['aria-current']) && $atts['aria-current'] == 'page') {
            $atts['class'] = 'active';
        }
        return $atts;
    }

    function get_switch_link($lang_code = 'en')
    {
        $current_page_id = get_the_ID();
        if (empty($current_page_id)) {
            return null;
        }
        $new_id = apply_filters('wpml_object_id', $current_page_id, 'post', FALSE, $lang_code);
        if (empty($new_id)) {
            return null;
        }
        $url = get_the_permalink();
        $new_url = apply_filters('wpml_permalink', $url, $lang_code);
        return $new_url;
    }


    function get_lang_title($foreign_lang_code = 'en')
    {
        switch ($foreign_lang_code) {
            case 'de':
                $lang_title = 'German';
                break;
            case 'fr':
                $lang_title = 'French';
                break;
            case 'da':
                $lang_title = 'Danish';
                break;
            case 'ru':
                $lang_title = 'Russian';
                break;
            case 'no':
                $lang_title = 'Norwegian';
                break;
            case 'zh-hans':
                $lang_title = 'Mandarin';
                break;
            case 'sv':
                $lang_title = 'Swedish';
                break;
            case 'tr':
                $lang_title = 'Turkish';
                break;
            case 'ar':
                $lang_title = 'Arabic';
                break;

            default:
                $lang_title = 'English';
                break;
        }
        return $lang_title;
    }

    public function after_setup_theme()
    {
        add_theme_support('post-thumbnails');
        add_image_size('news-thumb-01', 500, 300, true);
        add_image_size('news-thumb-02', 350, 210, true);
    }

    public function wp_before_admin_bar_render()
    {
        global $wp_admin_bar;

        $wp_admin_bar->remove_menu('themes');
        $wp_admin_bar->remove_menu('customize');
        $wp_admin_bar->remove_menu('comments');
        $wp_admin_bar->remove_menu('new-content');
        $wp_admin_bar->remove_menu('search');
        $wp_admin_bar->remove_menu('wp-logo-external');
    }

    function add_defer_async_to_script($tag, $handle, $src)
    {
    
        if (FALSE === strpos($handle, 'bbpress-')) { // not our file
            return $tag;
        }


        return str_replace('><', ' defer="defer" async="async"><', $tag);
        
    }
}
