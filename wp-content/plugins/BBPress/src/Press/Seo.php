<?php

namespace BlueBeetle\Press;

class Seo extends Base {
	private static $instance = null;

	/**
	 * @var Config
	 */
	private $config;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		global $sitepress;

		$this->config = Config::get_instance();

		//Remove unwanted elements from theme
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_head', 'wp_generator' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'wp_head', 'rsd_link' );
		remove_action( 'wp_head', 'wlwmanifest_link' );
		remove_action( 'wp_head', 'wp_shortlink_wp_head' );
		remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
		remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
		add_filter( 'emoji_svg_url', '__return_false' );
		if ( is_a( $sitepress, 'SitePress' ) ) {
			remove_action( 'wp_head', array( $sitepress, 'meta_generator_tag' ) );
		}
		add_action( 'wp_print_styles', array( $this, 'wps_deregister_styles' ), 100 );
		add_action( 'init', array( $this, 'remove_wp_embed' ), 999 );


		add_action( 'wp_head', array( $this, 'wp_head' ), 0 );
	}

	public function wp_head() {
		$page_title = $this->get_page_title();
		print "\n<!-- START BB META -->\n";
		print  '<title>' . $page_title . '</title>' . "\n";
		$meta_description = $this->get_page_description();
		$this->print_meta_header( 'description', $meta_description );
		$this->print_meta_header( 'robots', $this->get_page_robots() );
		print "<!-- Open Graph -->\n";
		$this->print_meta_header( 'og:url', $this->get_open_graph_url(), true );
		$this->print_meta_header( 'og:type', $this->get_open_graph_type(), true );
		$this->print_meta_header( 'og:title', $this->get_open_graph_title( $page_title ), true );
		$this->print_meta_header( 'og:description', $this->get_open_graph_description( $meta_description ), true );
		$this->print_meta_header( 'og:image', $this->get_open_graph_image(), true );

		$this->insert_google_site_verification_code();

		print "<!-- END BB META -->\n\n";
	}

	public function print_meta_header( $key, $value, $is_property = false ) {
		if ( empty( $value ) ) {
			return;
		}

		$key_name = 'name';
		if ( $is_property ) {
			$key_name = 'property';
		}

		print '<meta ' . $key_name . '="' . $key . '" content="' . $value . '" />' . "\n";
	}

	function get_page_title() {
		$title_suffix = $this->config->get( 'bbpress_seo_option_title' );

		if ( is_search() ) {
			return 'Search Results - ' . $title_suffix;
		}

		if ( is_tax() ) {
			$term  = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$title = get_field( 'seo_title', $term->taxonomy . '_' . $term->term_id );
			if ( ! empty( $title ) ) {
				return $title;
			}
			$title = get_field( 'caption', $term->taxonomy . '_' . $term->term_id );
			if ( ! empty( $title ) ) {
				return $title . ' - ' . $title_suffix;
			}

			return $title_suffix;
		}

		$current_page_id = get_the_ID();
		if ( empty( $current_page_id ) ) {
			return $title_suffix;
		}
		$title = get_field( 'seo_title', $current_page_id );
		if ( ! empty( $title ) ) {
			return $title;
		}
		$title = get_field( 'caption', $current_page_id );
		if ( ! empty( $title ) ) {
			return $title . ' - ' . $title_suffix;
		}
		$title = get_the_title( $current_page_id );
		if ( ! empty( $title ) ) {
			return $title . ' - ' . $title_suffix;
		}

		return $title_suffix;
	}

	function get_page_description() {
		$default_description = $this->config->get( 'bbpress_seo_option_description' );

		if ( is_search() ) {
			return $default_description;
		}

		if ( is_tax() ) {
			$term        = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$description = get_field( 'seo_description', $term->taxonomy . '_' . $term->term_id );
			if ( ! empty( $description ) ) {
				return $description;
			}

			return $default_description;
		}

		$current_page_id = get_the_ID();
		if ( empty( $current_page_id ) ) {
			return $default_description;
		}
		$description = get_field( 'seo_description', $current_page_id );
		if ( ! empty( $description ) ) {
			return $description;
		}

		return $default_description;
	}

	function get_page_robots() {
		if ( is_search() ) {
			return 'noindex, nofollow';
		}

		$default_value = $this->config->get( 'bbpress_seo_option_robots' );

		if ( is_tax() ) {
			$term   = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$robots = get_field( 'seo_robots', $term->taxonomy . '_' . $term->term_id );
			if ( ! empty( $robots ) ) {
				return $robots;
			}

			return $default_value;
		}

		$current_page_id = get_the_ID();
		if ( empty( $current_page_id ) ) {
			return $default_value;
		}
		$robots = get_field( 'seo_robots', $current_page_id );
		if ( ! empty( $robots ) ) {
			return $robots;
		}

		return $default_value;
	}

	function get_open_graph_url() {
		global $wp;

		return home_url( $wp->request );
	}

	function get_open_graph_type() {
		return 'website';
	}

	function get_open_graph_title( $fallback_title = '' ) {
		if ( is_tax() ) {
			$term  = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$title = get_field( 'og_title', $term->taxonomy . '_' . $term->term_id );
			if ( ! empty( $title ) ) {
				return $title;
			}

			return $fallback_title;
		}

		$current_page_id = get_the_ID();
		if ( empty( $current_page_id ) ) {
			return $fallback_title;
		}

		$title = get_field( 'og_title', $current_page_id );
		if ( ! empty( $title ) ) {
			return $title;
		}

		return $fallback_title;
	}

	function get_open_graph_description( $fallback_description = '' ) {
		if ( is_tax() ) {
			$term        = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$description = get_field( 'og_description', $term->taxonomy . '_' . $term->term_id );
			if ( ! empty( $description ) ) {
				return $description;
			}

			return $fallback_description;
		}

		$current_page_id = get_the_ID();
		if ( empty( $current_page_id ) ) {
			return $fallback_description;
		}

		$description = get_field( 'og_description', $current_page_id );
		if ( ! empty( $description ) ) {
			return $description;
		}

		return $fallback_description;
	}

	function get_open_graph_image() {
		if ( is_tax() ) {
			$term  = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$image = get_field( 'og_image', $term->taxonomy . '_' . $term->term_id );
			if ( ! empty( $image ) ) {
				return $image['url'];
			}
		}

		$current_page_id = get_the_ID();
		if ( ! empty( $current_page_id ) ) {
			$image = get_field( 'og_image', $current_page_id );
			if ( ! empty( $image ) ) {
				return $image['url'];
			}
		}

		$default_image = $this->config->get( 'bbpress_og_option_image' );
		if ( ! empty( $default_image ) ) {
			if ( ! empty( $default_image['url'] ) ) {
				return $default_image['url'];
			}

			return wp_get_attachment_url( $default_image );
		}

		return '';
	}

	function insert_google_site_verification_code() {
		$verification_code = $this->config->get( 'bbpress_google_site_verification_code' );
		if ( empty( $verification_code ) ) {
			return;
		}
		print "<!-- Google Site Verification -->\n";
		$this->print_meta_header( 'google-site-verification', $verification_code );
	}

	function remove_wp_embed() {
		wp_deregister_script( 'wp-embed' );
	}

	function wps_deregister_styles() {
		wp_dequeue_style( 'wp-block-library' );
	}


}

