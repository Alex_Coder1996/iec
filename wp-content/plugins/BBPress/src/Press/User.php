<?php

namespace BlueBeetle\Press;

class User extends Base
{
    private static $instance = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        $this->start_session();
    }

    public function start_session()
    {
        session_start();
    }

    public function is_user_logged_in()
    {
        if (isset($_SESSION['user_hash']) && !empty($_SESSION['user_hash'])) {
            return true;
        }
        return false;
    }

    public function logout_user()
    {
        $_SESSION = array();
        session_destroy();
    }

    public function set_user($user_info)
    {
        $_SESSION['user_hash'] = $user_info['user_hash'];
        $_SESSION['user_info'] = $user_info;
    }

    public function get_user_hash()
    {
        if (isset($_SESSION['user_hash']) && !empty($_SESSION['user_hash'])) {
            return $_SESSION['user_hash'];
        }

        return null;
    }

    public function is_user_participated()
    {
        if (isset($_SESSION['user_info']) && !empty($_SESSION['user_info'])) {
            if (!empty($_SESSION['user_info']['has_participated']) && $_SESSION['user_info']['has_participated'] == 'yes') {
                return true;
            }
        }

        return false;
    }

}

