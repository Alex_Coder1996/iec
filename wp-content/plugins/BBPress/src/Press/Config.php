<?php

namespace BlueBeetle\Press;

class Config extends Base
{
    private static $instance = null;

    private $loaded_config = array();

    /**
     * Config constructor.
     */
    public function __construct()
    {
        $this->loaded_config = array();
    }

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }


    public function get($key, $default = null)
    {
        if (isset($this->loaded_config[$key])) {
            return $this->loaded_config[$key];
        }

        $value = get_field($key, 'option');

        if (is_null($value)) {
            $value = $default;
        }

        $this->loaded_config[$key] = $value;

        return $value;
    }

}

