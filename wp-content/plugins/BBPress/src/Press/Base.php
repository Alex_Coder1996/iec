<?php

namespace BlueBeetle\Press;


class Base
{
    /**
     * @var Rewrite
     */
    private $instance_rewrite = null;

    private $ajax_data = array();

    private $ajax_error = array();

    /**
     * @var Common
     */
    private $instance_common = null;

    /**
     * @var Config
     */
    private $instance_config = null;

    /**
     * @return Common
     */
    public function get_common()
    {
        if (is_null($this->instance_common)) {
            $this->instance_common = Common::get_instance();
        }
        return $this->instance_common;
    }

    /**
     * @return Config
     */
    public function get_config()
    {
        if (is_null($this->instance_config)) {
            $this->instance_config = Config::get_instance();
        }
        return $this->instance_config;
    }

    /**
     * @return \wpdb
     */
    public function db()
    {
        global $wpdb;

        return $wpdb;
    }

    public function get_lang_code()
    {
        $lang = apply_filters('wpml_current_language', NULL);
        if (!is_null($lang)) {
            return $lang;
        }
        return 'en';
    }

    public function get_admin_lang()
    {
        $admin_lang = apply_filters('wpml_current_language', NULL);
        if (!is_null($admin_lang)) {
            return $admin_lang;
        }
        return 'all';
    }


    public function getLanguage()
    {
        if (defined('ICL_LANGUAGE_CODE')) {
            return ICL_LANGUAGE_CODE;
        }
        return get_locale();
    }

    /**
     * @return array
     */
    public function get_ajax_data()
    {
        return $this->ajax_data;
    }

    /**
     * @param array $ajax_data
     */
    public function set_ajax_data($ajax_data)
    {
        $this->ajax_data = $ajax_data;
    }

    /**
     * @return array
     */
    public function get_ajax_error()
    {
        return $this->ajax_error;
    }

    /**
     * @param array $ajax_error
     */
    public function set_ajax_error($ajax_error)
    {
        $this->ajax_error = $ajax_error;
    }

}

