<?php

namespace BlueBeetle\Press;


class Acf extends Base {
	private static $instance = null;

	var $show_hotel_switcher = false;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		add_action( 'acf/init', array( $this, 'acf_init' ) );
		add_filter( 'acf/load_value/type=link', array( $this, 'acf_prepare_link_value' ), 10, 3 );
		add_filter( 'acf/format_value/type=link', array( $this, 'acf_format_link_value' ), 10, 3 );
		add_filter( 'acf/update_value/type=link', array( $this, 'acf_update_link_value' ), 10, 3 );

		add_filter( 'posts_join', array( $this, 'cf_search_join' ) );
		add_filter( 'posts_where', array( $this, 'cf_search_where' ) );
		add_filter( 'posts_distinct', array( $this, 'cf_search_distinct' ) );


	}

	function cf_search_join( $join ) {
		global $wpdb;

		if ( is_search() ) {
			$join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
		}

		return $join;
	}

	function cf_search_where( $where ) {
		global $pagenow, $wpdb;

		if ( is_search() ) {
			$where = preg_replace(
				"/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
				"(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)", $where );
		}

		return $where;
	}

	/**
	 * Prevent duplicates
	 *
	 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
	 */
	function cf_search_distinct( $where ) {
		global $wpdb;

		if ( is_search() ) {
			return "DISTINCT";
		}

		return $where;
	}

	public function acf_init() {

		$this->include_forms();

		acf_add_options_page( array(
			'page_title' => 'Theme Settings',
			'menu_title' => 'Theme Settings',
			'menu_slug'  => 'press-theme-menu',
			'capability' => 'edit_posts',
			'redirect'   => true
		) );

		acf_add_options_sub_page( array(
			'page_title'  => 'Common Settings',
			'menu_title'  => 'Common Settings',
			'parent_slug' => 'press-theme-menu',
		) );

		acf_add_options_sub_page( array(
			'page_title'  => 'Common Pages',
			'menu_title'  => 'Common Pages',
			'parent_slug' => 'press-theme-menu',
		) );

		acf_add_options_sub_page( array(
			'page_title'  => 'Enquiry Settings',
			'menu_title'  => 'Enquiry Settings',
			'parent_slug' => 'press-theme-menu',
		) );

		acf_add_options_sub_page( array(
			'page_title'  => 'Job Settings',
			'menu_title'  => 'Job Settings',
			'parent_slug' => 'press-theme-menu',
		) );

	}

	public function acf_prepare_link_value( $value, $post_id, $field ) {
		if ( is_array( $value ) && ! empty( $value['url'] ) ) {
			if ( defined( 'WP_SITEURL' ) ) {
				$value['url'] = str_replace( WP_SITEURL, '', $value['url'] );
			}
			$value['url'] = str_replace( 'http://iec.local', '', $value['url'] );
			$value['url'] = str_replace( 'https://iec.bluebeetle.net', '', $value['url'] );
		}

		return $value;
	}

	public function acf_format_link_value( $value, $post_id, $field ) {
		if ( is_array( $value ) && empty( $value['target'] ) ) {
			$value['target'] = '_self';
		}
		if ( is_array( $value ) && ! empty( $value['url'] ) ) {
			if ( defined( 'WP_SITEURL' ) ) {
				$value['url'] = str_replace( WP_SITEURL, '', $value['url'] );
			}
			$value['url'] = str_replace( 'http://iec.local', '', $value['url'] );
			$value['url'] = str_replace( 'https://iec.bluebeetle.net', '', $value['url'] );
		}

		return $value;
	}

	public function acf_update_link_value( $value, $post_id, $field ) {
		if ( is_array( $value ) && ! empty( $value['url'] ) && defined( 'WP_SITEURL' ) ) {
			$value['url'] = str_replace( WP_SITEURL, '', $value['url'] );
		}

		return $value;
	}

	private function include_forms() {
		$acf_form_folder_pattern = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . 'Acf' . DIRECTORY_SEPARATOR . '*.php';
		$acf_files               = glob( $acf_form_folder_pattern );
		if ( ! empty( $acf_files ) ) {
			foreach ( $acf_files as $file_name ) {
				include_once $file_name;
			}
		}
	}

}

