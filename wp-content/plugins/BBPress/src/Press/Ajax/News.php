<?php

namespace BlueBeetle\Press\Ajax;

use BlueBeetle\Press\Base;
use BlueBeetle\Press\Config;
use BlueBeetle\Press\Generic;
use BlueBeetle\Press\Press;
use BlueBeetle\Press\Email;
use BlueBeetle\Press\User;
use ReCaptcha\ReCaptcha;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;

class News extends Base {

	private static $instance = null;

	private $items_per_page;

	/**
	 * @var Config
	 */
	private $config;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		$this->items_per_page = 8;
		$this->config         = Config::get_instance();
	}

	public function search() {
		$get_method = INPUT_POST;
		$start_from = filter_input( $get_method, 'start-at', FILTER_VALIDATE_INT, array(
			"options" => array(
				"default" => 0,
			)
		) );
		$keyword    = filter_input( $get_method, 'keyword', FILTER_SANITIZE_STRING, array(
			"options" => array(
				"default" => '',
			)
		) );
		$term_ids   = filter_input( $get_method, 'terms', FILTER_VALIDATE_INT, FILTER_REQUIRE_ARRAY );

		if(!is_null($term_ids) && array_search(-1, $term_ids) !== FALSE)
        {
            // $term_ids has -1, so All filter is selected.
            //Ignore all other filters
            $term_ids = [];
        }

		$query                   = array();
		$query['post_type']      = array( 'news' );
		$query['posts_per_page'] = $this->items_per_page;
		$query['offset']         = $start_from;

		if ( is_array( $term_ids ) && ! empty( $term_ids ) ) {
			$term_ids           = array_unique( $term_ids );
			$query['tax_query'] = array(
				array(
					'taxonomy' => 'news_type',
					'field'    => 'id',
					'terms'    => $term_ids
				)
			);
		}else{
			$term_ids = [];
		}

		if(!empty($keyword))
		{
			$query['meta_query'] = [
				[
					'key' => 'caption',
					'value' => $keyword,
					'compare' => 'LIKE'
				]
			];
		}

		$query['suppress_filters'] = false;
		$post_query                = new \WP_Query( $query );

		$data            = array();
		$data['startAt'] = $start_from + $this->items_per_page;
		if ( $post_query->found_posts > $start_from + $this->items_per_page ) {
			$data['hasMore'] = true;
		} else {
			$data['hasMore'] = false;
		}
		$data['items'] = [];

		if ( $post_query->found_posts > 0 ) {
			foreach ( $post_query->posts as $post ) {
				$post_meta = get_fields( $post->ID );

				$location_text = '';
				if ( ! empty( $post_meta['office_location'] ) ) {
					$loc_texts = [];
					foreach ( $post_meta['office_location'] as $loc_term ) {
						$loc_texts[] = get_field( 'caption', $loc_term->taxonomy . '_' . $loc_term->term_id );
					}
					$location_text = implode( ', ', $loc_texts );
				}

				$image_url = ( isset( $post_meta['image']['sizes']['news-thumb-01'] ) ) ? $post_meta['image']['sizes']['news-thumb-01'] : $post_meta['image']['url'];

				$category_caption = '';

				if ( ! empty( $post_meta['type'] ) && is_array( $post_meta['type'] ) ) {
					foreach ( $post_meta['type'] as $cat ) {
						if ( in_array( $cat->term_id, $term_ids ) ) {
							$category_caption = get_field( 'caption', $cat->taxonomy . '_' . $cat->term_id );
							break;
						}
					}
				}

				$item            = [
					'cat'      => $category_caption,
					'href'     => get_the_permalink( $post->ID ),
					'date'     => get_the_date( 'd M Y', $post ),
					'title'    => $post_meta['caption'],
					'location' => $location_text,
					'thumb'    => $image_url
				];
				$data['items'][] = $item;
			}
		}

		$this->set_ajax_data( $data );

		return true;
	}


}