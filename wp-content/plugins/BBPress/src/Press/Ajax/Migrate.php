<?php

namespace BlueBeetle\Press\Ajax;

use BlueBeetle\Press\Base;
use BlueBeetle\Press\Config;
use BlueBeetle\Press\Generic;
use BlueBeetle\Press\Press;
use BlueBeetle\Press\Email;
use BlueBeetle\Press\User;
use ReCaptcha\ReCaptcha;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;

class Migrate extends Base {

	private static $instance = null;

	private $items_per_page;

	/**
	 * @var Config
	 */
	private $config;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		$this->items_per_page = 8;
		$this->config         = Config::get_instance();
	}

	public function index() {

		//migration completed
		exit;

		$sample_post = get_post( 57 );
		$acf_meta    = get_fields( $sample_post->ID, false );
		$sample_post = object_to_array( $sample_post );

		unset( $sample_post['ID'] );
		unset( $sample_post['post_password'] );
		unset( $sample_post['to_ping'] );
		unset( $sample_post['post_content'] );
		unset( $sample_post['post_excerpt'] );
		unset( $sample_post['post_parent'] );
		unset( $sample_post['pinged'] );
		unset( $sample_post['post_content_filtered'] );
		unset( $sample_post['guid'] );
		unset( $sample_post['menu_order'] );
		unset( $sample_post['post_mime_type'] );
		unset( $sample_post['comment_count'] );
		unset( $sample_post['filter'] );

		print "<p>Sample: " . $sample_post['post_title'] . "</p>\n";

		$query                     = array();
		$query['post_type']        = array( 'post' );
		$query['posts_per_page']   = 9999;
		$query['offset']           = 0;
		$query['suppress_filters'] = false;
		$post_query                = new \WP_Query( $query );

		if ( $post_query->found_posts > 0 ) {
			foreach ( $post_query->posts as $post_to_copy ) {

				print "<p>Migrate: " . $post_to_copy->post_title . "</p>\n";

				$sample_post['post_author']    = $post_to_copy->post_author;
				$sample_post['post_date']      = $post_to_copy->post_date;
				$sample_post['post_date_gmt']  = $post_to_copy->post_date_gmt;
				$sample_post['post_title']     = $post_to_copy->post_title;
				$sample_post['post_status']    = $post_to_copy->post_status;
				$sample_post['comment_status'] = $post_to_copy->comment_status;
				$sample_post['ping_status']    = $post_to_copy->ping_status;

				$sample_post['post_name']         = $post_to_copy->post_name;
				$sample_post['post_modified']     = $post_to_copy->post_modified;
				$sample_post['post_modified_gmt'] = $post_to_copy->post_modified_gmt;
				$sample_post['post_type']         = 'news';

				$new_post_id = wp_insert_post( $sample_post );

				update_field( 'caption', $post_to_copy->post_title, $new_post_id );
				update_field( 'content', $post_to_copy->post_content, $new_post_id );
				update_field( 'type', $acf_meta['type'], $new_post_id );
				update_field( 'office', $acf_meta['office'], $new_post_id );
				update_field( 'image', $acf_meta['image'], $new_post_id );


			}
		}

		exit;
	}


}