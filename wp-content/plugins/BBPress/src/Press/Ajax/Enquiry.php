<?php

namespace BlueBeetle\Press\Ajax;

use BlueBeetle\Press\Base;
use BlueBeetle\Press\Common;
use BlueBeetle\Press\Config;
use BlueBeetle\Press\Generic;
use BlueBeetle\Press\Press;
use BlueBeetle\Press\Email;
use BlueBeetle\Press\User;
use ReCaptcha\ReCaptcha;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Validation\RFCValidation;

class Enquiry extends Base {

	private static $instance = null;

	/**
	 * @var Config
	 */
	private $config;

	private $enquiry_table_name;

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function get_instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
	 */
	private function __construct() {
		$this->config             = Config::get_instance();
		$this->enquiry_table_name = $this->db()->prefix . Press::TABLE_ENQUIRY;
	}

	public function save() {

		if ( $_SERVER['REQUEST_METHOD'] !== 'POST' ) {
			$this->set_ajax_error( array( 'err_no' => '100' ) );

			return false;
		}

		$user_hash = Common::get_instance()->get_uuid();

		$data = array();

		$data['form_type'] = filter_input( INPUT_POST, 'form_type', FILTER_SANITIZE_STRING );
		$data['full_name'] = filter_input( INPUT_POST, 'full_name', FILTER_SANITIZE_STRING );
		$data['company']   = filter_input( INPUT_POST, 'company', FILTER_SANITIZE_STRING );
		$data['email']     = filter_input( INPUT_POST, 'email', FILTER_SANITIZE_EMAIL );
		$data['country']   = filter_input( INPUT_POST, 'country', FILTER_SANITIZE_STRING );
		$data['message']   = filter_input( INPUT_POST, 'message', FILTER_SANITIZE_STRING );
		$data['page_url']  = filter_input( INPUT_POST, 'page_url', FILTER_SANITIZE_STRING );
		$data['location']  = filter_input( INPUT_POST, 'location', FILTER_SANITIZE_STRING );
		$data['referrer_url']  = filter_input( INPUT_POST, 'referrer_url', FILTER_SANITIZE_STRING );

		if(empty($data['location']))
		{
			$data['location'] = 'global';
		}

		$interest  = filter_input( INPUT_POST, 'interest', FILTER_SANITIZE_STRING );
		$interests  = array_map(function($interest) {
			return filter_var( $interest, FILTER_SANITIZE_STRING );
		}, $_POST['interests']);
		$interests[] = $interest;
		$interests = array_unique(array_filter($interests)); 
		$data['interest'] .= implode(', ', $interests);


		if ( isset( $_POST['g-recaptcha-response'] ) ) {
			$data['recaptcha'] = filter_input( INPUT_POST, 'g-recaptcha-response', FILTER_UNSAFE_RAW );
		}

		$data['user_hash']    = $user_hash;
		$data['lang_code']    = $this->get_lang_code();
		$data['date_created'] = current_time( 'mysql' );

		$must_fields = [ 'form_type', 'full_name', 'company', 'email', 'message', 'page_url' ];
		foreach ( $must_fields as $value ) {
			if ( empty( $data[ $value ] ) ) {
				$this->set_ajax_error( array( 'err_no' => '101' ) );

				return false;
			}
		}

		$validator = new EmailValidator();
		if ( ! $validator->isValid( $data['email'], new RFCValidation() ) ) {
			$this->set_ajax_error( array( 'err_no' => '102' ) );

			return false;
		}

		$captcha_secret_key = get_config( 'bbpress_google_captcha_secret_key' );
		$recaptcha          = new ReCaptcha( $captcha_secret_key );
		$response           = $recaptcha->verify( $data['recaptcha'] );
		if ( ! $response->isSuccess() ) {
			$this->set_ajax_error( array( 'err_no' => '103' ) );

			return false;
		}
  	unset( $data['recaptcha'] );

		unset( $data['agreed'] );

		$this->db()->insert( $this->enquiry_table_name, $data );

		$query     = "SELECT * FROM {$this->enquiry_table_name} WHERE user_hash = '{$user_hash}' LIMIT 1;";
		$user_info = $this->db()->get_row( $query, ARRAY_A );

		$this->send_email( $user_info, $data['location'], $data['form_type'] );

		$result = array(
			'status' => 'ok',
		);

		$this->set_ajax_data( $result );

		return true;
	}

	private function send_email( $data = array(), $location = '', $form_type = '' ) {

		$email = Email::get_instance();


		if ( empty( $email->SMTP_host ) ) {
			return true;
		}

		$email->reset_last_error();

		if ( ! empty( $data['message'] ) ) {
			$data['message'] = nl2br( $data['message'] );
		}

		$variables = array(
			'full_name',
			'company',
			'email',
			'country',
			'interest',
			'message',
			'page_url',
			'location',
			'lang_code',
			'referrer_url'
		);

		//Send Email to Admin
		$admin_email_from_name = $this->config->get( 'enquiry_admin_email_from_name' );
		//$admin_delivery_email  = $this->get_admin_delivery_emails_for_country($location); //$this->config->get( 'enquiry_admin_delivery_email' );

		$admin_delivery_email = $this->get_admin_delivery_emails( $location, $form_type );

		$admin_email_subject = $this->config->get( 'enquiry_admin_email_subject' );
		$admin_email_subject = $this->replace_variables( $admin_email_subject, $variables, $data );

		$admin_email_body = $this->config->get( 'enquiry_admin_email_body' );
		$admin_email_body = $this->replace_variables( $admin_email_body, $variables, $data );

		$message = new \Swift_Message();
		if ( ! empty( $email->sender ) ) {
			$message->setSender( $email->sender, $admin_email_from_name );
			$message->setFrom( $email->sender, $admin_email_from_name );
		}
		if ( ! empty( $email->receiver ) ) {
			$message->setTo( $email->receiver );
		}

		foreach ( $admin_delivery_email as $email_info ) {
			$message->setTo( $email_info['email'], $email_info['name'] );
		}

		$message->setSubject( $admin_email_subject );
		$message->setBody( $admin_email_body, 'text/html' );

		try {
			$email->getEmailClient()->send( $message );
		} catch ( \Exception $e ) {
			$email->last_error = $e->getMessage();

			return false;
		}

		//Send Email to User
		$user_email_from_name  = $this->config->get( 'enquiry_user_email_from_name' );
		$user_email_from_email = $this->config->get( 'enquiry_user_email_from_email' );
		$user_email_subject    = $this->config->get( 'enquiry_user_email_subject' );
		$user_email_body       = $this->config->get( 'enquiry_user_email_body' );

		$user_email_subject = $this->replace_variables( $user_email_subject, $variables, $data );
		$user_email_body    = $this->replace_variables( $user_email_body, $variables, $data );

		$message = new \Swift_Message();

		if ( ! empty( $email->sender ) ) {
			$message->setSender( $email->sender );
			if ( $email->sender == $user_email_from_email ) {
				$message->setFrom( $user_email_from_email, $user_email_from_name );
			} else {
				$message->setFrom( $email->sender, $user_email_from_name );
				$message->setReplyTo( $user_email_from_email, $user_email_from_name );
			}
		}

		if ( ! empty( $email->receiver ) ) {
			$message->setTo( $email->receiver );
		} else {
			$message->setTo( $data['email'], $data['full_name'] );
		}
		$message->setSubject( $user_email_subject );
		$message->setBody( $user_email_body, 'text/html' );

		try {
			$email->getEmailClient()->send( $message );
		} catch ( \Exception $e ) {
			$email->set_last_error( $e->getMessage() );

			return false;
		}

		return true;
	}

	private function replace_variables( $content = '', $variables = array(), $data = array() ) {
		if ( ! empty( $variables ) ) {
			foreach ( $variables as $variable ) {
				$replace_value = '';
				if ( isset( $data[ $variable ] ) ) {
					$replace_value = $data[ $variable ];
				}
				$content = str_replace( '[[' . $variable . ']]', $replace_value, $content );
			}
		}

		return $content;
	}

	private function get_admin_delivery_emails( $location = '', $form_type = '' ) {

		$admin_delivery_email  = $this->config->get( 'enquiry_admin_delivery_email' );
		$admin_delivery_emails = [];
		foreach ( $admin_delivery_email as $email ) {
			$admin_delivery_emails[ $email['form_type'] ][ $email['country'] ][] = $email;
		}

		$emails = [];
		if ( $location != '' && $form_type != '' && @count( @$admin_delivery_emails[ $form_type ][ $location ] ) ) {
			$emails = $admin_delivery_emails[ $form_type ][ $location ];
		} else if ( $location == '' && $form_type != '' && @count( @$admin_delivery_emails[ $form_type ]['--'] ) ) {
			$emails = $admin_delivery_emails[ $form_type ]['--'];
		} else if ( $location != '' && $form_type == '' && @count( @$admin_delivery_emails['--'][ $location ] ) ) {
			$emails = @$admin_delivery_emails['--'][ $location ];
		} else {
			$emails = @$admin_delivery_emails['--']['--'];
		}

		return $emails;
	}

}