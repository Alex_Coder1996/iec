<?php

namespace BlueBeetle\Press\Ajax;

use BlueBeetle\Press\Base;
use BlueBeetle\Press\Config;

class Psfilter extends Base
{

    private static $instance = null;

    private $items_per_page;

    /**
     * @var Config
     */
    private $config;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        $this->items_per_page = 9;
        $this->config = Config::get_instance();
    }

    public function search()
    {
        $get_method = INPUT_POST;
        $start_from = filter_input($get_method, 'start-at', FILTER_VALIDATE_INT, array(
            "options" => array(
                "default" => 0,
            )
        ));
        $post_ps_filters = filter_input($get_method, 'ps_filter', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        $keyword = filter_input($get_method, 'keyword', FILTER_SANITIZE_STRING);

        $query = array();
        $query['post_type'] = array('product', 'solution');
        $query['posts_per_page'] = $this->items_per_page;
        $query['offset'] = $start_from;
        $query['suppress_filters'] = false;

        if (!empty($post_ps_filters)) {

            $filter_query_conditions = [];
            $individual_filter_conditions = [];
            $ps_filters = get_ps_filter_items(null, true);
            foreach ($ps_filters as $filter_name => $filter_keys) {
                if (isset($post_ps_filters[$filter_name]) && !empty($post_ps_filters[$filter_name])) {
                    foreach ($post_ps_filters[$filter_name] as $filter) {
                        if (in_array($filter, $filter_keys)) {
                            $filter_key = $filter_name . '_' . $filter;
                            $individual_filter_conditions[$filter_name][] = " filter_name = '{$filter_key}' AND filter_value ='1' ";
                        }
                    }
                }
            }

            $lang_code = $this->get_lang_code();

            foreach ($individual_filter_conditions as $filter_name => $conditions)
            {
                $filter_query_conditions[] = "SELECT DISTINCT post_id FROM wp_custom_ps_filter WHERE lang_code ='{$lang_code}' AND  ( " . implode($individual_filter_conditions[$filter_name], ') OR (') . " ) ";
            }

            if (!empty($filter_query_conditions)) {
                $filter_query = "SELECT DISTINCT post_id FROM wp_custom_ps_filter WHERE lang_code ='{$lang_code}' AND ";
                $filter_query_conditions = implode($filter_query_conditions, ') AND post_id in(');
                $filter_query = $filter_query . " post_id in(" . $filter_query_conditions . " ) ";

                $post_IDs = $this->db()->get_col($filter_query);
                if (!empty($post_IDs)) {
                    $query['post__in'] = $post_IDs;
                } else {
                    $query['post__in'] = array(-1);
                }
            }
        }

        if (!empty($keyword)) {

            $meta_query['relation'] = 'OR';

            $meta_query[] = array(
                'key' => 'caption',
                'value' => $keyword,
                'compare' => 'LIKE',
            );

            $meta_query[] = array(
                'key' => 'keywords',
                'value' => $keyword,
                'compare' => 'LIKE',
            );

            $query['meta_query'] = $meta_query;
        }


        $post_query = new \WP_Query($query);

        $data = array();
        $data['startAt'] = $start_from + $this->items_per_page;
        if ($post_query->found_posts > $start_from + $this->items_per_page) {
            $data['hasMore'] = true;
        } else {
            $data['hasMore'] = false;
        }
        $data['items'] = [];

        if ($post_query->found_posts > 0) {
            $app_filters = get_ps_filter_choices('application');
            foreach ($post_query->posts as $post) {
                $post_meta = get_fields($post->ID);

                $cats = [];
                if (!empty($post_meta['ps_filter_application'])) {
                    foreach ($post_meta['ps_filter_application'] as $app) {
                        $cats[] = ['cat' => $app, 'label' => $app_filters[$app]];
                    }
                }

                $item = [
                    'type' => $post->post_type,
                    'cats' => $cats,
                    'href' => get_the_permalink($post->ID),
                    'title' => $post_meta['caption'],
                    'thumb' => $post_meta['landing_image']['url']
                ];
                $data['items'][] = $item;
            }
        }

        $this->set_ajax_data($data);

        return true;
    }


}