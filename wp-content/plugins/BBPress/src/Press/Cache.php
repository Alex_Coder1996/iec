<?php

namespace BlueBeetle\Press;

use Phpfastcache\CacheManager;
use Phpfastcache\Config\ConfigurationOption;
use Phpfastcache\Core\Pool\ExtendedCacheItemPoolInterface;


class Cache extends Base
{
    private static $instance = null;

    private $cache_path = null;

    private $is_enabled = null;

    /**
     * @var ExtendedCacheItemPoolInterface
     */
    private $cache_instance = null;

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {
        $this->cache_path = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'cache';
        $this->is_enabled = false;

        $this->init_cache();
    }

    private function init_cache()
    {
        try {
            CacheManager::setDefaultConfig(new ConfigurationOption([
                'path' => $this->cache_path,
            ]));
        } catch (\Exception $e) {
            return;
        }

        try {
            $cache_instance = CacheManager::getInstance('files');
        } catch (\Exception $e) {
            return;
        }

        $this->cache_instance = $cache_instance;
        $this->is_enabled = true;
    }

    public function get_cache($key)
    {
        if (!$this->is_enabled) {
            return null;
        }

        $final_key = $this->make_key($key);

        try {
            $cached_item = $this->cache_instance->getItem($final_key);
        } catch (\Exception $e) {
            return null;
        }

        if (!$cached_item->isHit()) {
            return null;
        }

        return $cached_item->get();
    }

    public function write_cache($key, $value, $expire_in_seconds = 900)
    {
        if (!$this->is_enabled) {
            return false;
        }

        $final_key = $this->make_key($key);

        try {
            $cached_item = $this->cache_instance->getItem($final_key);
        } catch (\Exception $e) {
            return false;
        }

        $cached_item->set($value);
        if ($expire_in_seconds == 0) {
            $cached_item->expiresAfter($expire_in_seconds);
        }
        $this->cache_instance->save($cached_item);

        return true;
    }

    public function remove_cache($key)
    {
        if (!$this->is_enabled) {
            return null;
        }

        $final_key = $this->make_key($key);

        try {
            $status = $this->cache_instance->deleteItem($final_key);
        } catch (\Exception $e) {
            $status = false;
        }

        return $status;
    }


    /**
     * @param string $key
     * @return string
     */
    private function make_key($key)
    {
        $final = $this->get_lang_code() . '.' . $key;
        return md5($final);
    }

}

