<?php

namespace BlueBeetle\Press;


class Email extends Base
{

    private static $instance = null;

    /**
     * @var String
     */
    public $sender_name = null;

    /**
     * @var String
     */
    public $sender = null;

    /**
     * @var String
     */
    public $receiver = null;

    /**
     * @var String
     */
    public $SMTP_host = null;

    /**
     * @var String
     */
    public $SMTP_user = null;

    /**
     * @var String
     */
    public $SMTP_pass = null;

    /**
     * @var String
     */
    public $SMTP_port = null;

    /**
     * @var String
     */
    public $SMTP_protocol = null;

    /**
     * @var String
     */
    public $SMTP_login = null;

    /**
     * @var String
     */
    public $SMTP_timeout = null;

    /**
     * @var \Swift_SmtpTransport
     */
    var $transport = null;

    var $last_error = '';

    /**
     * Creates or returns an instance of this class.
     */
    public static function get_instance()
    {
        // If an instance hasn't been created and set to $instance create an instance and set it to $instance.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Initializes the plugin by setting localization, hooks, filters, and administrative functions.
     */
    private function __construct()
    {

        $config = Config::get_instance();
        $this->sender_name = $config->get('bbpress_email_sender_name');
        $this->sender = $config->get('bbpress_email_sender');
        $this->receiver = $config->get('bbpress_email_receiver');
        $this->SMTP_host = $config->get('bbpress_email_smtp_host');
        $this->SMTP_port = $config->get('bbpress_email_smtp_port');
        $this->SMTP_protocol = $config->get('bbpress_email_smtp_protocol');

        $this->SMTP_user = $config->get('bbpress_email_smtp_user');
        $this->SMTP_pass = $config->get('bbpress_email_smtp_pass');

        $this->SMTP_login = $config->get('bbpress_email_smtp_login');
        $this->SMTP_timeout = $config->get('bbpress_email_smtp_timeout');
    }

    /**
     * @param $php_mailer \PHPMailer
     */
    public function enable_smtp_wp_mail($php_mailer)
    {
        if (empty($this->SMTP_host)) {
            return;
        }

        $php_mailer->isSMTP();
        if (!is_null($this->SMTP_host)) {
            $php_mailer->Host = $this->SMTP_host;
        }
        if (!is_null($this->SMTP_port)) {
            $php_mailer->Port = $this->SMTP_port;
        }
        if (!is_null($this->SMTP_protocol)) {
            $php_mailer->SMTPSecure = $this->SMTP_protocol;
        }
        if (!is_null($this->SMTP_user) || !is_null($this->SMTP_pass)) {
            $php_mailer->SMTPAuth = true;
        }
        if (!is_null($this->SMTP_user)) {
            $php_mailer->Username = $this->SMTP_user;
        }
        if (!is_null($this->SMTP_pass)) {
            $php_mailer->Password = $this->SMTP_pass;
        }
        if (!is_null($this->SMTP_timeout)) {
            $php_mailer->Timeout = $this->SMTP_timeout;
        }
        if (!is_null($this->sender)) {
            $php_mailer->Sender = $this->sender;
            $php_mailer->From = $this->sender;
            $php_mailer->FromName = $this->sender_name;
        }
    }

    /**
     * @return \Swift_SmtpTransport
     */
    public function getEmailClient()
    {

        if (!is_null($this->transport)) {
            return $this->transport;
        }

        $transport = new \Swift_SmtpTransport();
        if (!is_null($this->SMTP_host)) {
            $transport->setHost($this->SMTP_host);
        }
        if (!is_null($this->SMTP_port)) {
            $transport->setPort($this->SMTP_port);
        }
        if (!is_null($this->SMTP_protocol)) {
            $transport->setEncryption($this->SMTP_protocol);
        }

        if (!is_null($this->SMTP_user)) {
            $transport->setUsername($this->SMTP_user);
        }
        if (!is_null($this->SMTP_pass)) {
            $transport->setPassword($this->SMTP_pass);
        }

//		if ( ! is_null( $this->SMTP_login ) ) {
//			$transport->setAuthMode( $this->SMTP_login );
//		}
        if (!is_null($this->SMTP_timeout)) {
            $transport->setTimeout($this->SMTP_timeout);
        }

        $this->transport = $transport;

        return $transport;
    }

    public function sent_test_email($email)
    {

        if (empty($this->SMTP_host)) {
            return 'Email is not configured!';
        }

        $message = new \Swift_Message();

        if (!empty($this->sender)) {
            $message->setSender($this->sender);
            $message->setFrom($this->sender, $this->sender_name);
        }

        $timestamp = date("F j, Y, g:i a");

        $message_subject = 'Test Email: ' . $timestamp;
        $message_body = "<p>Test Email</p>";
        $message_body .= "<p>Time: {$timestamp}</p>";
        $message_body .= "<hr>";
        $message_body .= "<p>BBPress Email</p>";

        $message->setTo($email);
        $message->setSubject($message_subject);
        $message->setBody($message_body, 'text/html');

        $mailLogger = new \Swift_Plugins_Loggers_ArrayLogger();
        $client = $this->getEmailClient();
        $client->registerPlugin(new \Swift_Plugins_LoggerPlugin($mailLogger));
        try {
            $client->send($message);
        } catch (\Exception $e) {

        }

        return $mailLogger->dump();

    }

    public function reset_last_error()
    {
        $this->last_error = '';
    }

    /**
     * @param string $last_error
     */
    public function set_last_error($last_error)
    {
        $this->last_error = $last_error;
    }

    public function get_last_error()
    {
        return $this->last_error;
    }


}

