<?php
/*
  Plugin Name: Blue Beetle Wordpress Plugin
  Plugin URI: https://www.bluebeetle.me
  Description: Plugin built by Blue Beetle team to extend the functionality of Wordpress
  Version: 1.1007
  Author: BBTeam
  Author URI: https://www.bluebeetle.ae/
  License: MIT
 */

use \BlueBeetle\Press\Config;
use \BlueBeetle\Press\Theme;
use \BlueBeetle\Press\Ajax;
use \BlueBeetle\Press\Press;

define('BBPRESS_FILE', __FILE__);
define('BBPRESS_ASSETS', dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR);
define('BBPRESS_VERSION', '1.1007');
define('BBPRESS_MODULE', 'bb_module');


include_once('vendor' . DIRECTORY_SEPARATOR . 'autoload.php');

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://s3.eu-west-1.amazonaws.com/bb.wp.updates/iec-telecom.com/bbpress/plugin.json',
    __FILE__, //Full path to the main plugin file or functions.php.
    'bbpress'
);

function get_config($key, $default = null)
{
    return Config::get_instance()->get($key, $default);
}

function get_lang_code()
{
    return Theme::get_instance()->get_lang_code();
}

function get_ajax_url($class, $method)
{
    return Ajax::get_instance()->get_ajax_url($class, $method);
}

global $bbPress;
$bbPress = Press::get_instance();
