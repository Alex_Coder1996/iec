<div class="wrap">
    <?php if(isset($page_caption) && !empty($page_caption)): ?>
        <h1 class="wp-heading-inline"><?php echo $page_caption; ?></h1>
    <?php endif; ?>

    <?php if(isset($action_menus) && !empty($action_menus)): ?>
        <?php foreach ($action_menus as $menu) : ?>
        <a href="<?php echo $menu['link']; ?>" class="page-title-action <?php echo $menu['action']; ?>"><?php echo $menu['title']; ?></a>
        <?php endforeach; ?>
    <?php endif; ?>
    <hr class="wp-header-end">


    <ul class="subsubsub">
        <?php if(isset($tab_menus) && !empty($tab_menus) && count($tab_menus) > 1): ?>
            <?php $menu_count = count($tab_menus); ?>
            <?php foreach ($tab_menus as $menu_index => $menu) : ?>
                <li class="all">
                    <a href="<?php echo $menu['link']; ?>" <?php if($menu['current']): ?> class="current" <?php endif; ?> aria-current="page"><?php echo $menu['title']; ?></span></a>
                    <?php if($menu_count != ($menu_index+1 )): ?> | <?php endif; ?>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>

    <br class="clear">

    <div class="bb_ajax_indicator">
        <p><img src="<?php echo $asset_url; ?>images/ajax-loader.gif" />Processing</p>
    </div>

    <?php if(isset($normal_messages) && !empty($normal_messages)): ?>
        <?php foreach ($normal_messages as $message) : ?>
            <div class="notice notice-success is-dismissible">
                <p><?php echo $message; ?></p>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if(isset($error_messages) && !empty($error_messages)): ?>
        <?php foreach ($error_messages as $message) : ?>
            <div class="notice notice-error is-dismissible">
                <p><?php echo $message; ?></p>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
