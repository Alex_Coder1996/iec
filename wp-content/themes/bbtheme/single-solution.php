<?php
get_header();

global $wp;
$current_page_link = home_url( $wp->request );
?>

<?php while (have_posts()) : the_post(); ?>

    <?php

    $fields = get_fields();
    $iec_ps_page = get_config('iec_ps_page');
    $iec_ps_page_link = get_permalink($iec_ps_page);
    $iec_enquire_page = get_config('iec_enquire_page');
    ?>

    <div id="main">
        <div class="top-body-pattern"></div>

        <div class="container w">
            <a href="<?php echo get_permalink($iec_ps_page); ?>"
               class="show-on-load back-link <?php echo ($_SERVER['HTTP_REFERER'] == $iec_ps_page_link) ? 'history-back':''; ?>"><?php _e('Return Back', 'bbtheme'); ?></a>
        </div>

        <div class="container m">
            <div class="columns cf">
                <div class="left">

                    <?php if (!empty($fields['product_images'])) : ?>
                        <div class="image-slider-widget">
                            <div class="slider">
                                <?php foreach ($fields['product_images'] as $image_index => $image) : ?>
                                    <?php if ($image_index == 0) : ?>
                                        <div class="slide">
                                            <img src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php else: ?>
                                        <div class="slide">
                                            <img class="lozad" data-src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($fields['coverage_images'])) : ?>
                        <div class="image-slider-widget">
                            <div class="slider">
                                <?php foreach ($fields['coverage_images'] as $image_index => $image) : ?>
                                    <?php if ($image_index == 0) : ?>
                                        <div class="slide">
                                            <img src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php else: ?>
                                        <div class="slide">
                                            <img class="lozad" data-src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="right">
                    <div class="text-content-widget">
                        <h1><?php echo $fields['caption']; ?></h1>
                        <?php echo $fields['content']; ?>
                    </div>

                    <?php if (!empty($fields['key_points']['points'])) : ?>
                        <div class="key-points-widget">
                            <?php if (!empty($fields['key_points']['caption'])) : ?>
                                <h5><?php echo $fields['key_points']['caption']; ?></h5>
                            <?php endif; ?>
                            <?php foreach ($fields['key_points']['points'] as $key_points) : ?>
                                <div><?php echo $key_points['text']; ?></div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="mid-body-pattern"></div>


        <?php if (!empty($fields['banner']['image']['url'])) : ?>
        <div class="container w">
            <div class="banner-widget">
                <div class="container xw n-p">

                    <?php if (!empty($fields['banner']['url'])) : ?>
                    <a href="<?php echo $fields['banner']['url']; ?>" class="" target="_blank">
                    <?php endif; ?>
                    <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                        <?php if (!empty($fields['banner']['caption']) || !empty($fields['banner']['description'])) : ?>
                            <div class="overlay-wrapper container w">
                                <div class="overlay">
                                    <?php if (!empty($fields['banner']['caption'])) : ?>
                                        <div class="caption-wrapper"><?php echo $fields['banner']['caption']; ?></div>
                                    <?php endif; ?>
                                    <?php if (!empty($fields['banner']['description'])) : ?>
                                        <?php echo $fields['banner']['description']; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($fields['banner']['caption'])) : ?>
                        <div class="bottom-gradient"></div>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($fields['banner']['url'])) : ?>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="container w">
            <div class="buttons-widget">
                <div class="container w">
                    <div class="wrapper">
                        <div class="container m">
                            <div class="cols cf">
                                <?php if (!empty($fields['brochure'])) : ?>
                                    <div>
                                        <a href="<?php echo $fields['brochure']['url']; ?>" download
                                           class="download-brochure"><?php _e('Download Brochure', 'bbtheme'); ?></a>
                                    </div>
                                <?php endif; ?>

                                <div>
                                    <a href="<?php echo get_permalink($iec_enquire_page); ?>?interest=<?php echo urlencode($fields['caption']); ?>&referrer=<?php echo urlencode($current_page_link); ?>"><?php _e('Enquire Now', 'bbtheme'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if(!empty($fields['video_section']['video_preview'])) : ?>
            <div class="video-section">
                <div class="container m">
                    <div class="s_columns">
                        <div class="s_columns_left">
                            <h5><?php echo $fields['video_section']['title']; ?></h5>
                            <p><?php echo $fields['video_section']['description']; ?></p>
                        </div>
                        <div class="s_columns_right">
                            <?php echo wp_video_shortcode( [
                                'src'      => ($fields['video_section']['video_file']) ? $fields['video_section']['video_file']['url'] : '',
                                'poster'   => $fields['video_section']['video_preview']['url'],
                                'height'   => 320,
                                'width'    => 570,
                            ] ); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($fields['recommended_solutions'])) : ?>
            <div class="recommended-solutions-widget">
                <div class="container w n-p">
                    <div class="wrapper">
                        <div class="container m">
                            <h5><?php _e('Recommended Solutions', 'bbtheme'); ?></h5>
                            <div class="items cf">
                                <?php foreach ($fields['recommended_solutions'] as $solution) : ?>
                                    <?php
                                    $caption = get_field('caption', $solution->ID);
                                    $summary = get_field('summary', $solution->ID);
                                    $landing_image = get_field('landing_image', $solution->ID);
                                    $applications = get_field('ps_filter_application', $solution->ID);
                                    $app_filters = get_ps_filter_choices('application');
                                    ?>
                                    <div class="item">
                                        <a href="<?php echo get_permalink($solution); ?>" class="inner-wrapper">
                                            <div class="thumb-wrapper">
                                                <div class="img lozad" data-background-image="<?php echo $landing_image['url']; ?>"></div>
                                                <?php if (!empty($applications)) : ?>
                                                    <div class="cats cats-<?php echo count($applications) ?>">
                                                        <?php foreach ($applications as $application) : ?>
                                                            <div class="<?php echo $application; ?>">
                                                                <div><?php echo $app_filters[$application]; ?></div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <h6><?php echo $caption; ?></h6>
                                            <p><?php echo $summary; ?></p>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
