<?php get_header(); ?>

<div class="body-wrapper">

    <div class="homepage">

        <?php get_template_part('includes/header') ?>

        <div id="main">


            <div class="container w">
                <div class="banner">
                    <img src="/html/dist/img/top-banner.png" alt="">
                </div>
            </div>

            <div class="container" style="min-height: 300px;">
            </div>
        </div>

        <?php get_template_part('includes/footer') ?>

    </div>

</div>


<?php get_footer(); ?>
