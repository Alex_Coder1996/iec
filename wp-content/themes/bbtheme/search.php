<?php
get_header();
?>

<div id="main" class="minimum-height">
    <div class="search-result-widget">
        <div class="container">
            <h1><?php _e( 'Search results', 'bbtheme' ); ?></h1>
        </div>

        <div class="container cf">
            <div class="the-search-form">
                <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
                    <input type="text" name="s" placeholder="<?php _e( 'Search', 'bbtheme' ); ?>"
                           value="<?php echo get_search_query(); ?>">
                    <input type="submit" value="<?php _e( 'Search', 'bbtheme' ); ?>">
                </form>
            </div>
        </div>

        <div class="container n-p">
			<?php if ( have_posts() ) : ?>
                <div class="items">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php
						$cat   = '';
						$image = '';
						$title = get_the_title();
						$link  = get_the_permalink();
						$meta  = get_fields();
						$link  = get_permalink();
						if ( ! empty( $meta['caption'] ) ) {
							$title = $meta['caption'];
						}
						if ( $post->post_type == 'page' ) {
							$cat = __( 'Page', 'bbtheme' );
						} elseif ( $post->post_type == 'news' ) {
							$cat = __( 'News', 'bbtheme' );
						} elseif ( $post->post_type == 'press-release' ) {
							$cat = __( 'Press Release', 'bbtheme' );
						} elseif ( $post->post_type == 'office' ) {
							$cat = __( 'Office', 'bbtheme' );
						} elseif ( $post->post_type == 'product' ) {
							$image = $meta['landing_image']['url'];
							$cat   = __( 'Product', 'bbtheme' );
						} elseif ( $post->post_type == 'solution' ) {
							$image = $meta['landing_image']['url'];
							$cat   = __( 'Solution', 'bbtheme' );
						} elseif ( $post->post_type == 'job' ) {
							$cat = __( 'Job', 'bbtheme' );
						}
						?>
                        <a href="<?php echo $link; ?>" class="item ">

                            <div class="cf">
								<?php if ( ! empty( $image ) ) : ?>
                                    <div class="img" style="background-image: url('<?php echo $image; ?>')"></div>
								<?php endif; ?>
								<?php if ( ! empty( $cat ) ) : ?>
                                    <div class="other"><?php echo $cat; ?></div>
								<?php endif; ?>
                                <div class="title"><?php echo $title; ?></div>
                            </div>
                        </a>
					<?php endwhile; ?>
                </div>
			<?php else : ?>
                <div class="no-result"><?php _e( 'Sorry, no results were found for this query.', 'bbtheme' ); ?></div>
			<?php endif; ?>
        </div>

		<?php
		$total            = isset( $wp_query->max_num_pages ) ? (int) $wp_query->max_num_pages : 1;
		$current          = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
		$posts_pagination = paginate_links(
			array(
				'mid_size'  => 1,
				'type'      => 'array',
				'prev_text' => '',
				'next_text' => '',
			)
		);
		?>
		<?php if ( ! empty( $posts_pagination ) ): ?>
            <div class="container cf">
                <div class="pagination">
                    <ul>
						<?php if ( $current > 1 ): ?>
                            <li><a href="<?php echo get_pagenum_link( $current - 1 ); ?>" class="prev"></a></li>
						<?php endif; ?>
						<?php for ( $page_no = 1; $page_no <= $total; $page_no ++ ): ?>
                            <li>
                                <a href="<?php echo get_pagenum_link( $page_no ); ?>" <?php if ( $page_no == $current ): ?> class="active" <?php endif; ?> ><?php echo $page_no; ?></a>
                            </li>
						<?php endfor; ?>
						<?php if ( $current < $total ): ?>
                            <li><a href="<?php echo get_pagenum_link( $current + 1 ); ?>" class="next"></a></li>
						<?php endif; ?>
                    </ul>
                </div>
            </div>
		<?php endif; ?>

    </div>
</div>

<?php get_footer(); ?>
