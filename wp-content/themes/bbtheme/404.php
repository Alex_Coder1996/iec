<?php
get_header();
?>

<div id="main">
    <div class="not-found-widget">
        <div class="container">
            <h1>404</h1>
            <h3><?php _e('Page not found.', 'bbtheme'); ?></h3>
            <p><?php _e('We are sorry, the page you requested could not be found.', 'bbtheme'); ?></p>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="back"><?php _e('Back to Homepage', 'bbtheme'); ?></a>
        </div>

    </div>
</div>

<?php get_footer(); ?>
