<?php
$footer_copyright_text = get_config('footer_copyright_text');
?>
<div id="footer">
        <div class="container m">
                <div class="wrapper">
                    <span class="copy-rights"><?php echo esc_html($footer_copyright_text); ?></span>
                    <!-- <a href="#">Legal Notices</a>
                    <a href="#">Privacy</a> -->
                    <?php wp_nav_menu(['theme_location' => 'footer-menu', 'container' => false, 'depth' => 1]); ?>
                </div>
        </div>
    </div>