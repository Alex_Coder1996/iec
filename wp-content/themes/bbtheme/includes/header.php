<?php
    $header_logo = get_config('header_logo');
?>
<div id="header">
    <div class="top-wrapper">
        <div class="container w">
            <div class="cf">
                <?php if(!empty($header_logo)) :?>
                    <a href="<?php echo esc_url(home_url('/')); ?>" class="logo">
                        <img src="<?php echo $header_logo['url']; ?>" alt="<?php echo $header_logo['alt']; ?>">
                    </a>
                <?php endif; ?>
                <a href="#" id="hamburger" class="expand-menu">
                    <span></span><span></span><span></span>
                </a>
                <div class="right-wrapper cf show-on-load">
                    <div class="search-form">
                        <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
                            <input type="text" name="s" placeholder="<?php _e('Search', 'bbtheme'); ?>" value="<?php echo get_search_query(); ?>">
                            <input type="submit">
                        </form>
                    </div>
                    <?php /*get_template_part('includes/language-switch') */?>
                    <div class="secondary-nav-wrapper">
                        <nav>
                            <?php wp_nav_menu(['theme_location' => 'secondary-menu', 'container' => false,'depth'=> 2]); ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="responsive-nav"></div>

    <div class="nav-wrapper show-on-load">
        <div class="container w">
            <nav>
                <?php wp_nav_menu(['theme_location' => 'primary-menu', 'container' => false, 'depth'=> 1]); ?>
            </nav>
        </div>
    </div>
</div>