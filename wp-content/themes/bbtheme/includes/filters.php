<?php
function get_ps_filters()
{
    $filter_order = ['application', 'operator', 'setup', 'service', 'type', 'speed', 'market', 'region'];

    return $filter_order;
}

function get_ps_filter_items($filter_name = null, $get_keys = false)
{
    $filters = [];

    $filters['application']['caption'] = 'Application';
    $filters['application']['locale'] = __('Application', 'bbtheme');
    $filters['application']['required'] = false;
    $filters['application']['choices'] = [
        ['key' => 'Maritime', 'caption' => 'Maritime', 'locale' => __('Maritime', 'bbtheme')],
        ['key' => 'Land', 'caption' => 'Land', 'locale' => __('Land', 'bbtheme')],
    ];

    $filters['operator']['caption'] = 'Operator';
    $filters['operator']['locale'] = __('Operator', 'bbtheme');
    $filters['operator']['required'] = false;
    $filters['operator']['choices'] = [
        ['key' => 'GlobalSatar', 'caption' => 'GlobalSatar', 'locale' => __('GlobalSatar', 'bbtheme')],
        ['key' => 'Intesat', 'caption' => 'Intesat', 'locale' => __('Intesat', 'bbtheme')],
        ['key' => 'Yahsat', 'caption' => 'Yahsat', 'locale' => __('Yahsat', 'bbtheme')],
        ['key' => 'Telenor', 'caption' => 'Telenor', 'locale' => __('Telenor', 'bbtheme')],
        ['key' => 'Iridium', 'caption' => 'Iridium', 'locale' => __('Iridium', 'bbtheme')],
        ['key' => 'Inmarsat', 'caption' => 'Inmarsat', 'locale' => __('Inmarsat', 'bbtheme')],
        ['key' => 'Thuraya', 'caption' => 'Thuraya', 'locale' => __('Thuraya', 'bbtheme')],
    ];

    $filters['setup']['caption'] = 'Set up';
    $filters['setup']['locale'] = __('Set up', 'bbtheme');
    $filters['setup']['required'] = false;
    $filters['setup']['choices'] = [
        ['key' => 'Fixed', 'caption' => 'Fixed', 'locale' => __('Fixed', 'bbtheme')],
        ['key' => 'Portable', 'caption' => 'Portable', 'locale' => __('Portable', 'bbtheme')],
    ];

    $filters['service']['caption'] = 'Service';
    $filters['service']['locale'] = __('Service', 'bbtheme');
    $filters['service']['required'] = false;
    $filters['service']['choices'] = [
        ['key' => 'Data', 'caption' => 'Data', 'locale' => __('Data', 'bbtheme')],
        ['key' => 'Voice', 'caption' => 'Voice', 'locale' => __('Voice', 'bbtheme')],
        ['key' => 'Video', 'caption' => 'Video', 'locale' => __('Video', 'bbtheme')],
        ['key' => 'M2M', 'caption' => 'M2M', 'locale' => __('M2M', 'bbtheme')],
        ['key' => 'Email Only', 'caption' => 'Email Only', 'locale' => __('Email Only', 'bbtheme')],
    ];

    $filters['type']['caption'] = 'Type';
    $filters['type']['locale'] = __('Type', 'bbtheme');
    $filters['type']['required'] = false;
    $filters['type']['choices'] = [
        ['key' => 'Solution', 'caption' => 'Solution', 'locale' => __('Solution', 'bbtheme')],
        ['key' => 'Product', 'caption' => 'Product', 'locale' => __('Product', 'bbtheme')],
        ['key' => 'Accessory', 'caption' => 'Accessory', 'locale' => __('Accessory', 'bbtheme')],
    ];

    $filters['speed']['caption'] = 'Speed';
    $filters['speed']['locale'] = __('Speed', 'bbtheme');
    $filters['speed']['required'] = false;
    $filters['speed']['choices'] = [
        ['key' => 'Low', 'caption' => 'Low', 'locale' => __('Low', 'bbtheme')],
        ['key' => 'Medium', 'caption' => 'Medium', 'locale' => __('Medium', 'bbtheme')],
        ['key' => 'High', 'caption' => 'High', 'locale' => __('High', 'bbtheme')],
    ];

    $filters['market']['caption'] = 'Market';
    $filters['market']['locale'] = __('Market', 'bbtheme');
    $filters['market']['required'] = false;
    $filters['market']['choices'] = [
        ['key' => 'Energy', 'caption' => 'Energy', 'locale' => __('Energy', 'bbtheme')],
        ['key' => 'Enterprise', 'caption' => 'Enterprise', 'locale' => __('Enterprise', 'bbtheme')],
        ['key' => 'Fishing', 'caption' => 'Fishing', 'locale' => __('Fishing', 'bbtheme')],
        ['key' => 'Government', 'caption' => 'Government', 'locale' => __('Government', 'bbtheme')],
        ['key' => 'Humanitarian', 'caption' => 'Humanitarian', 'locale' => __('Humanitarian', 'bbtheme')],
        ['key' => 'Leisure', 'caption' => 'Leisure', 'locale' => __('Leisure', 'bbtheme')],
        ['key' => 'Media', 'caption' => 'Media', 'locale' => __('Media', 'bbtheme')],
        ['key' => 'Offshore', 'caption' => 'Offshore', 'locale' => __('Offshore', 'bbtheme')],
        ['key' => 'Shipping', 'caption' => 'Shipping', 'locale' => __('Shipping', 'bbtheme')],
    ];

    $filters['region']['caption'] = 'Region';
    $filters['region']['locale'] = __('Region', 'bbtheme');
    $filters['region']['required'] = false;
    $filters['region']['choices'] = [
        ['key' => 'Global', 'caption' => 'Global', 'locale' => __('Global', 'bbtheme')],
        ['key' => 'Africa', 'caption' => 'Africa', 'locale' => __('Africa', 'bbtheme')],
        ['key' => 'Asia_Pacific', 'caption' => 'Asia Pacific', 'locale' => __('Asia Pacific', 'bbtheme')],
        ['key' => 'Central_America', 'caption' => 'Central America', 'locale' => __('Central America', 'bbtheme')],
        ['key' => 'Central_Asia', 'caption' => 'Central Asia', 'locale' => __('Central Asia', 'bbtheme')],
        ['key' => 'Europe', 'caption' => 'Europe', 'locale' => __('Europe', 'bbtheme')],
        ['key' => 'Middle_East', 'caption' => 'Middle East', 'locale' => __('Middle East', 'bbtheme')],
        ['key' => 'Northern_America', 'caption' => 'Northern America', 'locale' => __('Northern America', 'bbtheme')],
        ['key' => 'Oceania', 'caption' => 'Oceania', 'locale' => __('Oceania', 'bbtheme')],
        ['key' => 'South_America', 'caption' => 'South America', 'locale' => __('South America', 'bbtheme')],
    ];

    $filter_counts = \BlueBeetle\Press\Generic::get_instance()->get_ps_filter_counts();

    $filter_keys = [];
    foreach ($filters as $f_key => $f_value) {
        $filter_keys[$f_key] = [];
        foreach ($f_value['choices'] as $c_index => $c_value) {
            $sanitized_key = sanitize_title_for_query($c_value['key']);
            $db_key = $f_key . '_' . $sanitized_key;
            $filters[$f_key]['choices'][$c_index]['key'] = $sanitized_key;
            $filter_keys[$f_key][] = $sanitized_key;
            if (isset($filter_counts[$db_key])) {
                $filters[$f_key]['choices'][$c_index]['count'] = $filter_counts[$db_key];
            } else {
                $filters[$f_key]['choices'][$c_index]['count'] = 0;
            }
        }
    }

    if ($get_keys) {
        return $filter_keys;
    }

    if (is_null($filter_name)) {
        return $filters;
    }

    if (isset($filters[$filter_name])) {
        return $filters[$filter_name];
    }

    return [];
}

function print_ps_filter($filter_name)
{
    $ps_values = get_ps_filter_items($filter_name);
    if (empty($ps_values)) {
        return;
    }
    foreach ($ps_values['choices'] as $choice) {
        if ($choice['count'] > 0) {
            print '<div class="toggle-field">';
            print '<input type="checkbox" value="' . $choice['key'] . '" name="ps_filter[' . $filter_name . '][]" id="psf_' . $filter_name . '_' . $choice['key'] . '" data-key="' . $choice['key'] . '">';
            print '<label class="cf" for="psf_' . $filter_name . '_' . $choice['key'] . '">';
            print '<div class="switch"></div>';
            print '<div class="text">' . $choice['locale'] . '</div>';
            print '</label>';
            print '</div>';
        }
    }
}

function get_ps_filter_choices($filter_name)
{
    $filter_items = get_ps_filter_items($filter_name);
    $choices = [];
    if (!empty($filter_items['choices'])) {
        foreach ($filter_items['choices'] as $choice) {
            $choices[$choice['key']] = $choice['locale'];
        }
    }
    return $choices;
}