<?php
$footer_heading = get_config('footer_heading');
$footer_copyright_text = get_config('footer_copyright_text');
$footer_social_links = get_config('footer_social_links');
$cookie_notice = get_config('cookie_notice');
$privacy_policy_page = get_config('iec_privacy_policy_page');

?>
<div id="footer">
    <div class="container m">
        <div class="wrapper cf">

            <!-- <a href="#" class="chat"></a> -->

            <div class="left-wrapper">
                <?php if (!empty($footer_heading)) : ?>
                    <h3><?php echo $footer_heading; ?></h3>
                <?php endif; ?>
                <div class="cf">
                    <?php if (!empty($footer_copyright_text)) : ?>
                        <div class="copy-rights"><?php echo esc_html($footer_copyright_text); ?></div>
                    <?php endif; ?>
                    <div class="links">
                        <nav>
                            <?php wp_nav_menu(['theme_location' => 'footer-menu', 'container' => false, 'depth' => 1]); ?>
                        </nav>
                    </div>
                </div>
            </div>

            <?php if (!empty($footer_social_links)) : ?>
                <div class="right-wrapper">
                    <div class="cf">
                        <div class="social-media-widget cf">
                            <?php foreach ($footer_social_links as $link_index => $link) : ?>
                                <a href="<?php echo $link['link']; ?>" target="_blank" class="<?php echo $link['type']; ?>"><?php echo $link['type']; ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>


<div id="cookie-notice" class="cookie-notice-widget">
    <div class="inner-content">
        <div class="message">
            <?php echo nl2br($cookie_notice); ?>
        </div>
        <div class="buttons">
            <a href="#" class="accept">Accept</a>
            <a href="#" class="reject">Decline</a>
        </div>
        <div class="links">
            <a href="<?php echo get_permalink($privacy_policy_page)?>">Read our privacy policy</a>
        </div>
        <a href="#" class="close"></a>
    </div>
</div>