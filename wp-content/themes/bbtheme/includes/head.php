<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	
	<link rel="stylesheet" href="https://iec-telecom.com/wp-content/themes/bbtheme/css/home-page.css">
	
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>

    <?php
    $gtm_container_id =  get_config('bbpress_google_gtm_container_id');
    //$cookie_notice_accepted = filter_var($_COOKIE['cookie_notice_accepted'], FILTER_VALIDATE_BOOLEAN);
    ?>
    <?php if (!empty($gtm_container_id)) : ?>
        <!-- Google Tag Manager -->
        <script>
            function addGTMScript(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            };

            function initGTM() {
                if (window.gtmDidInit) {
                    return false;
                }
                window.gtmDidInit = true;
                addGTMScript(window, document, 'script', 'dataLayer', '<?php echo $gtm_container_id; ?>');
            }
        </script>
        <!-- End Google Tag Manager -->


    <?php endif; ?>

    <?php if (is_single(927)) :?>
        <!-- Global site tag (gtag.js) - Google Ads: 10785844840 -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=AW-10785844840"></script>
        <script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-10785844840'); </script>
        <!-- Event snippet for Submit lead form conversion page -->
        <script> gtag('event', 'conversion', {'send_to': 'AW-10785844840/sLzoCNWTwPkCEOjci5co'}); </script>
    <?php endif; ?>
	
	<?php if(is_single()) { ?>
	
	<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "BlogPosting",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "https://iec-telecom.com/en/news/iec-telecom-heads-to-the-international-fishing-industry-trade-fair-itechmer-2021/"
  },
  "headline": "IEC Telecom heads to the International Fishing Industry Trade Fair - Itechmer 2021",
  "description": "Itechmer is renowned as a meeting place for decision makers and the only global exhibition devoted to the fishing industry in France. IEC Telecom will be showcasing its state-of-the-art satellite communication solutions portfolio at this much anticipated event on the maritime sector calendar.",
  "image": "https://iec-telecom.com/wp-content/uploads/2019/10/logo.svg",  
  "author": {
    "@type": "Organization",
    "name": "IEC Telecom",
    "url": "https://iec-telecom.com/en/"
  },  
  "publisher": {
    "@type": "Organization",
    "name": "",
    "logo": {
      "@type": "ImageObject",
      "url": ""
    }
  },
  "datePublished": "2021-09-21"
}
</script>
	
	<?php } ?>
	
</head>

<body <?php body_class(); ?>>

    <?php if (!empty($gtm_container_id) && $cookie_notice_accepted) : ?>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm_container_id; ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    <?php endif; ?>