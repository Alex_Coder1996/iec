<?php
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    global $post;
    $fields = get_fields();
    $current_page_id = get_the_ID();

    $offices = \BlueBeetle\Press\Generic::get_instance()->get_offices();
    $countries = \BlueBeetle\Press\Common::get_instance()->get_countries();
    $form_interests = get_config('enquiry_form_interest');

    $success_popup_content = get_config('enquiry_success_popup_content');
    $success_popup_content_key = array_search('office-enquiry', array_column($success_popup_content, 'form_type'));

    $foreign_status = 0;
    $lang_title = 'English';    
    $foreign_lang_code = ICL_LANGUAGE_CODE;
    if(ICL_LANGUAGE_CODE=='en') :
        if($fields['translation_language']!='') :            
            $foreign_lang_code = $fields['translation_language'];
            $switch_link = \BlueBeetle\Press\Theme::get_instance()->get_switch_link($foreign_lang_code);
            $foreign_status = 1;
        endif;    
    else :
        $foreign_status = 1;
        $switch_link = \BlueBeetle\Press\Theme::get_instance()->get_switch_link('en');
    endif;   

    if($foreign_status == 1) :
        $lang_title = \BlueBeetle\Press\Theme::get_instance()->get_lang_title($foreign_lang_code);        
    endif;

    if(ICL_LANGUAGE_CODE=='en') :
        $keep_lang_text = 'Keep in English';
        $keep_flag = get_template_directory_uri() . '/img/flags/en.jpg';
        $switch_lang_text = 'Switch to '.$lang_title;
        $switch_lang_url = $switch_link;
        $switch_flag = get_template_directory_uri() . '/img/flags/'.$foreign_lang_code.'.jpg';
        $switch_lang_code = $foreign_lang_code;
    else:
        $keep_lang_text = 'Keep in '.$lang_title;
        $keep_flag = get_template_directory_uri() . '/img/flags/'.$foreign_lang_code.'.jpg';
        $switch_lang_text = 'Switch to English';
        $switch_lang_url = \BlueBeetle\Press\Theme::get_instance()->get_switch_link('en');
        $switch_flag = get_template_directory_uri() . '/img/flags/en.jpg';
        $switch_lang_code = 'en';
    endif;

    ?>

    <script language="JavaScript">
        let error_messages = {
            100: '<?php esc_attr_e('Invalid method', 'bbtheme'); ?>',
            101: '<?php esc_attr_e('Invalid data', 'bbtheme'); ?>',
            102: '<?php esc_attr_e('Invalid email', 'bbtheme'); ?>',
            103: '<?php esc_attr_e('Captcha not validated', 'bbtheme'); ?>'
        };
    </script>
    <div id="main">

        <div id="curr_page_lang" data-curr_page_lang="<?php echo ICL_LANGUAGE_CODE; ?>">
        <div id="office_page_slug" data-office-slug="<?php echo $post->post_name; ?>">    
        <?php 
        if (!empty($fields['translation_language'])) :
        ?>
        <div class="banner_wrapper">
            <div class="container xw">
                <div class="lang_block" id="lang_banner_block">
                    <div class="title"><?php echo $fields['language_selection_title']; ?></div>

                    <div class="lang_buttons_div">
                        <div class="lang_link_wrap">
                            <a href="<?php echo $switch_lang_url; ?>" class="switch_lang_cookie_link lang_cookie_link" data-langval="<?php echo $foreign_lang_code; ?>">
                                <div class="flag_img"><img width="18" height="12" src="<?php echo $switch_flag; ?>"></div>
                                <div class="switch_text"><?php echo $switch_lang_text; ?></div>
                            </a>
                        </div>
                        <div class="lang_link_wrap">
                            <a href="#" class="keep_lang_cookie_link lang_cookie_link" data-langval="<?php echo ICL_LANGUAGE_CODE; ?>">
                                <div class="flag_img"><img width="18" height="12" src="<?php echo $keep_flag; ?>"></div>
                                <div class="switch_text"><?php echo $keep_lang_text; ?></div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>   
        </div>     
        <?php endif; ?>

        <?php if (!empty($fields['slider'])) : ?>
            <div class="general-slider-widget">
                <div class="wrapper container xw n-p">

                    <?php if (!empty($offices)) : ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($offices as $office_index => $post) : ?>
                                    <?php
                                    $caption = get_field('landing_caption', $post->ID);
                                    if (empty($caption)) {
                                        $caption = get_field('caption', $post->ID);
                                    }

                                    $region_page_url = get_the_permalink();
                                    $reg_fields = get_fields();
                                    if($reg_fields['translation_language']!='') {            
                                        $translation_language_code = $reg_fields['translation_language'];
                                        if(isset($_COOKIE['lang_cookie_notice_accepted_'.$post->post_name]) && $_COOKIE['lang_cookie_notice_accepted_'.$post->post_name]!='') :
                                            if($_COOKIE['lang_cookie_notice_accepted_'.$post->post_name]==$translation_language_code) :
                                                $region_page_url = apply_filters('wpml_permalink', $region_page_url , $translation_language_code);
                                            endif;
                                        endif;

                                    }

                                    ?>
                                    <a href="<?php echo $region_page_url; ?>" <?php if ($current_page_id == $post->ID) : ?> class="active" <?php endif; ?>><?php echo $caption ?></a>
                                <?php endforeach; ?>
                            </div>                            

                            <?php
                            if($foreign_status == 1) : ?>
                            <div class="lang_sel_div">
                                <a href="#" class="lang_sel_box_link"><img width="18" height="12" src="<?php echo $keep_flag; ?>"></a>                                
                            </div>
                            <div class="langs_list_div">
                                <div class="lang_links_div">
                                    <div class="lang_link_wrap">
                                        <a href="#" class="close_lang_box_link lang_sel_link" data-langval="<?php echo ICL_LANGUAGE_CODE; ?>">
                                            <div class="flag_img"><img width="18" height="12" src="<?php echo $keep_flag; ?>"></div>
                                            <div class="switch_text"><?php echo $keep_lang_text; ?></div>
                                        </a>
                                    </div>
                                    <div class="lang_sel_note">This page is also available in: </div>
                                    <div class="lang_link_wrap">
                                        <a href="<?php echo $switch_lang_url; ?>" class="switch_lang_box_link lang_sel_link" data-langval="<?php echo $switch_lang_code; ?>">
                                            <div class="flag_img"><img width="18" height="12" src="<?php echo $switch_flag; ?>"></div>
                                            <div class="switch_text"><?php echo $switch_lang_text; ?></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="close_box">
                                    <a class="close_link"></a>
                                </div>
                            </div>
                            <?php endif; ?>

                        </div>
                    <?php endif; ?>

                    <div class="slider">
                        <?php foreach ($fields['slider'] as $slider) : ?>
                            <div class="slide">
                                <div class="inner-wrapper lozad" data-background-image="<?php echo $slider['image']['url']; ?>">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="container w n-p arrows-wrapper"></div>
                </div>
            </div>
        <?php endif; ?>

        <div class="columns">
            <div class="container m">
                <div class="cf">
                    <div class="left">
                        <div class="text-content-widget">
                            <div class="content">
                                <h1><?php echo $fields['caption']; ?></h1>
                                <?php echo $fields['content']; ?>
                            </div>

                        </div>
                    </div>
                    <div class="right">
                        <div class="enquiry-form-widget">

                            <form action="<?php echo get_ajax_url('enquiry', 'save'); ?>" method="post" id="enquiry-form" novalidate>



                                <div class="form-fields">
                                    <h5>
                                        <?php _e('Enquire Now', 'bbtheme'); ?>
                                    </h5>

                                    <div class="error">
                                        <?php _e('Found some issues, Try again later.', 'bbtheme'); ?>
                                    </div>

                                    <input type="hidden" name="form_type" value="office-enquiry">

                                    <!--<input type="hidden" name="enquiry_for_country" id="enquiry_for_country" value="<?php /*echo $enquiry_for_country */ ?>">-->
                                    <div class="form-field">
                                        <input type="text" name="full_name" placeholder="<?php esc_attr_e('Full Name...', 'bbtheme'); ?>" required>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" name="company" placeholder="<?php esc_attr_e('Company...', 'bbtheme'); ?>">
                                    </div>
                                    <div class="form-field">
                                        <input type="email" name="email" placeholder="<?php esc_attr_e('Email...', 'bbtheme'); ?>" required>
                                    </div>
                                    <?php if (!empty($countries)) : ?>
                                        <div class="form-field">
                                            <select name="country" id="" required class="select2">
                                                <option value=""><?php _e('Country...', 'bbtheme'); ?></option>
                                                <?php foreach ($countries as $country) : ?>
                                                    <option value="<?php echo $country['country_code']; ?>"><?php echo $country['country_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($form_interests)):  ?>
                                    
                                    <div class="form-field">
                                        <select name="interests[]" required multiple class="select2 select2-interest" data-select2-hide-search="true" data-select2-placeholder="<?php _e('Interest...', 'bbtheme');  ?>">
                                            <option value=""><?php _e('Interest...', 'bbtheme');  ?></option>
                                            <?php foreach ($form_interests as $interest): ?>
                                                <option value="<?php echo $interest['text'];  ?>"><?php echo $interest['text']; ?></option>
                                            <?php endforeach;  ?>
                                        </select>
                                    </div>
                                    <?php endif;  ?>

                                    <div class="form-field no-border">
                                        <textarea name="message" id="" cols="30" rows="4" placeholder="<?php esc_attr_e('Message...', 'bbtheme'); ?>" class="expand-on-click" required></textarea>
                                    </div>

                                    <div class="form-field  no-border google-captcha-wrapper">
                                    <div id="google-captcha-holder"
                                         data-sitekey="<?php echo get_config('bbpress_google_captcha_site_key'); ?>">
                                        <div class="g-recaptcha" id="g-recaptcha">
                                        </div>
                                    </div>
                                    </div>

                                    <input type="hidden" name="page_url" value="<?php the_permalink($current_page_id); ?>">
                                    <input type="hidden" name="location" value="<?php echo get_post($current_page_id)->post_name; ?>">

                                    <div class="form-button">
                                        <button name="submit"><?php _e('Send', 'bbtheme'); ?></button>
                                    </div>
                                </div>

                                <div class="loading-overlay">
                                    <div class="loader"></div>
                                </div>
                                <div class="success">
                                    <h5><?php _e('Thank You!', 'bbtheme'); ?></h5>
                                    <?php _e('Your inquiry has been well received. One of our customer service representatives will contact you shortly.', 'bbtheme'); ?>
                                </div>
                            </form>
                        </div>

                        <div id="success-popup" class="message-popup lity-hide">
                            <?php echo $success_popup_content[$success_popup_content_key]['content']; ?>
                        </div>

                        <?php if (!empty($fields['address'])) : ?>
                            <div class="address-tabs-widget">
                                <div class="tab-links">
                                    <?php foreach ($fields['address'] as $address_index => $address) : ?>
                                        <a href="#address-<?php echo $address_index; ?>"><?php echo $address['caption']; ?></a>
                                    <?php endforeach; ?>
                                </div>

                                <div class="tab-contents">
                                    <?php foreach ($fields['address'] as $address_index => $address) : ?>
                                        <div id="address-<?php echo $address_index; ?>" class="tab-content">
                                            <a href="<?php echo $address['map_link']['url']; ?>" target="<?php echo $address['map_link']['target']; ?>">
                                                <div class="map">
                                                    <img src="<?php echo $address['image']['url']; ?>" alt="<?php echo $address['caption']; ?>">
                                                </div>
                                                <?php if (!empty($address['address'])) : ?>
                                                    <div class="address">
                                                        <?php echo $address['address']; ?>
                                                    </div>
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>


                        <?php if (!empty($fields['downloads'])) : ?>
                            <?php foreach ($fields['downloads'] as $download) : ?>
                                <a href="<?php echo $download['download_file']['url']; ?>" download class="download-button">
                                    <div class="inner-wrapper cf">
                                        <div class="icon"></div>
                                        <div class="text <?php echo (empty($download['caption']) || empty($download['sub-caption'])) ? 'has-one-caption' : '' ?>">
                                            <?php if (!empty($download['caption'])) : ?>
                                                <span class="caption"><?php echo $download['caption']; ?></span>
                                            <?php endif; ?>
                                            <?php if (!empty($download['sub_caption'])) : ?>
                                                <span class="<?php echo (empty($download['caption']) ? 'caption' : 'sub-caption'); ?>"><?php echo $download['sub_caption']; ?></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="mid-body-pattern">
        </div>

        <?php if (!empty($fields['banner'])) : ?>
            <div class="cta-banners-widget">
                <div class="container">
                    <div class="items cf">
                        <?php foreach ($fields['banner'] as $banner) : ?>
                            <div class="item">
                                <a href="<?php echo $banner['link']['url']; ?>" target="<?php echo $banner['link']['target']; ?>" class="inner-wrapper lozad" data-background-image="<?php echo $banner['image']['url']; ?>">
                                    <div class="overlay">
                                        <?php if (!empty($banner['caption'])) : ?>
                                            <h4><?php echo $banner['caption']; ?></h4>
                                        <?php endif; ?>
                                        <?php if (!empty($banner['sub_caption'])) : ?>
                                            <p><?php echo $banner['sub_caption']; ?></p>
                                        <?php endif; ?>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php
        $news = \BlueBeetle\Press\Generic::get_instance()->get_regional_news($fields['show_news_by_location']);
        ?>

        <?php if (!empty($news)) : ?>
            <div class="news-widget">
                <div class="container">
                    <h3><?php _e('Regional News', 'bbtheme'); ?></h3>
                    <div class="items cf">
                        <?php 
                        $nskip_arr = array();
                        $news_count = 0;

                        foreach ($news as $news_index => $post) : ?>
                            <?php
                            $news_link = get_the_permalink($post);

                            if(in_array($post->ID, $nskip_arr)) :
                                continue;
                            endif;
                            
                            $news_lang = wpml_get_language_information($post->ID);
                            if(ICL_LANGUAGE_CODE != 'en') :
                                if($news_lang['language_code'] == 'en') :
                                    $news_tr_id = apply_filters('wpml_object_id', $post->ID, 'post', FALSE, ICL_LANGUAGE_CODE);
                                    if($news_tr_id!='' && $news_tr_id > 0) :
                                        $post = get_post($news_tr_id); 
                                        $news_link = apply_filters('wpml_permalink', $news_link , ICL_LANGUAGE_CODE);
                                        $nskip_arr[] = $news_tr_id;
                                    endif;
                                endif; 
                            else :
                                 if($news_lang['language_code'] != 'en') :
                                    continue;
                                endif;                              
                            endif;

                            $caption = get_field('caption', $post->ID);
                            $image = get_field('image', $post->ID);
                            $location = get_field('office', $post->ID);
                            $location_text = get_field('news_location_text', $location->ID);
                            $image_url = (isset($image['sizes']['news-thumb-02'])) ? $image['sizes']['news-thumb-02'] : $image['url'];
                            ?>
                            <a class="item" href="<?php echo $news_link; ?>">
                                <img src="<?php echo $image_url; ?>" alt="<?php echo $caption; ?>">
                                <h4><?php echo $caption; ?></h4>
                                <div class="date-n-location">
                                    <span><?php echo get_the_date('d M Y', $post); ?></span>
                                    <span><?php echo $location_text; ?></span>
                                </div>
                            </a>
                        <?php 
                            $news_count++;
                            if($news_count==3)
                                break;
                        endforeach;  ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>