window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        /* '.news-event-detail-widget': require('./partials/news-event-detail-widget') */
    }

    componentLoader.initActiveComponents(components);


    let newsDetailHandler = {
        init: () => {
            
            $('.lang_sel_box_link').on('click', function(e) {
                e.preventDefault();
                $('.langs_list_div').addClass('show');
                $('.lang_sel_div').addClass('hide');
            });

            $('.close_lang_box_link').on('click', function(e) {
                e.preventDefault();
                $('.lang_sel_div').removeClass('hide');
                $('.langs_list_div').removeClass('show');
            });

            $('.close_box .close_link').on('click', function(e) {
                e.preventDefault();
                $('.lang_sel_div').removeClass('hide');
                $('.langs_list_div').removeClass('show');
            });

        }
    }

    newsDetailHandler.init();


});