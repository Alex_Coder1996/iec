window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        /*'.become-partner-widget': require('./partials/become-partner-widget'),*/
        '#enquiry-form': require('./partials/enquiry-form')
    }

    componentLoader.initActiveComponents(components);

});