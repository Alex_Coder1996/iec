window.$ = window.jQuery = require('jquery');

let util = require('./modules/util'),
    componentLoader = require('./modules/componentLoader');

$(function () {

    require('./partials/common')();

    let components = {
        'body': require('./partials/common'),
        'board-members-widget': require('./partials/board-members-widget')
    }

    componentLoader.initActiveComponents(components);


    util.equalHeights('.board-members-widget .items .item');

});