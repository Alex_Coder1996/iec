window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.solutions-products-listing-widget': require('./partials/solutions-products-listing-widget')
    }

    componentLoader.initActiveComponents(components);

});