window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '#enquiry-form': require('./partials/enquiry-form'),
        '.need-accordions-widget': require('./partials/need-accordions-widget')
    }

    componentLoader.initActiveComponents(components);
});