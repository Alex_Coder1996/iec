let slick = require('slick-carousel');

module.exports = (sel) => {

    /* START: Hero Slider */

    let fadeEffect = true;
    let speed = 1200;
    let windowWidth = $(window).width();
    if(windowWidth <= 640)
    {
        fadeEffect = false;
        speed = 500;
    }

    $('.hero-slider-widget .slider').slick({
        rows: 0,
        appendArrows: $('.hero-slider-widget .arrows-wrapper'),
        autoplay: true,
        autoplaySpeed: 2000,
        speed: speed,
        fade: fadeEffect
    });

    /* END: Hero Slider */
}
