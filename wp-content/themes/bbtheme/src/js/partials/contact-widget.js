let util = require('../modules/util'),
    formValidation = require('../modules/formValidation'),
    select2 = require('select2');

module.exports = (sel) => {


    let change = () => {

        $('.addresses > .address', sel).removeClass('active');

        let value = $('#country', sel).val();

        if (value == '') {
            return false;
        }

        $('.addresses #' + value, sel).addClass('active');
    }

    $('#country', sel).on('change', function (e) {
        e.preventDefault();
        change();
    });

    change();

    $('.contact-widget #enquiry-form').on('change', function () {
        if (!$('#success-popup').hasClass('lity-hide')) {
            let url_aditional_text = '?' + $('#select2-country-container').prop('title').toLowerCase() + '-thank-you';
            window.history.replaceState(null, '', url_aditional_text);
        }
    });

    /* ----------------------- */

    if(! util.isMobile())
    {
        $('#country').select2({
            minimumResultsForSearch: -1,
            theme: 'large'
        });

        //$('.contact-widget .form-field select').select2({theme: 'grey-border'});
    }

    /*let contactForm = {
        hasRecaptcha: false,
        validationInstance: null,
        init: () => {

            contactForm.initValidation();

            if($('#google-captcha-holder').length)
            {
                contactForm.hasRecaptcha = true;

                contactForm.addRecaptchaScript();
                window.recaptchaOnloadCallback = function() {
                    grecaptcha.render('g-recaptcha', {
                        'sitekey' : $('#google-captcha-holder').attr('data-sitekey')
                    });
                };
            }

            $('#contact-form').on('validation_success', function (e) {
                e.preventDefault();

                $(".contact-widget form").addClass('loading').removeClass('success').removeClass('error');
                var data = $("#contact-form").serializeArray();
                var url = $("#contact-form").attr('action');
                var posting = $.post(url, data);
                posting.done(function (result) {
                    $(".contact-widget form").removeClass('loading').addClass('success');
                });
                posting.fail(function (jqXHR, textStatus, error) {
                    if(contactForm.hasRecaptcha)
                    {
                        grecaptcha.reset();
                    }
                    let errorNumber = jqXHR.responseJSON['err_no'];
                    $(".contact-widget form .error").html(error_messages[errorNumber]).show();
                    $(".contact-widget form").removeClass('loading').addClass('error');
                });
            });

        },
        addRecaptchaScript : () => {
            if($('#recaptchaScript').length == 0)
            {
                let tag = document.createElement('script');
                let lang = $('html').attr('lang');
                tag.id = 'recaptchaScript';
                tag.defer = true;
                tag.async = true;
                let src = "https://www.google.com/recaptcha/api.js?onload=recaptchaOnloadCallback&render=explicit";
                if(lang == 'ar')
                {
                    src += '&hl=ar'
                }
                tag.src = src;
                let firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            }
            else {
                recaptchaOnloadCallback();
            }
        },
        initValidation: () => {
            contactForm.validationInstance =  new formValidation.validate($('#contact-form'), (e) => {
                e.preventDefault();
                let form_validated = true;
                if($(e.currentTarget).find('.field-invalid').length) {
                    form_validated = false;
                }

                let captcha_validated = false;
                if(contactForm.hasRecaptcha)
                {
                    let google_response_text = $('textarea[name=g-recaptcha-response]').val();
                    if(google_response_text==''){
                        $('#google-captcha-holder').addClass('gcerror');
                    }else{
                        captcha_validated = true;
                        $('#google-captcha-holder').removeClass('gcerror');
                    }
                }
                else {
                    captcha_validated = true;
                }

                if (!form_validated || !captcha_validated) {
                    return false;
                }
                $(e.currentTarget).trigger('validation_success');
                return false;
            });
        }
    }

    contactForm.init();*/

}
