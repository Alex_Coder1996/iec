let util = require('../modules/util'),
    formValidation = require('../modules/formValidation'),
    select2 = require('select2');

module.exports = (sel) => {

    return false;

    if(! util.isMobile())
    {
        $('.become-partner-widget select').select2( {theme: 'grey-border'});
    }

    let becomePartnerForm = {
        hasRecaptcha: false,
        validationInstance: null,
        init: () => {

            becomePartnerForm.initValidation();

            if($('#google-captcha-holder').length)
            {
                becomePartnerForm.hasRecaptcha = true;

                becomePartnerForm.addRecaptchaScript();
                window.recaptchaOnloadCallback = function() {
                    grecaptcha.render('g-recaptcha', {
                        'sitekey' : $('#google-captcha-holder').attr('data-sitekey')
                    });
                };
            }

            $('#become-partner-form').on('validation_success', function (e) {
                e.preventDefault();

                $(".become-partner-widget form").addClass('loading').removeClass('success').removeClass('error');
                var data = $("#become-partner-form").serializeArray();
                var url = $("#become-partner-form").attr('action');
                var posting = $.post(url, data);
                posting.done(function (result) {
                    $(".become-partner-widget form").removeClass('loading').addClass('success');
                });
                posting.fail(function (jqXHR, textStatus, error) {
                    if(becomePartnerForm.hasRecaptcha)
                    {
                        grecaptcha.reset();
                    }
                    let errorNumber = jqXHR.responseJSON['err_no'];
                    $(".become-partner-widget form .error").html(error_messages[errorNumber]).show();
                    $(".become-partner-widget form").removeClass('loading').addClass('error');
                });
            });

        },
        addRecaptchaScript : () => {
            if($('#recaptchaScript').length == 0)
            {
                let tag = document.createElement('script');
                let lang = $('html').attr('lang');
                tag.id = 'recaptchaScript';
                tag.defer = true;
                tag.async = true;
                let src = "https://www.google.com/recaptcha/api.js?onload=recaptchaOnloadCallback&render=explicit";
                if(lang == 'ar')
                {
                    src += '&hl=ar'
                }
                tag.src = src;
                let firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            }
            else {
                recaptchaOnloadCallback();
            }
        },
        initValidation: () => {
            becomePartnerForm.validationInstance =  new formValidation.validate($('#become-partner-form'), (e) => {
                e.preventDefault();
                let form_validated = true;
                if($(e.currentTarget).find('.field-invalid').length) {
                    form_validated = false;
                }

                let captcha_validated = false;
                if(becomePartnerForm.hasRecaptcha)
                {
                    let google_response_text = $('textarea[name=g-recaptcha-response]').val();
                    if(google_response_text==''){
                        $('#google-captcha-holder').addClass('gcerror');
                    }else{
                        captcha_validated = true;
                        $('#google-captcha-holder').removeClass('gcerror');
                    }
                }
                else {
                    captcha_validated = true;
                }

                if (!form_validated || !captcha_validated) {
                    return false;
                }
                $(e.currentTarget).trigger('validation_success');
                return false;
            });
        }
    }

    becomePartnerForm.init();

}
