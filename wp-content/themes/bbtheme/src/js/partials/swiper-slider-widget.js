let Swiper = require('../modules/swiper');

module.exports = (sel) => {
    const swiper = new Swiper('.swiper-container', {
        effect: 'cube',
        loop: true,
        speed: 600,
        autoplay: {
            delay: 7500,
            disableOnInteraction: false,
        },
        cubeEffect: {
            shadow: false,
            slideShadows: false,
            shadowOffset: 0,
            shadowScale: 1,
        },
        navigation: {
            nextEl: '.slider-button-next',
            prevEl: '.slider-button-prev',
        },
    });
}
