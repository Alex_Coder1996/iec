let util = require('../modules/util'),
    select2 = require('select2'),
    resizeWatcher = require('../modules/resizewatcher'),
    formValidation = require('../modules/formValidation'),
    lity = require('lity');

module.exports = (sel) => {



    if (!util.isMobile()) {
        $('#location').select2({
            minimumResultsForSearch: -1,
            theme: 'medium'
        });
    }

    $('#location').on('change', (e) => {
        let value = e.currentTarget.value;

        if (value == 'all') {
            $('.vacancies-widget .item').removeClass('hidden');
        }
        else {
            $('.vacancies-widget .item:not(.' + value + ')').addClass('hidden');
            $('.vacancies-widget .item.' + value).removeClass('hidden');
        }

        if ($('.vacancies-widget .item:not(.hidden)').length == 0) {
            $('.vacancies-widget').addClass('no-results');
        }
        else {
            $('.vacancies-widget').removeClass('no-results');
        }
    });

    $('.items').on('click', '.item:not(.expand)', (e) => {
        $(e.currentTarget).addClass('expand');
    });

    $('.items').on('click', '.close', (e) => {
        e.preventDefault();
        $(e.currentTarget).closest('.item').removeClass('expand');
    });


    if ($('.items .item').length) {
        setTimeout(() => {
            $('html, body').animate({
                scrollTop: $('.items .item').offset().top - 20
            }, 500);
        }, 800);
    }



    let formValidations = {
        instances: {},
        init: () => {
            $('.items .item').each((i, el) => {
                formValidations.instances[i] = new formValidation.validate($(el).find('form'), (e) => {

                    e.preventDefault();
                    let form_validated = true;

                    if ($(e.currentTarget).find('.field-invalid').length) {
                        form_validated = false;
                    }

                    if (!form_validated) {
                        return false;
                    }

                    $(e.currentTarget).trigger('validation_success');
                    return true;

                });



                $(el).find('form').on('validation_success', function (e) {
                    e.preventDefault();

                    $(e.currentTarget).addClass('loading').removeClass('success').removeClass('error');
                    var data = new FormData(e.currentTarget);
                    var url = $(e.currentTarget).attr('action');

                    var posting = $.ajax({
                        type: "POST",
                        enctype: 'multipart/form-data',
                        url: url,
                        data: data,
                        processData: false,
                        contentType: false,
                        cache: false/* ,
                        timeout: 600000 */
                    });

                    posting.done(function (result) {

                        $(e.currentTarget).removeClass('loading');
                        if($('#success-popup').length)
                        {
                            lity('#success-popup');
                            e.currentTarget.reset();
                            $(e.currentTarget).find('input[type=file]').trigger('change');
                        }
                        else{
                            $("#enquiry-form").addClass('success');   
                        }

                    });
                    posting.fail(function (jqXHR, textStatus, error) {
                        
                        let errorNumber = jqXHR.responseJSON['err_no'];
                        $(e.currentTarget).find('.error').html(error_messages[errorNumber]);
                        $(e.currentTarget).removeClass('loading').addClass('error');
                        
                    });
                });
            });




        }
    };
    formValidations.init();





    /* ----------------------- */


    $('.file-upload input[type=file]').on('change', (e) => {

        let label = $(e.currentTarget).closest('.file-upload').find('label');

        let fileName = '';
        if (e.currentTarget.files.length > 0) {
            fileName = e.currentTarget.value.split('\\').pop();
        }
        else {
            fileName = label.attr('data-default-label');
        }

        if (fileName) {
            label.text(fileName);
        }
    });


    /* ----------------------- */


    let resizeHandler = {
        init: () => {
            let windowWidth = $(window).width();

            if (windowWidth <= 768) {
                $('.vacancies-widget .item').each((i, el) => {
                    $(el).find('.details').after($(el).find('.form'));
                });
            }
            else {
                $('.vacancies-widget .item').each((i, el) => {
                    $(el).find('.form').after($(el).find('.details'));
                });
            }
        }
    }
    resizeWatcher(resizeHandler.init, 100);
    resizeHandler.init();
}
