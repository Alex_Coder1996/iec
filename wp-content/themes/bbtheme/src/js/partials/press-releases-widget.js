let util = require("../modules/util"),
  select2 = require("select2"),
  resizeWatcher = require("../modules/resizewatcher");

module.exports = sel => {
  if (!util.isMobile()) {
    $("#filter-dropdown").select2({
      minimumResultsForSearch: -1,
      theme: "medium"
    });
  }

  $("#filter-dropdown").on("change", e => {
    let value = e.currentTarget.value;

    if (value == "all") {
      $(".press-releases .item").removeClass("d-hidden");
    } else {
      $(".press-releases .item:not(." + value + ")").addClass("d-hidden");
      $(".press-releases .item." + value).removeClass("d-hidden");
    }
  });

  $(".items").on("click", ".item", e => {
    if (!$(e.currentTarget).hasClass("expand")) {
      e.preventDefault();
      $(e.currentTarget).addClass("expand").siblings().removeClass('expand');
    }
  });

  $(".items").on("click", ".item .arrow", e => {
    let item = $(e.currentTarget).closest('.item');
    if (item.hasClass("expand")) {
      e.preventDefault();
      e.stopPropagation();
      $(e.currentTarget)
        .closest(".item")
        .removeClass("expand");
    }
    else {
      item.addClass("expand").siblings().removeClass('expand');
    }
  });

  $(".items").on("click", ".item:not(.expand) .download", e => {
    e.stopPropagation();
  });

  let keywordSearch = keyword => {
    let keywords = keyword.split(" ");
    $(".items .item").each((i, el) => {
      let title = $(el)
        .find("h4")
        .text();

      keywords.forEach(function(keywordP, index) {
        let keywordPSearchPatt = new RegExp(keywordP, "i");

        if (keywordP.length && !keywordPSearchPatt.test(title)) {
          $(el).addClass("k-hidden");
        } else {
          $(el).removeClass("k-hidden");
        }
      });
    });
  };
  let keywordSearchTimeout = null;
  $("#keyword").on("keyup", e => {
    let keyword = e.currentTarget.value;
    if (keyword.length > 5 || keyword.length == 0) {
      $(".search-field").addClass("has-keyword");
      clearTimeout(keywordSearchTimeout);
      keywordSearchTimeout = setTimeout(() => {
        keywordSearch(keyword);
      }, 500);
    }
  });

  $(".search-field").on("click", ".close", e => {
    e.preventDefault();
    $(".search-field").removeClass("has-keyword");
    $("#keyword").val("");
    keywordSearch("");
  });

  $("#filters-form").on("keyup keypress", function(e) {
    let keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });

  /* ----------------------- */

  let resizeHandler = {
    init: () => {
      let windowWidth = $(window).width();

      if (windowWidth <= 768) {
        $(".press-releases .item").each((i, el) => {
          $(el)
            .find(".date")
            .before($(el).find(".description"));
        });
      } else {
      }
    }
  };
  resizeWatcher(resizeHandler.init, 100);
  resizeHandler.init();
};
