let slick = require('slick-carousel');

module.exports = (sel) => {

    /* START: General Slider */

    $('.image-slider-widget .slider').slick({
        rows: 0,
        fade: true,
        speed: 800
    });

    /* END: General Slider */
}
