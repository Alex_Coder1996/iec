module.exports = (sel) => {

    let change = () => {

        $('.tab-contents > div', sel).removeClass('active');

        let href = $('.tab-links a.active:not(.disabled)', sel).attr('href');

        if (href == '#') {
            return false;
        }

        $('.tab-contents ' + href, sel).addClass('active');
    }


    $('.tab-links', sel).on('click', 'a', function (e) {
        e.preventDefault();

        if ($(e.currentTarget).hasClass('disabled')) {
            return false;
        }

        $(e.currentTarget).addClass('active').siblings().removeClass('active');
        change();
    });


    if ($('.tab-links a.active', sel).length == 0) {
        $('.tab-links a:first-child', sel).addClass('active').siblings().removeClass('active');
    }

    if ($('.tab-links a', sel).length == 1) {
        $('.tab-links', sel).hide();
    }

    change();
}