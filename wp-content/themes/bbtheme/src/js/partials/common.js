let resizeWatcher = require('../modules/resizewatcher'),
  polyfill = require('../partials/object-assign-polyfill'),
  lozad = require('lozad'),
  util = require('../modules/util');

module.exports = (sel) => {
  if (util.hasTouch()) {
    $('html').addClass('touch');
  }

  /* START: Lozad Loading */

  window.observer = lozad(); // lazy loads elements with default selector as '.lozad'
  observer.observe();

  /* END: Lozad Loading */

  /* START : Cookie Notification */

  var cookieNotice = {
    init: () => {
      $('#cookie-notice .accept').on('click', (e) => {
        e.stopPropagation();
        e.preventDefault();
        util.setCookie('cookie_notice_accepted', true, 10 * 365);
        $('#cookie-notice').removeClass('expand');
        location.reload();
      });

      $('#cookie-notice .reject').on('click', (e) => {
        e.stopPropagation();
        e.preventDefault();
        util.setCookie('cookie_notice_accepted', false, 10 * 365);
        $('#cookie-notice').removeClass('expand');
        location.reload();
      });

      var cookieNoticeAcknowledged = util.readCookie('cookie_notice_accepted');
      if (cookieNoticeAcknowledged == null) {
        setTimeout(function() {
          $('#cookie-notice').addClass('expand');
        }, 300);
      } else {
        if (typeof initGTM == 'function') {
      
          // Call init GTM after 4 seconds, or on bellow events to start tracking ASAP.
          setTimeout(initGTM, 4000);

          document.addEventListener('scroll', initGTMOnEvent);
          document.addEventListener('mousemove', initGTMOnEvent);
          document.addEventListener('touchstart', initGTMOnEvent);
          
          function initGTMOnEvent (event) {
            initGTM();
            event.currentTarget.removeEventListener(event.type, initGTMOnEvent); 
          }
        
        }
      }
    },
  };

  if ($('#cookie-notice').length) {
    cookieNotice.init();
  }

  /* END : Cookie Notification */

  /* ----------------------- */

  /* START: Navigation Sub Menu */

  $('.menu-item-has-children > a').on('click', (e) => {
    $(e.currentTarget)
      .closest('.sub-menu')
      .toggleClass('open');
  });

  /* END: Navigation Sub Menu */

  /* ----------------------- */

  /* START: Search Form */

  let searchForm = {
    init: () => {
      if ($(window).width() > 640) {
        $('.search-form input[type=text]').on('focus', (e) => {
          $('.search-form').addClass('expand');
        });

        $('.search-form input[type=text]').on('blur', () => {
          $('.search-form').removeClass('expand');
        });
      }

      $('.search-form input[type=text]').on('keyup', (e) => {
        if (e.currentTarget.value.length > 4) {
          $('.search-form').addClass('valid');
        } else {
          $('.search-form').removeClass('valid');
        }
      });
    },
  };

  searchForm.init();

  /* END: Search Form */

  /* ----------------------- */

  /* START: Hamburger */

  $('#hamburger').on('click', () => {
    $('#header').toggleClass('expand-menu');
  });

  /* END: Hamburger */

  /* ----------------------- */

  $('.show-on-load').removeClass('show-on-load');

  /* ----------------------- */

  $('.history-back').on('click', (e) => {
    if (history.length > 1) {
      e.preventDefault();
      history.back();
    }
  });

  /*----------------------*/

  if ($('.minimum-height').length) {
    let height = $(document).height();
    $('#main').css('min-height', height - ($('#header').height() + $('#footer').height()));
  }

  let resizeHandler = {
    init: () => {
      resizeHandler.navigation();
    },
    navigation: () => {
      let windowWidth = $(window).width();

      if (windowWidth <= 960) {
        $('#header .responsive-nav').append($('#header .nav-wrapper'));
        $('#header .responsive-nav').append($('#header .secondary-nav-wrapper'));
      } else {
        $('#header .top-wrapper').after($('#header .nav-wrapper'));
        $('#header .right-wrapper').append($('#header .secondary-nav-wrapper'));
      }
    },
    searchForm: () => {
      let windowWidth = $(window).width();

      if (windowWidth <= 640) {
        $('#header .responsive-nav').prepend($('#header .search-form'));
      } else {
        $('#header .right-wrapper').prepend($('#header .search-form'));
      }
    },
  };
  resizeWatcher(resizeHandler.init, 100);
  resizeHandler.init();
  resizeHandler.searchForm();
};
