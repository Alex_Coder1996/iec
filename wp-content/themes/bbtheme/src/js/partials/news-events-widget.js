let util = require('../modules/util');

module.exports = (sel) =>
{
    /* START: Filters */

    let listingFilters = {
        keywordChangeTimeout: null,
        toggleFieldChangeTimeout: null,
        initialFormStates: {},
        setUrl: () => {
            
            let keyword = $('#keyword').val();
            let category = $('#filters-form .categories input[type=checkbox]:checked');
            let categoryId = null;
            if(category.length)
            {
                categoryId = category.attr('id').replace('filter-', '');
            }

            let filters = [];
            
            if(keyword.length)
            {
                filters.push('keyword/' + keyword);
            }

            if(categoryId != 'all' && categoryId != null)
            {
                filters.push('filter/' + categoryId);
            }

            if(filters.length)
            {
                history.pushState(null, null, '#' + filters.join('/'));
            }
            else {
                history.pushState(null, null, window.location.pathname);
            }
            
        },
        setFilters: () => {

            let hash = window.location.hash.replace('#', '');
            let params = {};
            let urlParts = hash.split('/');

            for(let i=0;i<urlParts.length;i=i+2)
            {
                params[urlParts[i]] = urlParts[i+1];
            }

            for(let i in params)
            {
                if(i == 'keyword')
                {
                    $('#keyword').val(decodeURIComponent(params[i]));
                    $('#filters-form .search-field').addClass('has-keyword')
                }
                else {
                    $('#filter-' + params[i]).prop('checked', true);
                }
            }

            listingFilters.checkFilterAll();

        },
        fetchItems: (fresh) =>
        {
            listingFilters.setUrl();

            if (typeof fresh == 'undefined')
            {
                fresh = true;
            }

            if (fresh)
            {
                $('#filters-form').find('input[name=start-at]').val(0);
                $('.news-events-widget').addClass('loading');
            }

            let data = null;
            data = $('#filters-form').serialize();

            $.post(CONFIG.newsEventsAPI, data)
                .done((data) =>
                {
                    if (fresh)
                    {
                        $('.listing .items').empty();
                    }

                    if (typeof data['items'] != 'undefined' && data['items'].length != 0)
                    {
                        $('.news-events-widget').removeClass('no-results no-more');

                        data['items'].forEach((item) =>
                        {
                            listingFilters.appendItem(item);
                        });

                        $('#filters-form')
                            .find('input[name=start-at]')
                            .val(data['startAt']);
                        util.equalHeights('.news-events-widget .item');
                    } else
                    {
                        $('.news-events-widget').addClass('no-results no-more');
                    }

                    if (!data['hasMore'])
                    {
                        $('.news-events-widget').addClass('no-more');
                    }

                    observer.observe();
                })
                .fail((data) => { })
                .always((data) =>
                {
                    $('.news-events-widget').removeClass('loading');
                    $('.load-more').removeClass('loading');
                });
        },
        appendItem: (data) =>
        {
            let item = $('<div />', {
                class: 'item'
            });

            let anchor = $('<a />', {
                href: data.href,
                class: 'inner-wrapper'
            });

            let thumbWrapper = $('<div />', {
                class: 'thumb-wrapper'
            });

            let img = $('<div />', {
                class: 'img lozad'
            }).attr('data-background-image', data.thumb);

            /* let cat = $('<div />', {
                class: 'cat',
                text: data.cat
            }); */

            thumbWrapper.append(img); //.append(cat);
            anchor.append(thumbWrapper);

            let title = $('<h5 />', {
                text: data.title
            });
            anchor.append(title);

            let dateLocation = $('<div />', {
                class: 'date-n-location',
                html: '<span>' + data.date + '</span> <span>' + data.location + '</span>'
            });

            anchor.append(dateLocation);

            item.append(anchor);

            $('#items-listing .items').append(item);
        },
        setFilterAll: () =>
        {
            $('#filter-all').prop('checked', true);
            $('#filters-form input[type=checkbox]:not(#filter-all)').prop('checked', false);
            $('#filters-form input[name=keyword]').val('');
            $('#filters-form .search-field').removeClass('has-keyword');

        },
        checkFilterAll: () =>
        {

            if (
                $('#filters-form input[type=checkbox]:checked').length ||
                $('#filters-form input[name=keyword]').val().length
            )
            {
                $('#filter-all').prop('checked', false);
            } else
            {
                $('#filter-all').prop('checked', true);
                listingFilters.setFilterAll();
            }
        },
        init: () =>
        {
        
            listingFilters.setFilters();
            listingFilters.fetchItems();

            $('#filters-form input[type=checkbox]').on('change', (e) =>
            {
                // Allow only one cateogry filter to be active at a time.
                if ($(e.currentTarget).is(':checked'))
                {
                    $('#filters-form input[type=checkbox]')
                        .not(e.currentTarget)
                        .prop('checked', false);
                }

                clearTimeout(listingFilters.toggleFieldChangeTimeout);
                listingFilters.toggleFieldChangeTimeout = setTimeout(function ()
                {
                    if ($(e.currentTarget).attr('id') == 'filter-all')
                    {
                        listingFilters.setFilterAll();
                    } else
                    {
                        listingFilters.checkFilterAll();
                    }

                    listingFilters.fetchItems();
                }, 300);
            });

            $('#filters-form .search-field input[type=text]').on('keyup', (e) =>
            {
                let keyword = e.currentTarget.value;
                let keyCode = e.keyCode || e.which;

                if (keyword.length >= 3 || keyword.length == 0 || keyCode == 13)
                {
                    $('.search-field').addClass('has-keyword');
                    clearTimeout(listingFilters.keywordChangeTimeout);
                    listingFilters.keywordChangeTimeout = setTimeout(function ()
                    {
                        listingFilters.checkFilterAll();
                        listingFilters.fetchItems();
                    }, 500);
                }
            });

            $('.search-field').on('click', '.close', (e) =>
            {
                e.preventDefault();
                $('.search-field').removeClass('has-keyword');
                $('#filters-form input[name=keyword]').val('');
                listingFilters.checkFilterAll();
                listingFilters.fetchItems();
            });

            $('#filters-form').on('keyup keypress', function (e)
            {
                let keyCode = e.keyCode || e.which;
                if (keyCode === 13)
                {
                    e.preventDefault();
                    return false;
                }
            });

            $('.load-more a').on('click', (e) =>
            {
                e.preventDefault();
                $('.load-more').addClass('loading');
                listingFilters.fetchItems(false);
            });

            $('#filters-form').each((i, el) =>
            {
                listingFilters.initialFormStates['#filters-form'] = $(
                    'input[name!=start-at]',
                    $('#filters-form')
                ).serialize();
            });
        }
    };

    listingFilters.init();

    /* END: Filters */
};
