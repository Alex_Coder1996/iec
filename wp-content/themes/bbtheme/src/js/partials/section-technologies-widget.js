module.exports = (sel) => {
    $('#transfer_img').on('click', videoPlayTrigger);

    $('#stream_img').on('click', videoPlayTrigger);

    function videoPlayTrigger () {
        let video = $(this).find('.video').get(0);
        let button = $(this).find('.img_button');
        if (video.paused) {
            video.play();
            button.hide();
        } else {
            video.pause();
            button.show();
        }
    }
}