let util = require('../modules/util'),
    select2 = require('select2'),
    formValidation = require('../modules/formValidation'),
    lity = require('lity');

module.exports = (sel) => {


    let enquiryForm = {
        hasRecaptcha : false, 
        validationInstance: null,
        init: () => {

            enquiryForm.initValidation();

            let select2selector = null;
            if(! util.isMobile())
            {
                select2selector = '#enquiry-form .form-field .select2';
            }
            else 
            {
                select2selector = '#enquiry-form .form-field .select2-interest';
            }


            if(select2selector != null)
            {
                $(select2selector).each((i, el) => {
                    let theme = $(el).attr('data-select2-theme');
                    if (typeof theme == 'undefined')
                    {
                        theme = 'default';
                    }
                    
                    let config = {};
                    config['theme'] = theme;

                    if($(el).attr('data-select2-placeholder') != 'undefined')
                    {
                        config['placeholder'] = $(el).attr('data-select2-placeholder');
                    }

                    if($(el).attr('data-select2-hide-search') == 'true')
                    {
                        config['minimumResultsForSearch'] = -1;
                    }

                    $(el).select2(config);
                });
            }

            if($('#google-captcha-holder').length)
            {
                enquiryForm.hasRecaptcha = true;

                enquiryForm.addRecaptchaScript();
                window.recaptchaOnloadCallback = function() {
                    grecaptcha.render('g-recaptcha', {
                        'callback' : function(val) {
                            $('#google-captcha-holder').removeClass('gcerror');
                        },
                        'sitekey' : $('#google-captcha-holder').attr('data-sitekey')
                    });
                };
            }

            $('.expand-on-click').on('keyup', (e) => {
                e.preventDefault();
                if(e.currentTarget.value.length > 10)
                {
                    $(e.currentTarget).addClass('expand');
                    $('.expand-on-click').off('keyup');
                }
            });

            $('#enquiry-form').on('validation_success', function (e) {
                e.preventDefault();


                $("#enquiry-form").addClass('loading').removeClass('success').removeClass('error');
                var data = $("#enquiry-form").serializeArray();
                var url = $("#enquiry-form").attr('action');
                var posting = $.post(url, data);
                posting.done(function (result) {
                    $("#enquiry-form").removeClass('loading');
                    if($('#success-popup').length)
                    {
                        lity('#success-popup');
                        $('#enquiry-form')[0].reset();
                        grecaptcha.reset();
                        
                        if(! util.isMobile())
                        {
                            $('#enquiry-form .form-field .select2').trigger('change');
                        }
                        else 
                        {
                            $('#enquiry-form .form-field .select2-interest').trigger('change');
                        }

                    }
                    else{
                        $("#enquiry-form").addClass('success');   
                    }
                
                });
                posting.fail(function (jqXHR, textStatus, error) {
                    if(enquiryForm.hasRecaptcha)
                    {
                        grecaptcha.reset();
                    }
                    let errorNumber = jqXHR.responseJSON['err_no'];
                    $("#enquiry-form .error").html(error_messages[errorNumber]);
                    $("#enquiry-form").removeClass('loading').addClass('error');
                });
            });

        },
        addRecaptchaScript : () => {
            if($('#recaptchaScript').length == 0)
            {
                let tag = document.createElement('script');
                let lang = $('html').attr('lang');
                tag.id = 'recaptchaScript';
                tag.defer = true;
                tag.async = true;
                let src = "https://www.google.com/recaptcha/api.js?onload=recaptchaOnloadCallback&render=explicit";
                if(lang == 'ar')
                {
                    src += '&hl=ar'
                }
                tag.src = src;
                let firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            }
            else {
                recaptchaOnloadCallback();
            }
        },
        initValidation: () => {
            enquiryForm.validationInstance =  new formValidation.validate($('#enquiry-form'), (e) => {
                e.preventDefault();
                let form_validated = true;
                if($(e.currentTarget).find('.field-invalid').length) {
                    form_validated = false;
                }

                let captcha_validated = false;
                if(enquiryForm.hasRecaptcha)
                {
                    let google_response_text = $('textarea[name=g-recaptcha-response]').val();
                    if(google_response_text==''){
                        $('#google-captcha-holder').addClass('gcerror');
                    }else{
                        captcha_validated = true;
                        $('#google-captcha-holder').removeClass('gcerror');
                    }
                }
                else {
                    captcha_validated = true;
                }

                if (!form_validated || !captcha_validated) {
                    return false;
                }
                $(e.currentTarget).trigger('validation_success');
                return true;
            });
        }
    }

    enquiryForm.init();

}