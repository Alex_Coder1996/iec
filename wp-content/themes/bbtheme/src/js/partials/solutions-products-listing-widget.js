let resizeWatcher = require('../modules/resizewatcher'),
util = require('../modules/util');

module.exports = (sel) => {

    /* START: Filters */

    let listingFilters = {
        keywordChangeTimeout: null,
        toggleFieldChangeTimeout: null,
        initialFormStates: {},
        setUrl: () => {
            
            let keyword = $('#keyword').val();
            let filters = $('#filters-form .toggle-field input[type=checkbox]:checked');
            let filterKeys = [];
            let urlParams = [];

            if(filters.length)
            {
                filters.each((i, el) => {
                    filterKeys.push($(el).attr('data-key'));
                });
            }

            if(keyword.length)
            {
                urlParams.push('keyword/' + keyword);
            }

            if(filterKeys.length)
            {
                urlParams.push('filter/' + filterKeys.join('-'));
            }

            if(urlParams.length)
            {
                history.pushState(null, null, '#' + urlParams.join('/'));
            }
            else {
                history.pushState(null, null, window.location.pathname);
            }
            
        },
        setFilters: () => {
            
            let hash = window.location.hash.replace('#', '');
            let params = {};
            let urlParts = hash.split('/');

            for(let i=0;i<urlParts.length;i=i+2)
            {
                if(urlParts[i] != "" && typeof urlParts[i+1] != 'undefined')
                {
                    params[urlParts[i]] = urlParts[i+1];
                }
            }

            for(let i in params)
            {
                if(i == 'keyword')
                {
                    $('#keyword').val(decodeURIComponent(params[i]));
                    $('#filters-form .search-field').addClass('has-keyword');
                }
                else if(i == 'filter') {
                    params[i].split('-').forEach((el, i) => {
                        $('#filters-form .toggle-field [data-key=' + el + ']').prop('checked', true);
                    });
                }
            }

        },
        fetchProducts: (fresh) => {

            listingFilters.setUrl();

            if (typeof fresh == 'undefined') {
                fresh = true;
            }

            if (fresh) {
                $('#filters-form').find('input[name=start-at]').val(0);
                $('.solutions-products-listing-widget').addClass('loading');
            }

            let data = null;
            data = $('#filters-form').serialize();

            $.post(CONFIG.productsSolutionAPI, data).done((data) => {
                if (fresh) {
                    $('.listing .items').empty();
                }

                if (typeof data['items'] != 'undefined' && data['items'].length != 0) {
                    $('.solutions-products-listing-widget').removeClass('no-results no-more');

                    data['items'].forEach((item) => {
                        listingFilters.appendProduct(item);
                    });

                    if($(window).width() > 570)
                    {
                        util.equalHeights('.solutions-products-listing-widget .items .item');
                    }
                    $('#filters-form').find('input[name=start-at]').val(data['startAt']);

                } else {
                    $('.solutions-products-listing-widget').addClass('no-results no-more');
                }

                if (!data['hasMore']) {
                    $('.solutions-products-listing-widget').addClass('no-more');
                }

                observer.observe();

            }).fail((data) => {

            }).always((data) => {
                $('.solutions-products-listing-widget').removeClass('loading');
                $('.load-more').removeClass('loading');
            })
        },
        appendProduct: (data) => {

            let item = $('<div />', {
                class: 'item ' + data.type
            });


            let anchor = $('<a />', {
                href: data.href,
                class: 'inner-wrapper'
            });


            let thumbWrapper = $('<div />', {
                class: 'thumb-wrapper'
            });

            let img = $('<div />', {
                class: 'img lozad'
            }).attr('data-background-image', data.thumb);

            let cats = $('<div />', {
                class: 'cats'
            });

            data.cats.forEach((catObj) => {

                let catEl = $('<div />', {
                    class: catObj.cat
                });

                let textEl = $('<div />', {
                    text: catObj.label
                });

                catEl.append(textEl);

                cats.append(catEl);
            });

            if(data.cats.length > 1)
            {
                cats.addClass('cats-2');
            }

            thumbWrapper.append(img).append(cats);
            anchor.append(thumbWrapper)

            let title = $('<h6 />', {
                text: data.title
            });
            anchor.append(title);

            item.append(anchor);

            $('#items-listing .items').append(item);

        },
        init: () => {

            listingFilters.setFilters();
            listingFilters.fetchProducts();

            $('#filters-form input[type=checkbox]').on('change', (e) => {

                clearTimeout(listingFilters.toggleFieldChangeTimeout);
                listingFilters.toggleFieldChangeTimeout = setTimeout(function () {
                    listingFilters.fetchProducts();
                }, 300);
            });

            $('#filters-form .search-field input[type=text]').on('keyup', (e) => {
                
                let keyword = e.currentTarget.value;
                let keyCode = e.keyCode || e.which;

                if (keyword.length >= 3  || keyword.length == 0 || keyCode == 13) {
                    $('.search-field').toggleClass('has-keyword', keyword.length>0);
                    clearTimeout(listingFilters.keywordChangeTimeout);
                    listingFilters.keywordChangeTimeout = setTimeout(function () {
                        listingFilters.fetchProducts();
                    }, 500);
                }
            });

            $('#filters-form').on('submit', (e) => {
                e.preventDefault();
                listingFilters.fetchProducts();
            });

            $('.load-more a').on('click', (e) => {
                e.preventDefault();
                $('.load-more').addClass('loading');
                listingFilters.fetchProducts(false);
            });

            $('.filters-group').on('click', 'h6', (e) => {
                e.preventDefault();
                $(e.currentTarget).closest('.filters-group').toggleClass('expand');
            });

            $('#filters-form').each((i, el) => {
                listingFilters.initialFormStates["#filters-form"] = $('input[name!=start-at]', $('#filters-form')).serialize();
            });

            $('.search-field').on('click', '.close', (e) =>
            {
                e.preventDefault();
                $('.search-field').removeClass('has-keyword');
                $('#filters-form input[name=keyword]').val('');
                listingFilters.fetchProducts();
            });

            /* $('#filters-form').on('keyup keypress', function (e)
            {
                let keyCode = e.keyCode || e.which;
                if (keyCode === 13)
                {
                    e.preventDefault();
                    return false;
                }
            }); */
        }
    }

    listingFilters.init();

    /* END: Filters */


    let resizeHandler = {
        init: () => {
            let windowWidth = $(window).width();

            if(windowWidth <= 900)
            {
                $('.filters-group').each((i, el) => {
                    $('.filters-form .filters').append($(el));
                });

                if(windowWidth > 580)
                {
                    util.equalHeights('.filters-group');
                }
                else {
                    $('.filters-group').height('');
                }
            }
            else {

                $('.filters-group').each((i, el) => {

                    let col = '';
                    if($(el).hasClass('first'))
                    {
                        col = 'first';
                    }
                    else if($(el).hasClass('second'))
                    {
                        col = 'second';
                    }
                    else if($(el).hasClass('third'))
                    {
                        col = 'third';
                    }

                    $('.filters-form .filters .col.' + col).append($(el));

                });

                $('.filters-group').height('');

            }
        }
    }
    resizeWatcher(resizeHandler.init, 100);
    resizeHandler.init();
}