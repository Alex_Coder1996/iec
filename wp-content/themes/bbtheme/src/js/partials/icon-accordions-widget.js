let lity = require('lity');

module.exports = (sel) => {

    let openAccordion = (item) => {
        item.closest('.icon-accordions-widget').find(' > .content').remove();
        item.closest('.icon-accordions-widget').find('.item.active').removeClass('active');
        item.addClass('active');
        item.closest('.container').after(item.find('.content').clone());
    }
    let closeAccordion = (item) => {
        item.closest('.icon-accordions-widget').find('.item.active').removeClass('active');
        item.closest('.icon-accordions-widget').find(' > .content').remove();
    }

    $('.icon-accordions-widget .item').on('click', (e) => {
        let item = $(e.currentTarget);
        if(item.hasClass('active'))
        {
            closeAccordion(item);
        }
        else {
            openAccordion(item);
        }
    });

    $('.icon-accordions-widget').each((i, el) => {

        let iconAccordions = $(el);
        let items = iconAccordions.find('.item');

        iconAccordions.find('.items > .container').addClass('old');

        let itemWidth = items.first().outerWidth();
        let parentWidth = iconAccordions.find('.items').width();
        let itemsInRow = Math.floor(parentWidth/itemWidth);

        let itemsEl;
        items.each((i, el) => {
            if((i + 1) % itemsInRow == 1 || itemsInRow == 1)
            {
                let containerEl = $('<div />', {
                    class: 'container'
                });

                itemsEl = $('<div />', {
                    class: 'items cf'
                });

                containerEl.append(itemsEl);

                iconAccordions.append(containerEl);
            }

            itemsEl.append($(el));
        });

        iconAccordions.find('.old').remove();

        if(iconAccordions.find('.item.active').length)
        {
            openAccordion(iconAccordions.find('.item.active'));
        }
    });


    /* ----------------------- */


    /* START: Coverage Map Popup */

    let openLityh = null;
    $('body').on('click', '.view-map', (e) => {
        let map = $(e.currentTarget).attr('data-map');
        $('.coverage-map-popup img').attr('src', map);
        openLityh = lity('.coverage-map-popup');
    });


    $('body').on('click', '.coverage-map-popup .close', (e) => {
        openLityh.close();
    });

    /* END: Coverage Map Popup */

}
