let slick = require('slick-carousel');

module.exports = (sel) => {

    /* START: General Slider */

    $('.general-slider-widget .slider').slick({
        rows: 0,
        appendArrows: $('.general-slider-widget .arrows-wrapper'),
        fade: true,
        speed: 800
    });

    /* END: General Slider */
}
