window.$ = window.jQuery = require('jquery');

let util = require('./modules/util'),
    componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common')
    }

    componentLoader.initActiveComponents(components);

    if($(window).width() > 640)
    {
        util.equalHeights('.our-offerings-widget .inner-wrapper');
    }

});