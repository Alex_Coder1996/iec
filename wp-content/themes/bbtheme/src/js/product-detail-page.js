window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '#enquiry-form': require('./partials/enquiry-form'),
        '.image-slider-widget': require('./partials/image-slider-widget'),
        '.need-accordions-widget': require('./partials/need-accordions-widget')
    }

    componentLoader.initActiveComponents(components);
});