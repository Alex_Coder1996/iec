window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader'),
resizeWatcher = require('./modules/resizewatcher'),
    util = require('./modules/util');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.general-slider-widget': require('./partials/general-slider-widget'),
        '#enquiry-form': require('./partials/enquiry-form'),
        '.address-tabs-widget': require('./partials/address-tabs-widget')
    }

    componentLoader.initActiveComponents(components);



    let resizeHandler = {
        init: () => {
            let windowWidth = $(window).width();

            if(windowWidth <= 1000)
            {
                $('.columns .left').after($('.mid-body-pattern'));
            }
            else
            {
                $('.columns').after($('.mid-body-pattern'));
            }

            $('.lang_sel_box_link').on('click', function(e) {
                e.preventDefault();
                $('.langs_list_div').addClass('show');
                $('.lang_sel_div').addClass('hide');
            });

            $('.close_lang_box_link').on('click', function(e) {
                e.preventDefault();
                $('.lang_sel_div').removeClass('hide');
                $('.langs_list_div').removeClass('show');
            });

            $('.close_box .close_link').on('click', function(e) {
                e.preventDefault();
                $('.lang_sel_div').removeClass('hide');
                $('.langs_list_div').removeClass('show');
            });
            

            

        }
    }
    resizeWatcher(resizeHandler.init, 100);
    resizeHandler.init();



    /* START : Language Notification */

    var cookieLangNotice = {
        init: () => { 
            var lang_val = 'en';
            var switch_url = '#';
            $('.lang_buttons_div .switch_lang_cookie_link').on('click', (e) => {
                e.preventDefault();
                lang_val = $('.lang_buttons_div .switch_lang_cookie_link').data('langval');
                switch_url = $('.lang_buttons_div .switch_lang_cookie_link').attr('href');
            });

            var office_slug = $('#office_page_slug').data('office-slug');

            $('.lang_buttons_div .lang_cookie_link').on('click', (e) => {
                e.preventDefault();
                util.setCookie('lang_cookie_notice_accepted_'+office_slug, lang_val, 7);
                $('#lang_banner_block').hide();
                $('.lang_sel_div').show();
                if(switch_url!='#') {
                    location.replace(switch_url);
                }
            });


            $('.langs_list_div .switch_lang_box_link').on('click', (e) => {
                e.preventDefault();
                lang_val = $('.langs_list_div .switch_lang_box_link').data('langval');
                switch_url = $('.langs_list_div .switch_lang_box_link').attr('href');
            });

            $('.langs_list_div .switch_lang_box_link').on('click', (e) => {
                e.preventDefault();
                util.setCookie('lang_cookie_notice_accepted_'+office_slug, lang_val, 7);
                $('.lang_sel_div').removeClass('hide');
                $('.langs_list_div').removeClass('show');
                if(switch_url!='#') {
                    location.replace(switch_url);
                }
            });





            var cookieNoticeAcknowledged = util.readCookie('lang_cookie_notice_accepted_'+office_slug);
            if (cookieNoticeAcknowledged == null) {  
                setTimeout(function() {
                    $('#lang_banner_block').show();
                }, 300);
            }
            else {                 
                $('.lang_sel_div').show();
                /*var curr_page_lang = $('#curr_page_lang').data('curr_page_lang');
                if (cookieNoticeAcknowledged != 'en' && cookieNoticeAcknowledged != curr_page_lang) {  
                    switch_url = $('.lang_buttons_div .switch_lang_cookie_link').attr('href');
                    location.replace(switch_url);
                }*/
            }
        }
    }
    
    cookieLangNotice.init();
    
    /* END : Language Notification */


});