window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.contact-widget' : require('./partials/contact-widget'),
        '#enquiry-form': require('./partials/enquiry-form')
    }

    componentLoader.initActiveComponents(components);

});