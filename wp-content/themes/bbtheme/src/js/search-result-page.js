window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.search-result-widget': require('./partials/search-result-widget'),
    }

    componentLoader.initActiveComponents(components);

});