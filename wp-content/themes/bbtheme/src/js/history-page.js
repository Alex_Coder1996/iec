window.$ = window.jQuery = require('jquery');

let util = require('./modules/util'),
    componentLoader = require('./modules/componentLoader');

$(function () {


    let components = {
        'body': require('./partials/common')
    }

    componentLoader.initActiveComponents(components);



    /* START: Connecting Line */

    let drawConnectingLine = () => {
        $('.line').css('top', $('.item.first').position().top + 12);

        if($(window).width() <= 640)
        {
            $('.line').css('bottom', $('.item.last').outerHeight() - 56 - 13);
        }
        else {
            $('.line').css('bottom', $('.item.last').outerHeight() - 90 - 13);
        }
    }

    setTimeout(drawConnectingLine, 300);
    /* END: Connecting Line */

});