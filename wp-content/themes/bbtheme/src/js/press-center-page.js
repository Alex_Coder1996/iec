window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.press-releases-widget' : require('./partials/press-releases-widget')
    }

    componentLoader.initActiveComponents(components);

});