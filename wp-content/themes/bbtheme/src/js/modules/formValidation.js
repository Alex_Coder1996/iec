let formValidation = {
    getFieldInvalidMsg: function ($field) {
        // Determine how to validate form fields
        
        // Check that the value is not blank
        var comparators = [{
            validator: function ($el) {
                return typeof $el.val() !== 'undefined' && $el.val().length > 0;
            },
            msg: $field.data('invalid')
        }];
        
        // If it's an email field, make sure to validate the address
        if ($field.attr('type') === 'email') {
            comparators.push({
                validator: function ($el) {
                    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($el.val());
                },
                msg: ""
            });
        }
        
        if ($field.attr('type') === 'checkbox') {
            comparators.push({
                validator: function ($el) {
                    return $el.prop("checked") === true;
                },
                msg: ""
            })
        }
        
        if ($field.attr('type') === 'number') {
            comparators.push({
                validator: function ($el) {
                    var _val = $el.val();
                    return _val === parseInt(_val, 10);
                },
                msg: ""
            });
        }
        
        if ($field.attr('type') === 'tel') {
            comparators.push({
                validator: function ($el) {
                    return /^\+*[\d\s]+$/im.test($el.val())
                },
                msg: ""
            });
        }

        if ($field.hasClass('confirm-password')) {
            comparators.push({
                validator: function ($el) {
                    var password = $($el.attr('data-password-field-selector')).val();
                    return password == $el.val()
                },
                msg: ""
            });
        }

        for (var i = 0; i < comparators.length; i++) {
            if (!comparators[i].validator($field)) {
                return comparators[i].msg;
            }
        }
        
        return null;
        
    },
    getFormFields: function ($form) {
        // return all form fields
        return $form.find('input, select, textarea');
    },
    getRequiredFields: function ($form) {
        // Return all form fields with 'required' attribute
        return $form.find('input, select, textarea').filter(
            function () {
                return formValidation.fieldRequired($(this));
            }
        );
    },
    getInvalidFields: function ($fields) {
        // Return fields that are not valid
        return $fields.filter(
            function () {
                return !formValidation.fieldValid($(this));
            }
        );
    },
    getInvalidFieldsMsgs: function($fields) {
        // Return fields that are not valid
        var fields = $fields.map(function() {
            return {
                "field": $(this),
                "msg" : formValidation.getFieldInvalidMsg($(this))
            }
        });

        return fields.filter(
            function () {
                return this.msg !== null;
            }
        );
    },
    fieldRequired: function ($field) {
        // does field contain required attribute
        return !!$field.attr('required');
    },
    fieldValid: function ($field) {
        // is the fields value valid
        return formValidation.getFieldInvalidMsg($field) === null;
    },
    formValid: function ($fields) {
        // is the group of fields valid
        return !!formValidation.getInvalidFields($fields).length;
    },
    validate: function ($formEl, onSubmit) {
        
        var formInvalidClassName = 'form-invalid';
        var fieldInvalidClassName = 'field-invalid';

        var self = this;
        
        var fieldChangeCbs = [],
            fieldBlurCbs = [];
        
        this.$requiredFields = formValidation.getRequiredFields($formEl);
        this.$form = $formEl;
        this.$fields = formValidation.getFormFields($formEl);
        
        this.formValid = true;
        
        this.getInvalidFields = function () {
            return formValidation.getInvalidFields(self.$requiredFields);
        };

        this.getInvalidFieldsMsgs = function() {
            return formValidation.getInvalidFieldsMsgs(self.$requiredFields);
        }
        
        this.onFieldValueChange = function (cb) {
            fieldChangeCbs.push(cb);
        };
        
        this.onFieldBlur = function (cb) {
            fieldBlurCbs.push(cb);
        };
        
        this.$fields.on(
            'keyup change',
            function (e) {
                if (self.$requiredFields.index($(e.target)) > -1) {
                    $(e.target).removeClass(fieldInvalidClassName).closest('.form-field').removeClass(fieldInvalidClassName);
                }
                
                for (var i = 0; i < fieldChangeCbs.length; i++) {
                    if (typeof fieldChangeCbs[i] === 'function') {
                        fieldChangeCbs[i].call(self, e, $(e.target));
                    }
                }
                
            }
        );
        
        this.$fields.on(
            'blur',
            function (e) {
                if (self.$requiredFields.index($(e.currentTarget)) > -1 && !formValidation.fieldValid($(e.target))) {
                    $(e.target).addClass(fieldInvalidClassName).closest('.form-field').addClass(fieldInvalidClassName);
                }
                
                for (var i = 0; i < fieldBlurCbs.length; i++) {
                    if (typeof fieldBlurCbs[i] === 'function') {
                        fieldBlurCbs[i].call(self, e, $(e.target));
                    }
                }
            }
        );
        
        $formEl.on(
            'submit',
            function (e) {
                
                //e.preventDefault();
                
                $formEl.removeClass(formInvalidClassName);
                self.$requiredFields.removeClass(fieldInvalidClassName).closest('.form-field').removeClass(fieldInvalidClassName);
                
                self.$requiredFields = formValidation.getRequiredFields($formEl);
                
                self.$invalidFields = formValidation.getInvalidFields(self.$requiredFields);
                
                self.formValid = (self.$invalidFields.length < 1);
                
                if (!self.formValid) {
                    self.$invalidFields.addClass(fieldInvalidClassName).addClass(fieldInvalidClassName).closest('.form-field').addClass(fieldInvalidClassName);
                }
                
                if (typeof onSubmit === 'function') {
                    onSubmit.call(self, e);
                }
                
            }
        )
        
    },
};

module.exports = formValidation;