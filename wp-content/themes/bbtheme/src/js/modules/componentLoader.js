var componentLoader = {
    "activeComponents": {},
    "initActiveComponents": (components) => {

        let index = 0;
        componentLoader.activeComponents = {};
        for (let componentId in components) {
            if ($(componentId).length > 0) {
                componentLoader.activeComponents[++index] = componentId;
            }
        }


        for (let id in componentLoader.activeComponents) {
            let componentId = componentLoader.activeComponents[id];
            components[componentId](componentId);
        }
    }
};

module.exports = componentLoader;