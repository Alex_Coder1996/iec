
/**
 * Trigger an event when the user has stopped resizing their window
 * - To stop listening for resize, re-invoke the function returned by the original call
 *
 * @param  {function} onTriggerFn - The callback which should invoke when the offset is met
 * @param  {number} debounceDelay - The ms delay value that should trigger the callback
 * @return {function}
 */
module.exports = function (onTriggerFn, debounceDelay){

    var watchDebounce = null;

    var handleResize = function (evt) {
        window.clearTimeout(watchDebounce);

        watchDebounce = window.setTimeout(function () {
            if (typeof onTriggerFn == 'function') {
                onTriggerFn.call(this, evt);
            }
        }, debounceDelay || 250);
    };

    $(window).on('resize', handleResize);

    return function () {
        $(window).off('resize', handleResize);
    };
};
