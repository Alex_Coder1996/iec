window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader'),
    util = require('./modules/util');

$(function () {

    let components = {
        'body': require('./partials/common')
    }

    componentLoader.initActiveComponents(components);




    if($(window).width() > 640)
    {
        util.equalHeights('.link-ctas-group-widget h3');
        util.equalHeights('.link-ctas-group-widget p');
    }


});