window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.regional-offices-widget': require('./partials/regional-offices-widget')
    }

    componentLoader.initActiveComponents(components);
});