window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.vacancies-widget' : require('./partials/vacancies-widget')
    }

    componentLoader.initActiveComponents(components);

});