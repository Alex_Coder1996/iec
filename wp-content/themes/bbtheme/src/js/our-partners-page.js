window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.icon-accordions-widget': require('./partials/icon-accordions-widget')
    }

    componentLoader.initActiveComponents(components);

});