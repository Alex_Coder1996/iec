window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.need-accordions-widget': require('./partials/need-accordions-widget'),
        '.section_technologies': require('./partials/section-technologies-widget')
    }

    componentLoader.initActiveComponents(components);

});