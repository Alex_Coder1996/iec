//window._ = require('lodash');
window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader')

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.hero-slider-widget': require('./partials/hero-slider-widget')
    }

    componentLoader.initActiveComponents(components);

});