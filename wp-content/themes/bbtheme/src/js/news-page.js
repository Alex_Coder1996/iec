window.$ = window.jQuery = require('jquery');

let componentLoader = require('./modules/componentLoader');

$(function () {

    let components = {
        'body': require('./partials/common'),
        '.news-events-widget' : require('./partials/news-events-widget')
    }

    componentLoader.initActiveComponents(components);

});