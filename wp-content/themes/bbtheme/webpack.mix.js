let mix = require('laravel-mix');

mix
  .options({
    processCssUrls: false,
  })
  .sass('src/sass/home-page.scss', 'css/')
  .sass('src/sass/about-page.scss', 'css/')
  .sass('src/sass/introduction-page.scss', 'css/')
  .sass('src/sass/management-board-page.scss', 'css/')
  .sass('src/sass/history-page.scss', 'css/')
  .sass('src/sass/our-partners-page.scss', 'css/')
  .sass('src/sass/our-offerings-page.scss', 'css/')
  .sass('src/sass/regional-offices-page.scss', 'css/')
  .sass('src/sass/office-detail-page.scss', 'css/')
  .sass('src/sass/vertical-markets-page.scss', 'css/')
  .sass('src/sass/maritime-page.scss', 'css/')
  .sass('src/sass/shipping-page.scss', 'css/')
  .sass('src/sass/product-detail-page.scss', 'css/')
  .sass('src/sass/solutions-products-page.scss', 'css/')
  .sass('src/sass/value-added-services-page.scss', 'css/')
  .sass('src/sass/vas-detail-page.scss', 'css/')
  .sass('src/sass/vas-detail-v2-page.scss', 'css/')
  .sass('src/sass/news-page.scss', 'css/')
  .sass('src/sass/news-event-detail-page.scss', 'css/')
  .sass('src/sass/contact-page.scss', 'css/')
  .sass('src/sass/become-partner-page.scss', 'css/')
  .sass('src/sass/join-our-team-page.scss', 'css/')
  .sass('src/sass/press-center-page.scss', 'css/')
  .sass('src/sass/media-kit-page.scss', 'css/')
  .sass('src/sass/search-result-page.scss', 'css/')
  .sass('src/sass/not-found-page.scss', 'css/')
  .sass('src/sass/content-page.scss', 'css/')
  .sass('src/sass/support-center-page.scss', 'css/')
  .sass('src/sass/iot-page.scss', 'css/')
  .sass('src/sass/vsat-experience-page.scss', 'css/')
  .sass('src/sass/surveillance-portfolio-page.scss', 'css/')

  .js('src/js/home-page.js', 'js/')
  .js('src/js/about-page.js', 'js/')
  .js('src/js/introduction-page.js', 'js/')
  .js('src/js/management-board-page.js', 'js/')
  .js('src/js/history-page.js', 'js/')
  .js('src/js/our-partners-page.js', 'js/')
  .js('src/js/our-offerings-page.js', 'js/')
  .js('src/js/regional-offices-page.js', 'js/')
  .js('src/js/office-detail-page.js', 'js/')
  .js('src/js/vertical-markets-page.js', 'js/')
  .js('src/js/maritime-page.js', 'js/')
  .js('src/js/shipping-page.js', 'js/')
  .js('src/js/product-detail-page.js', 'js/')
  .js('src/js/solutions-products-page.js', 'js/')
  .js('src/js/value-added-services-page.js', 'js/')
  .js('src/js/vas-detail-page.js', 'js/')
  .js('src/js/vas-detail-v2-page.js', 'js/')
  .js('src/js/news-page.js', 'js/')
  .js('src/js/news-event-detail-page.js', 'js/')
  .js('src/js/contact-page.js', 'js/')
  .js('src/js/become-partner-page.js', 'js/')
  .js('src/js/join-our-team-page.js', 'js/')
  .js('src/js/press-center-page.js', 'js/')
  .js('src/js/media-kit-page.js', 'js/')
  .js('src/js/search-result-page.js', 'js/')
  .js('src/js/not-found-page.js', 'js/')
  .js('src/js/content-page.js', 'js/')
  .js('src/js/support-center-page.js', 'js/')
  .js('src/js/iot-page.js', 'js/')
  .js('src/js/vsat-experience-page.js', 'js/')
  .js('src/js/surveillance-portfolio-page.js', 'js/');

// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.preact(src, output); <-- Identical to mix.js(), but registers Preact compilation.
// mix.coffee(src, output); <-- Identical to mix.js(), but registers CoffeeScript compilation.
// mix.ts(src, output); <-- TypeScript support. Requires tsconfig.json to exist in the same folder as webpack.mix.js
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.test');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.babelConfig({}); <-- Merge extra Babel configuration (plugins, etc.) with Mix's default.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.override(function (webpackConfig) {}) <-- Will be triggered once the webpack config object has been fully generated by Mix.
// mix.dump(); <-- Dump the generated webpack config object to the console.
// mix.extend(name, handler) <-- Extend Mix's API with your own components.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   globalVueStyles: file, // Variables file to be imported in every component.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   terser: {}, // Terser-specific options. https://github.com/webpack-contrib/terser-webpack-plugin#options
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
