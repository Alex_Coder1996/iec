<?php
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php

    $fields = get_fields();
    $iec_news_page = get_config('iec_news_page');
    $iec_news_page_link = get_permalink($iec_news_page);

	/* $category_caption = '';
	if(!empty($fields['type']) && is_array($fields['type'])){
		$category_caption = get_field('caption', $fields['type'][0]->taxonomy . '_' . $fields['type'][0]->term_id);
    } */

    $office_location = get_field('caption', $fields['office']->ID);

    $related_news = \BlueBeetle\Press\Generic::get_instance()->get_homepage_news();
    
    ?>


    <div id="main">
        <div class="container w return_back_container">
            <a href="<?php echo $iec_news_page_link ?>" class="show-on-load back-link <?php echo ($_SERVER['HTTP_REFERER'] == $iec_news_page_link) ? 'history-back':''; ?>">
                <?php _e('Return Back', 'bbtheme'); ?>
            </a>

            <?php
            $languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
            if(count($languages)>1) :
                if ( !empty( $languages ) ) :
                    ?>
                    <div class="lang_sel_div">
                        <a href="#" class="lang_sel_box_link"><img width="18" height="12" src="<?php echo get_template_directory_uri() . '/img/flags/'.ICL_LANGUAGE_CODE.'.jpg'; ?>"></a>                                
                    </div>
                    <div class="langs_list_div">
                        <div class="lang_links_div">
                            <?php
                            foreach( $languages as $l ) :
                                if ( $l['active'] ) : ?>
                                    <div class="lang_link_wrap">
                                        <a href="#" class="close_lang_box_link">
                                            <div class="flag_img"><img width="18" height="12" src="<?php echo get_template_directory_uri() . '/img/flags/'. esc_attr( $l['language_code'] ) .'.jpg'; ?>"></div>
                                            <div class="switch_text"><?php echo 'Keep '.$l['translated_name']; ?></div>
                                        </a>
                                    </div>
                                <?php endif;
                            endforeach;
                            ?>
                            <div class="lang_box_note">
                                <?php echo _e('This news is also available in:'); ?>
                            </div>
                            <?php
                            foreach( $languages as $l ) :
                                if ( !$l['active'] ) :
                                    $translated_name_text = $l['translated_name'];
                                    if($l['translated_name']=='Chinese (Simplified)'):
                                        $translated_name_text = 'Mandarin';
                                    endif; ?>
                                    <div class="lang_link_wrap">
                                        <a href="<?php echo esc_url( $l['url'] ); ?>" class="lang_page_link">
                                            <div class="flag_img"><img width="18" height="12" src="<?php echo get_template_directory_uri() . '/img/flags/'. esc_attr( $l['language_code'] ) .'.jpg'; ?>"></div>
                                            <div class="switch_text"><?php echo 'Switch to '.$translated_name_text; ?></div>
                                        </a>
                                    </div>
                                <?php
                                endif;
                            endforeach;
                            ?>
                        </div>                    
                        <div class="close_box">
                            <a class="close_link"></a>
                        </div>
                    </div>
                    <?php                
                endif;
            endif;
            ?>

        </div>

        <div class="news-event-detail-widget">
            <div class="container xs">
                <h1><?php echo $fields['caption']; ?></h1>
                <div class="date-n-location cf">
                    <div><?php echo get_the_date('d M Y'); ?></div>
                    <div><?php echo $office_location; ?></div>
                </div>

                <div class="image-wrapper">
                    <img class="lozad" src="<?php echo get_template_directory_uri(); ?>/img/spacer.gif" data-src="<?php echo $fields['image']['url']; ?>"
                         alt="<?php echo $fields['caption']; ?>">
	                <?php /* if (!empty($category_caption)) : ?>
                        <div class="cat hidden"><?php echo $category_caption; ?></div>
	                <?php endif; */ ?>
                </div>


                <div class="text-content-widget">
                    <div class="content">
                        <?php echo $fields['content']; ?>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($related_news)): ?>
            <div class="news-widget">
                <div class="container w">
                    <div class="wrapper">
                        <div class="container m">
                            <h3><?php _e('Featured News', 'bbtheme'); ?></h3>
                            <div class="items cf">
                                <?php foreach ($related_news as $news_index => $post) : ?>
                                    <?php
                                    $caption = get_field('caption', $post->ID);
                                    $image = get_field('image', $post->ID);
                                    $location = get_field('office', $post->ID);
                                    $location_text = get_field('news_location_text', $location->ID);
                                    $image_url = (isset($image['sizes']['news-thumb-02'])) ? $image['sizes']['news-thumb-02'] : $image['url'];
                                    ?>
                                    <a class="item" href="<?php echo get_the_permalink($post); ?>">
                                        <img src="<?php echo $image_url; ?>" alt="<?php echo $caption; ?>">
                                        <h4><?php echo $caption; ?></h4>
                                        <div class="date-n-location">
                                            <span><?php echo get_the_date('d M Y',$post); ?></span>
                                            <span><?php echo $location_text; ?></span>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
