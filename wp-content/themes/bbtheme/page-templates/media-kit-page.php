<?php
/**
 * Template Name: Media Kit Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    ?>

    <div id="main">

        <div class="banner-widget small">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($fields['banner']['caption']) || !empty($fields['banner']['description'])) : ?>
                        <div class="overlay-wrapper container w">
                            <div class="overlay">
                                <?php if (!empty($fields['banner']['caption'])) : ?>
                                    <div class="caption-wrapper"><?php echo $fields['banner']['caption']; ?></div>
                                <?php endif; ?>
                                <?php if (!empty($fields['banner']['description'])) : ?>
                                    <?php echo $fields['banner']['description']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-2">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>"
                                <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="media-kit-widget">
            <div class="container">
                <div class="header cf">
                    <?php if (!empty($fields['contacts'])) : ?>
                        <div class="press-contacts-widget">
                            <h6><?php _e('Press Contacts', 'bbtheme'); ?></h6>
                            <div class="contacts">
                                <?php foreach ($fields['contacts'] as $contact) : ?>
                                    <div class="contact cf">
                                        <div class="right">
                                            <?php if (!empty($contact['telephone'])) : ?>
                                                <a href="tel:<?php echo $contact['telephone']; ?>" class="phone"></a>
                                            <?php endif; ?>
                                            <?php if (!empty($contact['email'])) : ?>
                                                <a href="mailto:<?php echo antispambot($contact['email'], 1); ?>"
                                                   class="email"></a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="left">
                                            <div class="name"><?php echo $contact['name']; ?></div>
                                            <div class="job-title"><?php echo $contact['job_title']; ?></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if (!empty($fields['downloads'])) : ?>
                    <div class="downloads">
                        <h6><?php _e('Downloads', 'bbtheme'); ?></h6>
                        <div class="items cf">
                            <?php foreach ($fields['downloads'] as $download) : ?>
                                <?php if (!empty($download['image'])): ?>
                                    <div class="item">
                                        <div class="inner-wrapper lozad"
                                             data-background-image="<?php echo $download['image']['url']; ?>">
                                            <div class="text-overlay">
                                                <h3><?php echo $download['title']; ?></h3>
                                                <?php if (!empty($download['content'])): ?>
                                                    <p><?php echo $download['content']; ?></p>
                                                <?php endif; ?>
                                            </div>
                                            <div class="download-overlay">
                                                <a href="<?php echo $download['file']['url']; ?>"
                                                   class="download" download=""><?php _e('Download', 'bbtheme'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="item full">
                                        <div class="inner-wrapper">
                                            <div class="cf">
                                                <div class="left">
                                                    <h5><?php echo $download['title']; ?></h5>
                                                    <?php if (!empty($download['content'])): ?>
                                                        <p><?php echo $download['content']; ?></p>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="right">
                                                    <a href="<?php echo $download['file']['url']; ?>"
                                                       class="download"
                                                       download=""><?php _e('Download', 'bbtheme'); ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>