<?php
/**
 * Template Name: Home Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php $fields = get_fields(); ?>

    <div id="main">

        <?php if (!empty($fields['slider'])) : ?>
            <div class="hero-slider-widget">
                <div class="container xw n-p">
                    <div class="slider">
                        <?php foreach ($fields['slider'] as $slide_index => $slide) : ?>
                            <div class="slide">
                                <div class="inner-wrapper lozad"
                                     data-background-image="<?php echo $slide['image']['url']; ?>">
                                    <div class="container w overlay-wrapper">
                                        <div class="overlay">
                                            <?php if (!empty($slide['caption'])) : ?>

                                                <?php
                                                $linkCaption = false;
                                                if (!empty($slide['link']) && empty($slide['link_caption'])) :
                                                    $linkCaption = true;
                                                endif
                                                ?>

                                                <div class="caption-wrapper <?php echo ($linkCaption) ? 'caption-linked': ''?>">

                                                    <?php if ($linkCaption): ?>
                                                    <a href="<?php echo $slide['link']['url']; ?>">
                                                    <?php endif ?>

                                                        <span class="caption"><?php echo $slide['caption']; ?></span>
                                                        <span class="bg"><?php echo $slide['caption']; ?></span>

                                                    <?php if ($linkCaption): ?>
                                                    </a>
                                                <?php endif ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if (!empty($slide['link']) && !empty($slide['link_caption'])) : ?>
                                                <a href="<?php echo $slide['link']['url']; ?>"
                                                   target="<?php echo $slide['link']['target']; ?>"
                                                   class="btn">
                                                    <?php echo $slide['link_caption']; ?>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="bottom-gradient"></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="container w n-p arrows-wrapper"></div>
            </div>
        <?php endif; ?>

        <?php if (!empty($fields['section'])) : ?>
            <div class="vertical-markets-widget">
                <?php if (!empty($fields['section_title'])) : ?>
                    <div class="container w">
                        <h3><?php echo $fields['section_title']; ?></h3>
                    </div>
                <?php endif; ?>
                <div class="container w n-p">
                    <div class="items cf">
                        <?php foreach ($fields['section'] as $section_index => $section) : ?>
                            <?php
                            $section_links = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($section['links']);
                            $link = array_shift($section_links);
                            ?>
                            <div class="item lozad secondary-links-<?php echo count($section_links) ?>"
                                 data-background-image="<?php echo $section['image']['url']; ?>">
                                <div class="overlay">
                                    <a href="<?php echo $link['url']; ?>"
                                       class="primary-link"><?php echo $link['caption']; ?></a>
                                    <?php if (!empty($section_links)) : ?>
                                        <div class="secondary-links">
                                            <?php foreach ($section_links as $link) : ?>
                                                <a href="<?php echo $link['url']; ?>">
                                                    <div class="icon lozad"
                                                         data-background-image="<?php echo $link['icon']; ?>"></div>
                                                    <div><?php echo $link['caption']; ?></div>
                                                </a>

                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php
        $news = \BlueBeetle\Press\Generic::get_instance()->get_homepage_news();
        ?>

        <?php if (!empty($news)): ?>
            <div class="news-widget">
                <div class="container m">
                    <?php if (!empty($fields['news_title'])) : ?>
                        <h3><?php echo $fields['news_title']; ?></h3>
                    <?php endif; ?>
                    <div class="items cf">
                        <?php foreach ($news as $news_index => $post) : ?>
                            <?php
                            $caption = get_field('caption', $post->ID);
                            $image = get_field('image', $post->ID);
                            $location = get_field('office', $post->ID);
                            $location_text = get_field('news_location_text', $location->ID);
                            $image_url = (isset($image['sizes']['news-thumb-02'])) ? $image['sizes']['news-thumb-02'] : $image['url'];
                            ?>
                            <a class="item" href="<?php echo get_the_permalink($post); ?>">
                                <img class="lozad" data-src="<?php echo $image_url; ?>" alt="<?php echo $caption; ?>">
                                <h4><?php echo $caption; ?></h4>
                                <div class="date-n-location">
                                    <span><?php echo get_the_date('d M Y', $post); ?></span>
                                    <span><?php echo $location_text; ?></span>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
