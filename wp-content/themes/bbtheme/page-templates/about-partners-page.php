<?php
/**
 * Template Name: About Partners Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $menu_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['banner']['links']);
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    ?>

    <div id="main">

        <div class="banner-widget small">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($menu_pages)): ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($menu_pages as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>"
                                        <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>


        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-3">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>"
                                <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($fields['partners'])) : ?>
            <div class="icon-accordions-widget">
                <div class="container">
                    <div class="items cf">

                        <?php foreach ($fields['partners'] as $partner_index => $partner) : ?>
                            <?php
                            $icon = get_field('icon', $partner->ID);
                            $caption = get_field('caption', $partner->ID);
                            $content = get_field('content', $partner->ID);
                            $brochure = get_field('brochure', $partner->ID);
                            $coverage_map = get_field('coverage_map', $partner->ID);
                            ?>
                            <div class="item <?php if ($partner_index == 0): ?> active <?php endif; ?>">
                                <div class="inner-wrapper">
                                    <?php if (!empty($icon)) : ?>
                                        <div class="img lozad" data-background-image="<?php echo $icon['url']; ?>" title="<?php echo $icon['title']; ?>"></div>
                                    <?php endif; ?>
                                    <div class="title-wrapper">
                                        <h6><?php echo $caption; ?></h6>
                                        <div class="toggle-icon"></div>
                                    </div>
                                </div>

                                <div class="content">
                                    <div class="container w">
                                        <h3><?php echo $caption; ?></h3>

                                        <?php if (!empty($content)) : ?>
                                            <div class="text-content">
                                                <?php echo $content; ?>
                                            </div>
                                        <?php endif; ?>

                                        <?php if (!empty($brochure) || !empty($coverage_map)) : ?>
                                            <div class="buttons cf">
                                                <?php if (!empty($brochure)) : ?>
                                                    <div>
                                                        <a href="<?php echo $brochure['url']; ?>"
                                                           class="download-brochure"><?php _e('Download brochure', 'bbtheme'); ?></a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php if (!empty($coverage_map)) : ?>
                                                    <div>
                                                        <a href="#" class="view-map"
                                                           data-map="<?php echo $coverage_map['url']; ?>"><?php _e('View Coverage Map', 'bbtheme'); ?></a>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="coverage-map-popup lity-hide">
            <a href="#" class="close"></a>
            <img src="" alt="">
        </div>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
