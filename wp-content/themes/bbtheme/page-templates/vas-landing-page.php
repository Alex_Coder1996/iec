<?php
/**
 * Template Name: VAS Landing Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php $fields = get_fields(); ?>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <div class="text-content-widget">
            <div class="container">
                <div class="content">
                    <?php if (!empty($fields['banner']['caption'])) : ?>
                        <h1><?php echo $fields['banner']['caption']; ?></h1>
                    <?php endif; ?>
                    <?php if (!empty($fields['banner']['description'])) : ?>
                        <?php echo $fields['banner']['description']; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['section'])) : ?>
            <div class="value-added-services-widget">
                <div class="container m">
                    <div class="items cf">
                        <?php foreach ($fields['section'] as $section_index => $section) : ?>
                            <div class="item <?php echo $section['full_width']==1 ? 'full':''?>">
                                <a href="<?php echo get_the_permalink($section['page']); ?>" class="inner-wrapper lozad"
                                   data-background-image="<?php echo $section['image']['url']; ?>">
                                    <div class="overlay">
                                        <?php if (!empty($section['caption'])) : ?>
                                            <h6><?php echo $section['caption']; ?></h6>
                                        <?php endif; ?>
                                        <?php if (!empty($section['summary'])) : ?>
                                            <p><?php echo $section['summary']; ?></p>
                                        <?php endif; ?>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>