<?php
/**
 * Template Name: Offices Landing Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $offices = \BlueBeetle\Press\Generic::get_instance()->get_offices();
    ?>

    <div id="main">
        <div class="regional-offices-widget">
            <div class="container">
                <div class="regional-map">
                    <img src="<?php echo $fields['map_image']['url']; ?>" alt="<?php echo $fields['caption']; ?>">

                    <?php if (!empty($offices)): ?>
                        <div class="office-pins">
                            <?php foreach ($offices as $office_index => $post) : ?>
                                <?php
                                $map_location_top = get_field('map_location_top',$post->ID);
                                $map_location_left = get_field('map_location_left',$post->ID);
                                ?>
                                <a href="#" data-office-name="<?php echo $post->post_name;?>" style="left: <?php echo $map_location_left;?>%; top: <?php echo $map_location_top;?>%;"></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="details">
                <div class="container">
                    <div class="text-content">
                        <h2><?php echo $fields['map_caption']; ?></h2>
                        <?php if(!empty($fields['map_description'])) : ?>
                            <p><?php echo $fields['map_description']; ?></p>
                        <?php endif; ?>
                    </div>

                    <?php if (!empty($offices)): ?>
                        <div class="office-links">
                            <?php foreach ($offices as $office_index => $post) : ?>
                                <?php
                                $caption = get_field('landing_caption',$post->ID);
                                if(empty($caption)){
                                    $caption = get_field('caption',$post->ID);
                                }

                                $region_page_url = get_the_permalink();
                                $reg_fields = get_fields();
                                if($reg_fields['translation_language']!='') {            
                                    $foreign_lang_code = $reg_fields['translation_language'];
                                    if(isset($_COOKIE['lang_cookie_notice_accepted_'.$post->post_name]) && $_COOKIE['lang_cookie_notice_accepted_'.$post->post_name]!='') :
                                        if($_COOKIE['lang_cookie_notice_accepted_'.$post->post_name]==$foreign_lang_code) :
                                            $region_page_url = apply_filters('wpml_permalink', $region_page_url , $foreign_lang_code);
                                        endif;
                                    endif;

                                }

                                ?>
                                <a href="<?php echo $region_page_url; ?>" data-office-name="<?php echo $post->post_name;?>" ><?php echo $caption ?></a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    </div>

<?php endwhile; ?>

<?php get_template_part('includes/footer'); ?>

<?php wp_footer(); ?>
<div class="bottom-pattern"></div>
</body>
</html>
