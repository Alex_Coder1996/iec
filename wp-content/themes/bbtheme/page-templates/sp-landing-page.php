<?php

/**
 * Template Name: Solution and Product Landing Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php $fields = get_fields(); ?>
    <script>
        var CONFIG = {
            productsSolutionAPI: '<?php echo get_ajax_url('psfilter', 'search'); ?>'
        }
    </script>
    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>


        <div class="text-content-widget">
            <div class="container m">
                <div class="content">
                    <?php if (!empty($fields['banner']['caption'])) : ?>
                        <h1><?php echo $fields['banner']['caption']; ?></h1>
                    <?php endif; ?>
                    <?php if (!empty($fields['banner']['description'])) : ?>
                        <?php echo $fields['banner']['description']; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="solutions-products-listing-widget">
            <div class="">
                <div class="filters-form">
                    <form action="" id="filters-form">
                        <input type="hidden" name="start-at" value="0">
                        <div class="head-wrapper">
                            <div class="container m">
                                <div class="cf">
                                    <h3><?php _e('We will find a tailored solution for you', 'bbtheme'); ?></h3>
                                    <div class="search-field">
                                        <a href="#" class="close"></a>
                                        <input type="text" name="keyword" id="keyword" placeholder="Search products...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container m">
                            <div class="filters cf">
                                <div class="col first">
                                    <div class="filters-group first">
                                        <h6><?php _e('Application', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('application'); ?>
                                            </div>
                                        </div>
                                        <!-- <h6><?php _e('By Specifications', 'bbtheme'); ?></h6> -->
                                    </div>

                                    <div class="filters-group first">
                                        <h6><?php _e('Set up', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('setup'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="filters-group first">
                                        <h6><?php _e('Service', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('service'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="filters-group first">
                                        <h6><?php _e('Type', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('type'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col second">
                                    <div class="filters-group second">
                                        <h6><?php _e('Operator', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('operator'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="filters-group second">
                                        <h6><?php _e('Market', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('market'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col third">
                                    <div class="filters-group third">
                                        <h6><?php _e('Speed', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('speed'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="filters-group third">
                                        <h6><?php _e('Coverage', 'bbtheme'); ?></h6>
                                        <div class="toggle-fields-wrapper">
                                            <div class="toggle-fields-row cf">
                                                <?php print_ps_filter('region'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="listing" id="items-listing">
                    <div class="container m">
                        <div class="items cf">
                        </div>
                    </div>
                </div>

                <div class="loading-overlay">
                    <div class="loader"></div>
                </div>

                <div class="load-more">
                    <a href="#"><?php _e('Load More', 'bbtheme'); ?></a>
                    <div class="loader"></div>
                </div>

                <div class="no-results"><?php _e('No results found.', 'bbtheme'); ?></div>

            </div>
        </div>


    </div>

<?php endwhile; ?>

<?php get_footer(); ?>