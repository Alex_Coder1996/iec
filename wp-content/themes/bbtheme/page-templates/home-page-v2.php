<?php
/**
 * Template Name: Home Page v2
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php $fields = get_fields(); ?>
	
    <link rel="stylesheet" href="https://iec-telecom.com/wp-content/themes/bbtheme/css/home-page.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://iec-telecom.com/wp-content/themes/bbtheme/js/home-page.js"></script>
	<style>
		.news-v2 { width: 100%; max-width: 1120px; margin: 0 auto; }
		.news-v2 ul { margin: 0; list-style: none; text-align: center; }
		.news-v2 ul li { float: left; width: 33.3%; padding: 5px 20px; vertical-align: top; text-align: left; }
		.news-v2 ul li img { width: auto; max-width: 100%; min-height: 200px; margin-bottom: 10px; }
		.news-v2 ul li h2 { line-height: 20px; margin-bottom: 10px; min-height: 44px; }
		.news-v2 ul li h2 a { color: #1b204c; text-transform: uppercase; font-size: 14px; }
		.news-v2 ul li p { font-size: 12px; color: #b2b2b2; }
		
		.section1-v2 { background: #dedff0; padding: 50px 0; margin: 50px 0; }
		.section1-v2-inner { width: 100%; max-width: 1080px; margin: 0 auto; }
		.section1-v2-inner h2 { color: #1b204c; font-size: 35px; line-height: 40px; margin-bottom: 30px; }
		.section1-v2-inner p { color: #1b204c; font-size: 20px; line-height: 30px; text-align: justify; }
		.section1-v2-inner p a { background: #515a79; color: #fff; font-weight: 800; display: inline-block; border-radius: 3px; padding: 8px 80px; margin-top: 30px; }
		
		.container.w { max-width: 1080px !important; }
		
		.section2-v2 { background: url(https://iec-telecom.com/wp-content/uploads/2021/09/section2-v2-bg.jpg) no-repeat; background-size: cover; padding: 50px 0; margin: 50px 0 0; }
		.section2-v2-inner { width: 100%; max-width: 1080px; margin: 0 auto; }
		.section2-v2-inner h2 { color: #1b204c; font-size: 35px; line-height: 40px; margin-bottom: 30px; }
		.section2-v2-inner p { color: #1b204c; font-size: 20px; line-height: 30px; text-align: justify; }
		
		.section3-v2 { background: url(https://iec-telecom.com/wp-content/uploads/2021/09/section3-v2-bg.jpg) no-repeat; background-size: cover; padding: 50px 0 350px; margin: 0 0 -84px; background-position: bottom center; }
		.section3-v2-inner { width: 100%; max-width: 1080px; margin: 0 auto; }
		.section3-v2-inner h2 { color: #1b204c; font-size: 35px; line-height: 40px; margin-bottom: 30px; }
		.section3-v2-inner p { color: #1b204c; font-size: 20px; line-height: 30px; text-align: justify; display: inline-block; vertical-align: middle; width: 70%; margin-right: 20px; }
		.section3-v2-inner a { background: #515a79; color: #fff; font-weight: 800; display: inline-block; border-radius: 3px; padding: 8px 60px; }
		.section3-v2-inner img { vertical-align: middle; margin-left: 10px; }
		
		.hero-slider-widget .slider .slide .overlay { left: 0 !important; padding-left: 0 !important; }
		
		#footer { padding-bottom: 0; padding-top: 10px; background: rgba(27,32,76,.8); position: absolute; bottom: 0; width: 100%; }
		#footer .right-wrapper .social-media-widget a { background-color: transparent !important; }
		#footer .left-wrapper h3, #footer .copy-rights, #footer .links a { color: #fff !important; }
		#menu-item-27636 { display: none !important; }
		
		body:after {
			background-image: none !important;
		}
		
		@media only screen and (max-width: 800px) {
			.news-v2 ul li { width: 100% !important; }
			.section1-v2-inner, .section2-v2-inner, .section3-v2-inner { padding: 0 30px !important; }
			.section1-v2 { margin-bottom: 0 !important; }
			.section2-v2 { margin-top: 0 !important; }
			.vertical-markets-widget { margin-bottom: 0 !important; margin-top: 0 !important; }
			.section3-v2-inner p { width: 100% !important; }
			.section3-v2-inner h4 { text-align: center; display: block !important; }
			.section3-v2 { padding-bottom: 270px !important; }
			.section1-v2-inner p:last-child { text-align: center !important; }
			.news-v2 ul li { padding-right: 30px !important; padding-left: 30px !important; }
			.hero-slider-widget .slider .slide .overlay { padding-right: 30px !important; padding-left: 30px !important; }
			
			#footer .left-wrapper h3,
			#footer .left-wrapper .links,
			#footer .left-wrapper .links,
			#footer .left-wrapper .copy-rights { width: 100%; text-align: center !important; margin-right: 0 !important; }
			#footer .right-wrapper { text-align: center !important; }
			#footer .right-wrapper .social-media-widget { display: inline-block !important; }
			#footer .wrapper { padding-top: 30px !important; }
		}
	</style>
    
	<div id="main">
		<?php if (!empty($fields['slider'])) : ?>
            <div class="hero-slider-widget">
                <div class="container xw n-p">
                    <div class="slider">
                        <?php foreach ($fields['slider'] as $slide_index => $slide) : ?>
                            <div class="slide">
                                <div class="inner-wrapper lozad"
                                     data-background-image="<?php echo $slide['image']['url']; ?>">
                                    <div class="container w overlay-wrapper">
                                        <div class="overlay">
                                            <?php if (!empty($slide['caption'])) : ?>

                                                <?php
                                                $linkCaption = false;
                                                if (!empty($slide['link']) && empty($slide['link_caption'])) :
                                                    $linkCaption = true;
                                                endif
                                                ?>

                                                <div class="caption-wrapper <?php echo ($linkCaption) ? 'caption-linked': ''?>">

                                                    <?php if ($linkCaption): ?>
                                                    <a href="<?php echo $slide['link']['url']; ?>">
                                                    <?php endif ?>

                                                        <span class="caption"><?php echo $slide['caption']; ?></span>
                                                        <span class="bg"><?php echo $slide['caption']; ?></span>

                                                    <?php if ($linkCaption): ?>
                                                    </a>
                                                <?php endif ?>
                                                </div>
                                            <?php endif; ?>
                                            <?php if (!empty($slide['link']) && !empty($slide['link_caption'])) : ?>
                                                <a href="<?php echo $slide['link']['url']; ?>"
                                                   target="<?php echo $slide['link']['target']; ?>"
                                                   class="btn">
                                                    <?php echo $slide['link_caption']; ?>
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div class="bottom-gradient"></div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="container w n-p arrows-wrapper"></div>
            </div>
        <?php endif; ?>
		
		<div class="news-v2">
			<ul>
			<?php
				$lastposts = get_posts(array('posts_per_page' => 7, 'post_type' => 'news'));
				$counter = 0;
				if($lastposts) {
					foreach($lastposts as $post):
						setup_postdata($post); ?>
						<?php
							$image = get_field('image', $post->ID);
							$image_url = (isset($image['sizes']['news-thumb-02'])) ? $image['sizes']['news-thumb-02'] : $image['url'];
							$counter++;
						?>
						<li>
							<?php if($counter == 1) { ?> <img src="<?php echo $image_url; ?>"> <?php } ?>
							<h2><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
							<p><?php echo get_the_date('d M Y'); ?></p>
						</li>
						<?php
					endforeach; 
					wp_reset_postdata();
				}
			?>
			</ul>
		</div>
		<div style="clear: both;"></div>

		<div class="section1-v2">
			<div class="section1-v2-inner">
				<h2>SMART SATELITE COMMUNICATION SOLUTIONS FOR THE MOST CHALLENGING ENVIRONMENT & SITUATIONS</h2>
				<p>IEC Telecom provides global industry-leading satellite communication services to governments and public institutions, businesses and consumers.</p>
				<p>With over 25+ years of expertise in mobile and fixed satellite communications technology, IEC Telecom is well-positioned to supply reliable, high-quality, innovative satellite communication products and solutions to both institutional and individual customers all over the world, whether at sea or remote areas.</p>
				<p style="text-align: right;"><a href="https://iec-telecom.com/en/contact/">Lets talk</a></p>
			</div>
		</div>
		
        <?php if (!empty($fields['section'])) : ?>
            <div class="vertical-markets-widget">
                <div class="container w n-p">
                    <div class="items cf">
                        <?php foreach ($fields['section'] as $section_index => $section) : ?>
                            <?php
                            $section_links = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($section['links']);
                            $link = array_shift($section_links);
                            ?>
                            <div class="item lozad secondary-links-<?php echo count($section_links) ?>"
                                 data-background-image="<?php echo $section['image']['url']; ?>">
                                <div class="overlay">
                                    <a href="<?php echo $link['url']; ?>"
                                       class="primary-link"><?php echo $link['caption']; ?></a>
                                    <?php if (!empty($section_links)) : ?>
                                        <div class="secondary-links">
                                            <?php foreach ($section_links as $link) : ?>
                                                <a href="<?php echo $link['url']; ?>">
                                                    <div class="icon lozad"
                                                         data-background-image="<?php echo $link['icon']; ?>"></div>
                                                    <div><?php echo $link['caption']; ?></div>
                                                </a>

                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
		
		<?php the_content(); ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>