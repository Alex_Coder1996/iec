<?php
/**
 * Template Name: Iot Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $current_page_id = get_the_ID();
    $countries = \BlueBeetle\Press\Common::get_instance()->get_countries();
    $form_interests = get_config('enquiry_form_interest');
    $success_popup_content = get_config('enquiry_success_popup_content');
    $success_popup_content_key = array_search('iot-enquiry', array_column($success_popup_content, 'form_type'));
    ?>

    <script language="JavaScript">
        let error_messages = {
            100: '<?php esc_attr_e('Invalid method', 'bbtheme'); ?>',
            101: '<?php esc_attr_e('Invalid data', 'bbtheme'); ?>',
            102: '<?php esc_attr_e('Invalid email', 'bbtheme'); ?>',
            103: '<?php esc_attr_e('Captcha not validated', 'bbtheme'); ?>'
        };
    </script>

    <div id="main">

        <div class="slider_section">
            <!-- Slider main container -->
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <?php foreach ($fields['slider'] as $item) : ?>
                        <div class="item_slider swiper-slide" style="background-image: url(<?php echo $item['image']; ?>);">
                            <div class="container w overlay">
                                <div class="title">
                                    <h3><?php echo $item['caption']; ?></h3>
                                </div>
                                <div class="content"><?php echo $item['description']; ?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <!-- If we need navigation buttons -->
                <div class="navigation_overlay container w">
                    <div class="slider-button-prev"></div>
                    <div class="slider-button-next"></div>
                </div>
            </div>
        </div>

        <div class="section_content">
            <div class="container w">
                <div class="columns">
                    <div class="column_left">
                        <div class="text-content-widget">
                            <div class="content">
                                <div class="section-title">
                                    <h3><?php echo $fields['content']['title']; ?></h3>
                                </div>
                                <?php echo $fields['content']['content']; ?>
                                <div class="section-link-arrow">
                                    <a href="<?php echo $fields['content']['link']['url']; ?>" <?php if ($fields['content']['link']['target']) : ?>
                                        target="<?php echo $fields['content']['link']['target']; ?>" <?php endif; ?>><?php echo $fields['content']['link']['title']; ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="column_right">
                        <div class="enquiry-form-widget">
                            <form action="<?php echo get_ajax_url('enquiry', 'save'); ?>" method="post" id="enquiry-form" novalidate>
                                <div class="form-fields">
                                    <h5>
                                        <?php _e('Enquire Now', 'bbtheme'); ?>
                                    </h5>

                                    <div class="error">
                                        <?php _e('Found some issues, Try again later.', 'bbtheme'); ?>
                                    </div>

                                    <input type="hidden" name="form_type" value="iot-enquiry">

                                    <div class="form-field">
                                        <input type="text" name="full_name" placeholder="<?php esc_attr_e('Full Name...', 'bbtheme'); ?>" required>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" name="company" placeholder="<?php esc_attr_e('Company...', 'bbtheme'); ?>">
                                    </div>
                                    <div class="form-field">
                                        <input type="email" name="email" placeholder="<?php esc_attr_e('Email...', 'bbtheme'); ?>" required>
                                    </div>
                                    <?php if (!empty($countries)) : ?>
                                        <div class="form-field">
                                            <select name="country" id="" required class="select2">
                                                <option value=""><?php _e('Country...', 'bbtheme'); ?></option>
                                                <?php foreach ($countries as $country) : ?>
                                                    <option value="<?php echo $country['country_code']; ?>"><?php echo $country['country_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($form_interests)):  ?>

                                        <div class="form-field">
                                            <select name="interests[]" required multiple class="select2 select2-interest" data-select2-hide-search="true" data-select2-placeholder="Interest...">
                                                <option value=""><?php _e('Interest...', 'bbtheme');  ?></option>
                                                <?php foreach ($form_interests as $interest): ?>
                                                    <option value="<?php echo $interest['text'];  ?>" <?php echo (in_array($interest['text'], [$fields['caption'], $fields['interest']]))?'selected':'';?>><?php echo $interest['text']; ?></option>
                                                <?php endforeach;  ?>
                                            </select>
                                        </div>
                                    <?php endif;  ?>

                                    <div class="form-field no-border">
                                        <textarea name="message" id="" cols="30" rows="4" placeholder="<?php esc_attr_e('Message...', 'bbtheme'); ?>" class="expand-on-click" required></textarea>
                                    </div>

                                    <div class="form-field  no-border google-captcha-wrapper">
                                        <div id="google-captcha-holder"
                                             data-sitekey="<?php echo get_config('bbpress_google_captcha_site_key'); ?>">
                                            <div class="g-recaptcha" id="g-recaptcha">
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="page_url" value="<?php the_permalink($current_page_id); ?>">

                                    <div class="form-button">
                                        <button name="submit"><?php _e('Send', 'bbtheme'); ?></button>
                                    </div>
                                </div>
                                <div class="loading-overlay">
                                    <div class="loader"></div>
                                </div>
                                <div class="success">
                                    <h5><?php _e('Thank You!', 'bbtheme'); ?></h5>
                                    <?php _e('Your inquiry has been well received. One of our customer service representatives will contact you shortly.', 'bbtheme'); ?>
                                </div>

                            </form>
                        </div>

                        <div id="success-popup" class="message-popup lity-hide">
                            <?php echo $success_popup_content[$success_popup_content_key]['content']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['section_services'])) : ?>
        <div class="section_services">
            <div class="section-title">
                <div class="container w">
                    <h3><?php echo $fields['section_services']['section_title']; ?></h3>
                </div>
            </div>
            <div class="section_content">
                <div class="container w">
                    <div class="items">
                        <?php foreach ($fields['section_services']['items'] as $item) : ?>
                        <div class="item" style="background-image: url(<?php echo $item['image']; ?>);">
                            <div class="title">
                                <h5><?php echo $item['caption']; ?></h5>
                            </div>
                            <div class="content">
                                <p><?php echo $item['description']; ?></p>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if (!empty($fields['section_areas'])) : ?>
        <div class="section_areas">
            <div class="section-title">
                <div class="container w">
                    <h3><?php echo $fields['section_areas']['section_title']; ?></h3>
                </div>
            </div>
            <div class="section_content">
                <div class="container w">
                    <div class="items">
                        <?php foreach ($fields['section_areas']['items'] as $item) : ?>
                            <div class="item" style="background-image: url(<?php echo $item['image']; ?>);">
                                <div class="content">
                                    <div class="title">
                                        <h5><?php echo $item['caption']; ?></h5>
                                    </div>
                                    <div class="list">
                                        <ul>
                                            <?php foreach ($item['list'] as $list_item) : ?>
                                            <li><?php echo $list_item['list_item']; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="link section-link-arrow">
                                    <a href="<?php echo $item['link']['url']; ?>" <?php if ($item['link']['target']) : ?>
                                        target="<?php echo $item['link']['target']; ?>" <?php endif; ?>><?php echo $item['link']['title']; ?></a>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if (!empty($fields['section_maritime'])) : ?>
        <div class="section_maritime">
            <div class="section-title">
                <div class="container w">
                    <h3><?php echo $fields['section_maritime']['section_title']; ?></h3>
                </div>
            </div>
            <div class="need-accordions-widget maritime">
                <div class="container w n-p">
                    <div class="items">
                        <?php foreach ($fields['section_maritime']['accordion_maritime'] as $need_index => $need) : ?>
                            <div class="item  <?php if ($need_index == 0) : ?> expand <?php endif; ?>">
                                <div class="container m">
                                    <div class="title">
                                        <h5><?php echo $need['caption']; ?></h5>
                                    </div>
                                    <div class="content">
                                        <?php echo $need['content']; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if (!empty($fields['section_in-land'])) : ?>
        <div class="section_in-land">
            <div class="section-title">
                <div class="container w">
                    <h3><?php echo $fields['section_in-land']['section_title']; ?></h3>
                </div>
            </div>
            <div class="need-accordions-widget in_land">
                <div class="container w n-p">
                    <div class="items">
                        <?php foreach ($fields['section_in-land']['accordion_in-land'] as $need_index => $need) : ?>
                            <div class="item  <?php if ($need_index == 0) : ?> expand <?php endif; ?>">
                                <div class="container m">
                                    <div class="title">
                                        <h5><?php echo $need['caption']; ?></h5>
                                    </div>
                                    <div class="content">
                                        <?php echo $need['content']; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>