<?php
/**
 * Template Name: Thank You Landing Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    
    <link rel="stylesheet" href="https://iec-telecom.com/wp-content/themes/bbtheme/css/contact-page.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://iec-telecom.com/wp-content/themes/bbtheme/js/contact-page.js"></script>
    <style>
    	body {
            background-color: #f3f3f3;
        }
    	body:after {
            background-image: url('https://iec-telecom.com/wp-content/uploads/2021/09/thankyoubg-v2.png') !important;
            background-size: cover !important;
            opacity: 1;
            background-position: center;
        }
        .thankyou-inner {
            width: 100%;
    		max-width: 600px;
    		margin: 50px auto;
    		background: #fff;
    		padding: 50px;
            box-shadow: 0 5px 30px 0 #888888;
            text-align: center;
        }
		.thankyou-inner h1 {
            color: #26294a;
    		font-size: 30px;
        }
		.thankyou-inner p {
            color: #777;
    		font-size: 18px;
    		line-height: 28px;
        }
		.thankyou-inner p .btn {
            background: #515a79;
    		color: #fff;
    		font-weight: 800;
    		letter-spacing: 1px;
    		padding: 10px 60px;
    		display: inline-block;
    		border-radius: 3px;
            margin-top: 30px;
        }
		.thankyou-inner .contacts {
        	line-height: 30px;
    		margin: 30px 0;
        }
		.thankyou-inner .contacts strong,
		.thankyou-inner .contacts a {
            color: #26294a !important;
        }
    </style>
    
    <div id="main">

        <?php echo the_content(); ?>

    </div>

<?php endwhile; ?>

<script>
	jQuery(document).ready(function() {
    	if(jQuery('.show-on-load').length) jQuery('.show-on-load').removeClass('show-on-load');
    });
</script>
    
<?php get_footer(); ?>