<?php
/**
 * Template Name: About Management Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $menu_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['banner']['links']);
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    ?>

    <div id="main">

        <div class="banner-widget small">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($menu_pages)): ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($menu_pages as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>"
                                        <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-3">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>"
                                <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="text-content-widget">
            <div class="container m">
                <h2><?php echo $fields['caption']; ?></h2>
                <div class="text">
                    <?php echo $fields['content']; ?>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['profiles'])) : ?>
            <div class="board-members-widget">
                <div class="container m">
                    <h3><?php echo $fields['profiles_heading']; ?></h3>
                    <div class="items cf">
                        <?php foreach ($fields['profiles'] as $k => $profile) : ?>
                            <div class="item">
                                <a
                                   <?php if(!empty($profile['linkedin_url'])): ?>
                                       target="_blank"
                                        href="<?php echo $profile['linkedin_url']?>"
                                   <?php elseif(!empty($profile['bio'])): ?>
                                        data-lity
                                        href="#member-popup-<?php echo $k ?>"
                                   <?php endif; ?>
                                >
                                    <div class="img lozad"
                                     data-background-image="<?php echo $profile['image']['url']; ?>"></div>
                                    <div class="details">
                                    <div class="name"><?php echo $profile['name']; ?></div>
                                    <?php if (!empty($profile['position'])) : ?>
                                        <div class="position"><?php echo $profile['position']; ?></div>
                                    <?php endif; ?>
                                    <div class="cf row">
                                        <div class="company"><?php echo $profile['company']?></div>
                                        <?php if (!empty($profile['linkedin_url'])) : ?>
                                            <div class="linkedin"></div>
                                        <?php else: ?>
                                            <div class="popup"></div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                </a>

                                <?php if(empty($profile['linkedin_url']) && !empty($profile['bio'])): ?>
                                    <div id="member-popup-<?php echo $k?>" class="bio-popup">
                                    <a href="javascript:void(0)" data-lity-close class="close"></a>
                                    <div class="inner-wrapper">
                                        <div class="header cf">
                                            <div class="img-wrapper">
                                                <div class="img lozad"
                                                     data-background-image="<?php echo $profile['image']['url']; ?>"></div>
                                            </div>
                                            <div class="details">
                                                <div class="name"><?php echo $profile['name']; ?></div>
                                                <?php if (!empty($profile['position'])) : ?>
                                                    <div class="position"><?php echo $profile['position']; ?></div>
                                                <?php endif; ?>
                                                <div class="company"><?php echo $profile['company']?></div>
                                            </div>
                                        </div>
                                        <div class="bio-wrapper">
                                            <div class="bio">
                                                <?php echo $profile['bio']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>

                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
