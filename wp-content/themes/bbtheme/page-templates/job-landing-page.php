<?php

/**
 * Template Name: Job Landing Page
 */
get_header();

$success_popup_content = get_config('job_success_popup_content');

$selected_job_id = filter_input(INPUT_GET, 'job_id', FILTER_VALIDATE_INT, array("options" => array(
    "default" => 0,
)));
$posted_success = filter_input(INPUT_GET, 'success', FILTER_VALIDATE_INT, array("options" => array(
    "default" => 0,
)));

$post_full_name = filter_input(INPUT_POST, 'full_name', FILTER_SANITIZE_STRING);
$post_email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
$form_errors = \BlueBeetle\Press\Form::get_instance()->get_errors();

?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    $offices = \BlueBeetle\Press\Generic::get_instance()->get_offices();
    $jobs = \BlueBeetle\Press\Generic::get_instance()->get_jobs();
    ?>

    <script language="JavaScript">
        let error_messages = {
            100: '<?php esc_attr_e('Invalid method', 'bbtheme'); ?>',
            101: '<?php esc_attr_e('Invalid data', 'bbtheme'); ?>',
            102: '<?php esc_attr_e('Invalid email', 'bbtheme'); ?>',
            103: '<?php esc_attr_e('Captcha not validated', 'bbtheme'); ?>',
            104: '<?php esc_attr_e('Please upload only PDF file.', 'bbtheme'); ?>'
        };
    </script>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($fields['banner']['caption']) || !empty($fields['banner']['description'])) : ?>
                        <div class="overlay-wrapper container w">
                            <div class="overlay">
                                <?php if (!empty($fields['banner']['caption'])) : ?>
                                    <div class="caption-wrapper"><?php echo $fields['banner']['caption']; ?></div>
                                <?php endif; ?>
                                <?php if (!empty($fields['banner']['description'])) : ?>
                                    <?php echo $fields['banner']['description']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-3">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>" <?php if ($link['active']) : ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="vacancies-widget <?php echo (empty($jobs) ? 'no-results' : '') ?>">
            <div class="container">
                <div class="filters">
                    <select name="location" id="location">
                        <option value="all" selected><?php _e('All locations', 'bbtheme'); ?></option>
                        <?php foreach ($offices as $office_index => $office) : ?>
                            <?php
                            $caption = get_field('caption', $office->ID);
                            ?>
                            <option value="<?php echo $office->post_name; ?>"><?php echo $caption; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <?php if (!empty($jobs)) : ?>
                    <div class="items">
                        <?php foreach ($jobs as $job_index => $job) : ?>
                            <?php
                            $job_fields = get_fields($job->ID);
                            $office_caption = get_field('caption', $job_fields['office']->ID);
                            ?>
                            <div class="item <?php echo $job_fields['office']->post_name; ?> <?php if ($selected_job_id == $job->ID) : ?> expand <?php endif; ?>">
                                <a href="#" class="close"></a>
                                <div class="columns cf">
                                    <div class="form">
                                        <form action="<?php echo get_ajax_url('job', 'save'); ?>" aaaction="<?php the_permalink(); ?>?job_id=<?php echo $job->ID; ?>" method="post" id="jform-<?php echo $job->ID; ?>" enctype="multipart/form-data" novalidate>
                                            <div class="error"></div>

                                            <div class="overlay">
                                                <div class="loader"></div>
                                            </div>

                                            <div class="form-fields">
                                                <h6><?php _e('Apply Now', 'bbtheme'); ?></h6>
                                                <div class="form-field">
                                                    <input type="text" name="full_name" placeholder="<?php esc_attr_e('Full Name', 'bbtheme'); ?>" value="<?php echo $post_full_name; ?>" required>
                                                </div>
                                                <div class="form-field">
                                                    <input type="email" name="email" value="<?php echo $post_email; ?>" placeholder="<?php esc_attr_e('Email', 'bbtheme'); ?>" required>
                                                </div>
                                                <div class="file-upload form-field">
                                                    <input type="file" name="file" id="file-<?php echo $job->ID; ?>" required accept=".pdf" data-invalid="<?php esc_attr_e('No file chosen', 'bbtheme'); ?>">
                                                    <label for="file-<?php echo $job->ID; ?>" data-default-label="<?php esc_attr_e('Upload Resume', 'bbtheme'); ?>"><?php _e('Upload Resume', 'bbtheme'); ?></label>
                                                </div>
                                                <input type="hidden" name="job_id" value="<?php echo $job->ID; ?>">
                                                <input type="submit" value="<?php esc_attr_e('Send', 'bbtheme'); ?>">
                                            </div>
                                        </form>
                                    </div>
                                    <div class="details">
                                        <div class="header cf">
                                            <h3><?php echo $job_fields['caption']; ?></h3>
                                            <div class="location"><?php echo $office_caption; ?></div>
                                        </div>
                                        <div class="description">
                                            <div class="text-content-widget">
                                                <?php echo $job_fields['content']; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>

                <div id="success-popup" class="message-popup lity-hide">
                    <?php echo $success_popup_content; ?>
                </div>

                <div class="no-results-note">

                    <p class="lead"><?php _e('While we do not have any vacancies at the moment, please visit us soon.'); ?></p>
                    <p><?php _e('New opportunities are updated on this page as they become available.', 'bbtheme'); ?></p>

                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>