<?php
/**
 * Template Name: Press Center Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    $pr_types = \BlueBeetle\Press\Generic::get_instance()->get_taxonomy_pr_type_terms();
    $press_releases = \BlueBeetle\Press\Generic::get_instance()->get_press_releases();

    ?>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($fields['banner']['caption']) || !empty($fields['banner']['description'])) : ?>
                        <div class="overlay-wrapper container w">
                            <div class="overlay">
                                <?php if (!empty($fields['banner']['caption'])) : ?>
                                    <div class="caption-wrapper"><?php echo $fields['banner']['caption']; ?></div>
                                <?php endif; ?>
                                <?php if (!empty($fields['banner']['description'])) : ?>
                                    <?php echo $fields['banner']['description']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-2">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>"
                                <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="press-releases-widget">
            <div class="container">
                <div class="header cf">
                    <?php if (!empty($fields['contacts'])) : ?>
                        <div class="press-contacts-widget">
                            <h6><?php _e('Press Contacts', 'bbtheme'); ?></h6>
                            <div class="contacts">
                                <?php foreach ($fields['contacts'] as $contact) : ?>
                                    <div class="contact cf">
                                        <div class="right">
                                            <?php if (!empty($contact['telephone'])) : ?>
                                                <a href="tel:<?php echo $contact['telephone']; ?>" class="phone"></a>
                                            <?php endif; ?>
                                            <?php if (!empty($contact['email'])) : ?>
                                                <a href="mailto:<?php echo antispambot($contact['email'], 1); ?>"
                                                   class="email"></a>
                                            <?php endif; ?>
                                        </div>
                                        <div class="left">
                                            <div class="name"><?php echo $contact['name']; ?></div>
                                            <div class="job-title"><?php echo $contact['job_title']; ?></div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="filters">
                        <form action="" id="filters-form">
                            <div class="cf">
                                <?php if (!empty($pr_types)): ?>
                                    <div class="dropdown">
                                        <select name="filter-dropdown" id="filter-dropdown">
                                            <option value="all"><?php _e('All', 'bbtheme'); ?></option>
                                            <?php foreach ($pr_types as $term) : ?>
                                                <?php
                                                $term_caption = get_field('caption', $term->taxonomy . '_' . $term->term_id);
                                                ?>
                                                <option value="<?php echo $term->slug; ?>"><?php echo $term_caption; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                <?php endif; ?>

                                <div class="search-field">
                                    <a href="#" class="close"></a>
                                    <input type="text" id="keyword" name="keyword" value=""
                                           placeholder="<?php _e('Search', 'bbtheme'); ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php if (!empty($press_releases)): ?>
                    <div class="press-releases">
                        <h6><?php _e('Press releases', 'bbtheme'); ?></h6>
                        <div class="items">
                            <?php foreach ($press_releases as $post) : ?>
                                <?php
                                $pr_fields = get_fields($post->ID);
                                $zip_download_url = \BlueBeetle\Press\Generic::get_instance()->get_pr_zip_url($post);
                                ?>
                                <div class="item <?php echo $pr_fields['type']->slug; ?>">
                                    <div class="arrow"></div>
                                    <div class="cf">
                                        <div class="date"><?php echo get_the_date('d M Y', $post); ?></div>
                                        <?php if (!empty($zip_download_url)): ?>
                                            <a href="<?php echo $zip_download_url; ?>" class="download"
                                               download=""><?php _e('Download', 'bbtheme'); ?></a>
                                        <?php endif; ?>
                                        <div class="description">
                                            <h4><?php echo $pr_fields['caption']; ?></h4>
                                            <?php echo $pr_fields['content']; ?>
                                        </div>
                                    </div>

                                    <?php if (!empty($pr_fields['files_for_download'])): ?>
                                        <div class="included-files">
                                            <h6><?php _e('Files Included', 'bbtheme'); ?></h6>
                                            <div class="files">
                                                <ul>
                                                    <?php foreach ($pr_fields['files_for_download'] as $file) : ?>
                                                        <li><a href="<?php echo $file['file']['url']; ?>"
                                                               download><?php echo $file['file']['filename']; ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>


    </div>

<?php endwhile; ?>

<?php get_footer(); ?>