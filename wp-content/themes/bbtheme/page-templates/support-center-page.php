<?php
/**
 * Template Name: Support Center Page
 */

get_header('simple');

$header_logo_white = get_config('header_logo_white');

?>

<?php while (have_posts()) : the_post(); ?>

    <?php $fields = get_fields(); ?>

    <div id="main" class="">
        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']?>">
                    <a href="#" class="logo">
                        <img src="<?php echo $header_logo_white['url'] ?>" alt="">
                    </a>
                    <div class="overlay-wrapper container w">
                        <div class="overlay">
                            <div class="caption-wrapper"><?php echo $fields['banner']['caption']?></div>
                            <p><?php echo $fields['banner']['description']?></p>
                        </div>
                    </div>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>


        <div class="ticketing-system-ctas-group-widget">
            <div class="container s">
                <div class="items cf">

                <?php foreach($fields['ticketing_systems'] as $i => $ts) : ?>
                    <div class="item <?php echo $i%2==1?'alt':'';?>">
                        <div class="content">
                            <div class="icon">
                                <img src="<?php echo $ts['icon']['url']?>" alt="">
                            </div>
                            <h3><?php echo $ts['caption']?></h3>
                            <h6><?php echo $ts['location']?></h6>
                            <div class="links">
                                <a href="<?php echo $ts['url']?>" target="_blank" class="">Access Ticketing System</a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer('simple'); ?>