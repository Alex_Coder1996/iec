<?php
/**
 * Template Name: Surveillance portfolio page
 */
get_header();
?>
<?php while (have_posts()) : the_post(); ?>

<?php
$fields = get_fields();
$vas_page = get_config('vas_page');
$vas_page_link = get_permalink($vas_page);
?>

<main id="main">
    <div class="banner-widget">
        <div class="container xw n-p">
            <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                <div class="container w return_back_container">
                    <a href="<?php echo $vas_page_link ?>" class="back-link">
                        <?php _e('Return Back', 'bbtheme'); ?>
                    </a>
                </div>
                <div class="bottom-content">
                    <div class="container w">
                        <h1><?php echo $fields['banner']['caption']; ?></h1>
                        <p><?php echo $fields['banner']['description']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(!empty($fields['section_simple_text']['simple_text'])) : ?>
    <div class="section_simple_text">
        <div class="container w">
            <div class="text-content-widget">
                <?php echo $fields['section_simple_text']['simple_text']; ?>
            </div>
        </div>
    </div>
    <?php endif; ?>

    <?php if (!empty($fields['section_technologies'])) : ?>
        <div class="section_technologies">
            <div class="container w">
                <div class="grid_columns">
                    <div class="transfer_side">
                        <div class="transfer_side_texts">
                            <div class="title">
                                <h2><?php echo $fields['section_technologies']['transfer_side']['title']; ?></h2>
                            </div>
                            <div class="description"><?php echo $fields['section_technologies']['transfer_side']['description']; ?></div>
                        </div>
                        <div class="block_bottom">
                            <div class="image" id="transfer_img">
                                <video class="video" src="<?php echo $fields['section_technologies']['transfer_side']['video_file']; ?>"
                                       poster="/wp-content/themes/bbtheme/img/transfer_img/transfer_img_bg_0.png" loop></video>
                                <div class="img_button">Click to view</div>
                            </div>
                            <div class="button">
                                <a href="<?php echo $fields['section_technologies']['transfer_side']['file']['url']; ?>" download><?php echo $fields['section_technologies']['transfer_side']['download_button_text']; ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="stream_side">
                        <div class="stream_side_texts">
                            <div class="title">
                                <h2><?php echo $fields['section_technologies']['stream_side']['title']; ?></h2>
                            </div>
                            <div class="description"><?php echo $fields['section_technologies']['stream_side']['description']; ?></div>
                        </div>
                        <div class="block_bottom">
                            <div class="image" id="stream_img">
                                <video class="video" src="<?php echo $fields['section_technologies']['stream_side']['video_file']; ?>"
                                       poster="/wp-content/themes/bbtheme/img/stream_img/stream_img_bg_0.png" loop></video>
                                <div class="img_button">Click to view</div>
                            </div>
                            <div class="button">
                                <a href="<?php echo $fields['section_technologies']['stream_side']['file']['url']; ?>" download><?php echo $fields['section_technologies']['stream_side']['download_button_text']; ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!empty($fields['section_surveillance_cases'])) : ?>
        <div class="section_surveillance_cases">
            <div class="section-title">
                <div class="container w">
                    <h3><?php echo $fields['section_surveillance_cases']['section_title']; ?></h3>
                </div>
            </div>
            <div class="need-accordions-widget surveillance_cases">
                <div class="container w n-p">
                    <div class="items">
                        <?php foreach ($fields['section_surveillance_cases']['surveillance_cases'] as $need_index => $need) : ?>
                            <div class="item">
                                <div class="container m">
                                    <div class="title">
                                        <h5><?php echo $need['caption']; ?></h5>
                                    </div>
                                    <div class="content">
                                        <?php echo $need['content']; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
</main>
<?php endwhile; ?>

<?php get_footer(); ?>
