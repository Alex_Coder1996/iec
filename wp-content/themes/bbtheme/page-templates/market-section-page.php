<?php
/**
 * Template Name: Market Section Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $menu_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['banner']['links']);
    ?>
    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($menu_pages)): ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($menu_pages as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>"
                                        <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <div class="text-content-widget">
            <div class="container m">
                <div class="content">
                    <?php if (!empty($fields['banner']['caption'])) : ?>
                        <h1><?php echo $fields['banner']['caption']; ?></h1>
                    <?php endif; ?>
                    <?php if (!empty($fields['banner']['description'])) : ?>
                        <?php echo $fields['banner']['description']; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['section'])) : ?>
            <div class="maritime-services-widget">
                <div class="container m">
                    <div class="items cf">
                        <?php foreach ($fields['section'] as $section_index => $section) : ?>
                            <?php
                            $caption = $section['caption'];
                            if (empty($caption)) {
                                $caption = get_field('caption', $section['page']->ID);
                            }
                            ?>
                            <a class="item" href="<?php echo get_the_permalink($section['page']); ?>">
                                <div class="inner-wrapper lozad"
                                     data-background-image="<?php echo $section['image']['url']; ?>">
                                    <div class="overlay">
                                        <div class="icon lozad"
                                             data-background-image="<?php echo $section['icon']['url']; ?>"></div>
                                        <h6><?php echo $caption; ?></h6>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>