<?php
/**
 * Template Name: About History Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $menu_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['banner']['links']);
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    ?>

    <div id="main">

        <div class="banner-widget small">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($menu_pages)): ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($menu_pages as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>"
                                        <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-3">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>"
                                <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($fields['history'])) : ?>
            <div class="history-widget">
                <div class="container">
                    <div class="items cf">
                        <?php foreach ($fields['history'] as $history_index => $history) : ?>
                            <div class="item <?php if ($history_index == 0): ?> first <?php endif; ?> <?php if ($history_index + 1 == count($fields['history'])): ?> last <?php endif; ?>">
                                <?php if (!empty($history['year'])) : ?>
                                    <h2><?php echo $history['year']; ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($history['caption'])) : ?>
                                    <h4><?php echo $history['caption']; ?></h4>
                                <?php endif; ?>
                                <?php echo $history['description']; ?>
                            </div>
                        <?php endforeach; ?>
                        <div class="line"></div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
