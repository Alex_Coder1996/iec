<?php
/**
 * Template Name: Market Landing Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php $fields = get_fields(); ?>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <div class="bottom-gradient">

                    </div>
                </div>
            </div>
        </div>

        <div class="text-content-widget">
            <div class="container">
                <div class="content">
                    <?php if (!empty($fields['banner']['caption'])) : ?>
                        <h1><?php echo $fields['banner']['caption']; ?></h1>
                    <?php endif; ?>
                    <?php if (!empty($fields['banner']['description'])) : ?>
                        <?php echo $fields['banner']['description']; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['section'])) : ?>
            <div class="vertical-markets-widget">
                <div class="container w n-p">
                    <div class="items cf">
                        <?php foreach ($fields['section'] as $section_index => $section) : ?>
                            <?php
                            $section_links = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($section['links']);
                            $link = array_shift($section_links);
                            ?>
                            <div class="item lozad  secondary-links-<?php echo count($section_links)?>" data-background-image="<?php echo $section['image']['url']; ?>">
                                <div class="overlay">
                                    <a href="<?php echo $link['url']; ?>"
                                       class="primary-link"><?php echo $link['caption']; ?></a>
                                    <?php if (!empty($section_links)) : ?>
                                        <div class="secondary-links">
                                            <?php foreach ($section_links as $link) : ?>
                                                <a href="<?php echo $link['url']; ?>">
                                                    <div class="icon lozad" data-background-image="<?php echo $link['icon']; ?>"></div>
                                                    <div><?php echo $link['caption']; ?></div>
                                                </a>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>