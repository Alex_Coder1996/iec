<?php
/**
 * Template Name: About Landing Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php $fields = get_fields(); ?>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($fields['banner']['caption']) || !empty($fields['banner']['description'])) : ?>
                        <div class="overlay-wrapper container w">
                            <div class="overlay">
                                <?php if (!empty($fields['banner']['caption'])) : ?>
                                    <div class="caption-wrapper"><?php echo $fields['banner']['caption']; ?></div>
                                <?php endif; ?>
                                <?php if (!empty($fields['banner']['description'])) : ?>
                                    <?php echo $fields['banner']['description']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['widget'])) : ?>
            <div class="link-ctas-group-widget">
                <div class="container m">
                    <div class="items cf">
                        <?php foreach ($fields['widget'] as $widget_index => $widget) : ?>
                            <div class="item <?php if ($widget_index % 2 == 0): ?> alt <?php endif; ?>">
                                <div class="content">
                                    <?php if (!empty($widget['caption'])) : ?>
                                        <h3><?php echo $widget['caption']; ?></h3>
                                    <?php endif; ?>
                                    <?php if (!empty($widget['description'])) : ?>
                                        <p><?php echo $widget['description']; ?></p>
                                    <?php endif; ?>
                                    <?php if (!empty($widget['links'])) : ?>
                                        <?php
                                        $links = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($widget['links']);
                                        ?>
                                        <div class="links">
                                            <?php foreach ($links as $link) : ?>
                                                <a href="<?php echo $link['url']; ?>"><?php echo $link['caption']; ?></a>
                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
