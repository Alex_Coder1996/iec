<?php
/**
 * Template Name: About Offerings Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $menu_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['banner']['links']);
    ?>

    <div id="main">

        <div class="banner-widget small">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($menu_pages)): ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($menu_pages as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>"
                                        <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient">
                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['widget'])) : ?>
            <div class="our-offerings-widget">
                <div class="container m">
                    <div class="items cf">

                        <?php foreach ($fields['widget'] as $widget) : ?>
                            <div class="item">
                                <div class="inner-wrapper">
                                    <div class="img lozad"
                                         data-background-image="<?php echo $widget['image']['url']; ?>"></div>
                                    <h3><?php echo $widget['caption']; ?></h3>
                                    <?php echo $widget['description']; ?>
                                    <?php if (!empty($widget['link'])) : ?>
                                        <a href="<?php echo $widget['link']['url']; ?>"
                                           target="<?php echo $widget['link']['target']; ?>">
                                            <?php echo $widget['link_caption']; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
