<?php
/**
 * Template Name: About Introduction Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $menu_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['banner']['links']);
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    ?>

    <div id="main">

        <div class="banner-widget small">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($menu_pages)): ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($menu_pages as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>"
                                        <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-3">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>"
                                <?php if ($link['active']): ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="text-content-widget">
            <div class="container m">
                <h2><?php echo $fields['caption']; ?></h2>
                <div class="text columns-2">
                    <?php echo $fields['content']; ?>
                </div>
            </div>
        </div>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
