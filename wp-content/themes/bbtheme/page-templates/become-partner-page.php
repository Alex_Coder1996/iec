<?php

/**
 * Template Name: Become Partner Page
 */
get_header();

$success_popup_content = get_config('enquiry_success_popup_content');
$success_popup_content_key = array_search('become-partner', array_column($success_popup_content, 'form_type'));

?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['tabs']);
    $offices = \BlueBeetle\Press\Generic::get_instance()->get_offices();

    $current_page_id = get_the_ID();
    $countries = \BlueBeetle\Press\Common::get_instance()->get_countries();
    $form_interests = get_config('enquiry_form_interest');
    ?>
    <script language="JavaScript">
        let error_messages = {
            100: '<?php esc_attr_e('Invalid method', 'bbtheme'); ?>',
            101: '<?php esc_attr_e('Invalid data', 'bbtheme'); ?>',
            102: '<?php esc_attr_e('Invalid email', 'bbtheme'); ?>',
            103: '<?php esc_attr_e('Captcha not validated', 'bbtheme'); ?>'
        };
    </script>
    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($fields['banner']['caption']) || !empty($fields['banner']['description'])) : ?>
                        <div class="overlay-wrapper container w">
                            <div class="overlay">
                                <?php if (!empty($fields['banner']['caption'])) : ?>
                                    <div class="caption-wrapper"><?php echo $fields['banner']['caption']; ?></div>
                                <?php endif; ?>
                                <?php if (!empty($fields['banner']['description'])) : ?>
                                    <?php echo $fields['banner']['description']; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <?php if (!empty($tab_pages)) : ?>
            <div class="tab-links-widget links-3">
                <div class="container w n-p">
                    <div class="links cf">
                        <?php foreach ($tab_pages as $link) : ?>
                            <a href="<?php echo $link['url']; ?>" <?php if ($link['active']) : ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="become-partner-widget">
            <div class="container s">
                <form action="<?php echo get_ajax_url('enquiry', 'save'); ?>" method="post" id="enquiry-form" novalidate>
                    <input type="hidden" name="form_type" value="become-partner">
                    <div class="columns cf">
                        <div class="col right">
                            <div class="text-content-widget">
                                <div class="content">
                                    <?php if (!empty($fields['title'])) : ?>
                                        <h5><?php echo $fields['title']; ?></h5>
                                    <?php endif; ?>
                                    <?php echo $fields['content']; ?>
                                </div>
                            </div>
                        </div>
                        <div class="col left">

                            <div class="form-fields">
                                <h6><?php _e('Write us a message', 'bbtheme'); ?></h6>
                                <div class="error">
                                </div>
                                <div class="">

                                    <div class="field">
                                        <div class="form-field">
                                            <input type="text" name="full_name" placeholder="<?php esc_attr_e('Full Name...', 'bbtheme'); ?>" required>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="form-field">
                                            <input type="text" name="company" placeholder="<?php esc_attr_e('Company...', 'bbtheme'); ?>">
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="form-field">
                                            <input type="email" name="email" placeholder="<?php esc_attr_e('Email...', 'bbtheme'); ?>" required>
                                        </div>
                                    </div>
                                    
                                    <?php if (!empty($countries)) : ?>
                                    <div class="field">
                                        <div class="form-field">
                                            <select name="country" id="" required class="select2" data-select2-theme="grey-border">
                                                <option value=""><?php _e('Country...', 'bbtheme'); ?></option>
                                                <?php foreach ($countries as $country) : ?>
                                                    <option value="<?php echo $country['country_code']; ?>"><?php echo $country['country_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php endif; ?>

                                    <?php if (!empty($form_interests)):  ?>
                                    <div class="field">
                                        <div class="form-field">
                                            <select name="interests[]" required multiple class="select2 select2-interest" data-select2-theme="grey-border" data-select2-hide-search="true" data-select2-placeholder="Interest...">
                                                <option value=""><?php _e('Interest...', 'bbtheme');  ?></option>
                                                <?php foreach ($form_interests as $interest): ?>
                                                    <option value="<?php echo $interest['text'];  ?>" <?php echo (filter_input(INPUT_GET, 'interest', FILTER_SANITIZE_STRING) == $interest['text'])?'selected':''?>><?php echo $interest['text']; ?></option>
                                                <?php endforeach;  ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php endif;  ?>

                                    <div class="field">
                                        <div class="form-field">
                                            <textarea name="message" id="" cols="30" rows="4" placeholder="<?php esc_attr_e('Message...', 'bbtheme'); ?>" class="expand-on-click" required></textarea>
                                        </div>
                                    </div>

                                    <div class="field">
                                        <div class="form-field  no-border google-captcha-wrapper">
                                            <div id="google-captcha-holder"
                                                 data-sitekey="<?php echo get_config('bbpress_google_captcha_site_key'); ?>">
                                                <div class="g-recaptcha" id="g-recaptcha">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div>
                                        <input type="hidden" name="page_url" value="<?php the_permalink($current_page_id); ?>">
                                        <input type="submit" value="<?php esc_attr_e('Send', 'bbtheme'); ?>">
                                    </div>


                                </div>
                            </div>

                            <div class="overlay">
                                <div class="loader"></div>
                            </div>



                            <?php /* ?>
                        <h6><?php _e('Write us a message', 'bbtheme'); ?></h6>
                        <div class="hubspot-form">
                            <!--[if lte IE 8]>
                            <script charset="utf-8" type="text/javascript"
                                    src="//js.hsforms.net/forms/v2-legacy.js"></script>
                            <![endif]-->
                            <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                            <script>
                                hbspt.forms.create({
                                    portalId: "<?php echo $fields['hubspot_form']['portal_id']; ?>",
                                    formId: "<?php echo $fields['hubspot_form']['form_id']; ?>"
                                });
                            </script>
                        </div>
                        <?php */ ?>
                        </div>
                    </div>

                    <div id="success-popup" class="message-popup lity-hide">
                        <?php echo $success_popup_content[$success_popup_content_key]['content']; ?>
                    </div>

                </form>
            </div>
        </div>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>