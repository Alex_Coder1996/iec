<?php

/**
 * Template Name: Market Detail Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $current_page_id = get_the_ID();
    $menu_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links($fields['banner']['links']);

    $countries = \BlueBeetle\Press\Common::get_instance()->get_countries();
    $form_interests = get_config('enquiry_form_interest');

    $success_popup_content = get_config('enquiry_success_popup_content');
    $success_popup_content_key = array_search('market-enquiry', array_column($success_popup_content, 'form_type'));
    ?>

    <script language="JavaScript">
        let error_messages = {
            100: '<?php esc_attr_e('Invalid method', 'bbtheme'); ?>',
            101: '<?php esc_attr_e('Invalid data', 'bbtheme'); ?>',
            102: '<?php esc_attr_e('Invalid email', 'bbtheme'); ?>',
            103: '<?php esc_attr_e('Captcha not validated', 'bbtheme'); ?>'
        };
    </script>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <?php if (!empty($menu_pages)) : ?>
                        <div class="container w n-p links-wrapper">
                            <div class="links">
                                <?php foreach ($menu_pages as $link) : ?>
                                    <a href="<?php echo $link['url']; ?>" <?php if ($link['active']) : ?> class="active" <?php endif; ?>><?php echo $link['caption']; ?></a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <div class="columns">
            <div class="container m">
                <div class="cf">
                    <div class="left">
                        <div class="text-content-widget">
                            <div class="content">
                                <h1><?php echo $fields['caption']; ?></h1>
                                <?php echo $fields['content']; ?>
                            </div>
                        </div>
                    </div>
                    <div class="right">
                        <div class="enquiry-form-widget">


                            <form action="<?php echo get_ajax_url('enquiry', 'save'); ?>" method="post" id="enquiry-form" novalidate>
                                <div class="form-fields">
                                    <h5>
                                        <?php _e('Enquire Now', 'bbtheme'); ?>
                                    </h5>

                                    <div class="error">
                                        <?php _e('Found some issues, Try again later.', 'bbtheme'); ?>
                                    </div>

                                    <input type="hidden" name="form_type" value="market-enquiry">

                                    <div class="form-field">
                                        <input type="text" name="full_name" placeholder="<?php esc_attr_e('Full Name...', 'bbtheme'); ?>" required>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" name="company" placeholder="<?php esc_attr_e('Company...', 'bbtheme'); ?>">
                                    </div>
                                    <div class="form-field">
                                        <input type="email" name="email" placeholder="<?php esc_attr_e('Email...', 'bbtheme'); ?>" required>
                                    </div>
                                    <?php if (!empty($countries)) : ?>
                                        <div class="form-field">
                                            <select name="country" id="" required class="select2">
                                                <option value=""><?php _e('Country...', 'bbtheme'); ?></option>
                                                <?php foreach ($countries as $country) : ?>
                                                    <option value="<?php echo $country['country_code']; ?>"><?php echo $country['country_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($form_interests)):  ?>
                                    
                                    <div class="form-field">
                                        <select name="interests[]" required multiple class="select2 select2-interest" data-select2-hide-search="true" data-select2-placeholder="Interest...">
                                            <option value=""><?php _e('Interest...', 'bbtheme');  ?></option>
                                            <?php foreach ($form_interests as $interest): ?>
                                                <option value="<?php echo $interest['text'];  ?>" <?php echo (in_array($interest['text'], [$fields['caption'], $fields['interest']]))?'selected':'';?>><?php echo $interest['text']; ?></option>
                                            <?php endforeach;  ?>
                                        </select>
                                    </div>
                                    <?php endif;  ?>

                                    <div class="form-field no-border">
                                        <textarea name="message" id="" cols="30" rows="4" placeholder="<?php esc_attr_e('Message...', 'bbtheme'); ?>" class="expand-on-click" required></textarea>
                                    </div>

                                    <div class="form-field  no-border google-captcha-wrapper">
                                    <div id="google-captcha-holder"
                                         data-sitekey="<?php echo get_config('bbpress_google_captcha_site_key'); ?>">
                                        <div class="g-recaptcha" id="g-recaptcha">
                                        </div>
                                    </div>
                                    </div>

                                    <input type="hidden" name="page_url" value="<?php the_permalink($current_page_id); ?>">

                                    <div class="form-button">
                                        <button name="submit"><?php _e('Send', 'bbtheme'); ?></button>
                                    </div>
                                </div>
                                <div class="loading-overlay">
                                    <div class="loader"></div>
                                </div>
                                <div class="success">
                                    <h5><?php _e('Thank You!', 'bbtheme'); ?></h5>
                                    <?php _e('Your inquiry has been well received. One of our customer service representatives will contact you shortly.', 'bbtheme'); ?>
                                </div>

                            </form>
                        </div>

                        <div id="success-popup" class="message-popup lity-hide">
                            <?php echo $success_popup_content[$success_popup_content_key]['content']; ?>
                        </div>

                        <?php if (!empty($fields['downloads'])) : ?>
                            <?php foreach ($fields['downloads'] as $download) : ?>
                                <a href="<?php echo $download['download_file']['url']; ?>" download class="download-button">
                                    <div class="inner-wrapper cf">
                                        <div class="icon"></div>
                                        <div class="text <?php echo (empty($download['caption']) || empty($download['sub-caption'])) ? 'has-one-caption' : '' ?>">
                                            <?php if (!empty($download['caption'])) : ?>
                                                <span class="caption"><?php echo $download['caption']; ?></span>
                                            <?php endif; ?>
                                            <?php if (!empty($download['sub_caption'])) : ?>
                                                <span class="<?php echo (empty($download['caption']) ? 'caption' : 'sub-caption'); ?>"><?php echo $download['sub_caption']; ?></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['needs'])) : ?>
            <div class="need-accordions-widget">
                <div class="container w n-p">
                    <div class="items">
                        <?php foreach ($fields['needs'] as $need_index => $need) : ?>
                            <div class="item  <?php if ($need_index == 0) : ?> expand <?php endif; ?>">
                                <div class="container m">
                                    <div class="title">
                                        <h5><?php echo $need['caption']; ?></h5>
                                    </div>
                                    <div class="content">
                                        <?php echo $need['content']; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>


        <?php if (!empty($fields['recommended_products'])) : ?>
            <div class="recommended-products-widget">
                <div class="container m">
                    <h5><?php _e('Recommended Products', 'bbtheme'); ?></h5>
                    <div class="items cf">
                        <?php foreach ($fields['recommended_products'] as $product) : ?>
                            <?php
                            $caption = get_field('caption', $product->ID);
                            $summary = get_field('summary', $product->ID);
                            $landing_image = get_field('landing_image', $product->ID);
                            $applications = get_field('ps_filter_application', $product->ID);
                            $app_filters = get_ps_filter_choices('application');
                            ?>
                            <div class="item">
                                <a href="<?php echo get_permalink($product); ?>" class="inner-wrapper">
                                    <div class="thumb-wrapper">
                                        <div class="img lozad" data-background-image="<?php echo $landing_image['url']; ?>"></div>
                                        <?php if (!empty($applications)) : ?>
                                            <div class="cats cats-<?php echo count($applications) ?>">
                                                <?php foreach ($applications as $application) : ?>
                                                    <div class="<?php echo $application; ?>">
                                                        <div><?php echo $app_filters[$application]; ?></div>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <h6><?php echo $caption; ?></h6>
                                </a>
                            </div>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($fields['recommended_solutions'])) : ?>
            <div class="recommended-solutions-widget">
                <div class="container w n-p">
                    <div class="wrapper">
                        <div class="container m">
                            <h5><?php _e('Recommended Solutions', 'bbtheme'); ?></h5>
                            <div class="items cf">
                                <?php foreach ($fields['recommended_solutions'] as $solution) : ?>
                                    <?php
                                    $caption = get_field('caption', $solution->ID);
                                    $summary = get_field('summary', $solution->ID);
                                    $landing_image = get_field('landing_image', $solution->ID);
                                    $applications = get_field('ps_filter_application', $solution->ID);
                                    $app_filters = get_ps_filter_choices('application');
                                    ?>
                                    <div class="item">
                                        <a href="<?php echo get_permalink($solution); ?>" class="inner-wrapper">
                                            <div class="thumb-wrapper">
                                                <div class="img lozad" data-background-image="<?php echo $landing_image['url']; ?>"></div>
                                                <?php if (!empty($applications)) : ?>
                                                    <div class="cats cats-<?php echo count($applications) ?>">
                                                        <?php foreach ($applications as $application) : ?>
                                                            <div class="<?php echo $application; ?>">
                                                                <div><?php echo $app_filters[$application]; ?></div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <h6><?php echo $caption; ?></h6>
                                            <p><?php echo $summary; ?></p>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>