<?php

/**
 * Template Name: VAS Detail Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $vas_page = get_config('vas_page');
    $vas_page_link = get_permalink($vas_page);
    ?>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <div class="container w return_back_container">
                        <a href="<?php echo $vas_page_link ?>" class="back-link">
                            <?php _e('Return Back', 'bbtheme'); ?>
                        </a>
                    </div>
                    <div class="bottom-gradient"></div>
                </div>
            </div>
        </div>

        <div class="text-content-widget">
            <div class="container m">
                <div class="content">
                    <h1><?php echo $fields['caption']; ?></h1>
                </div>
            </div>
        </div>

        <div class="container m">
            <div class="columns cf">
                <div class="left">
                    <div class="text-content-widget">
                        <?php echo $fields['content']; ?>
                    </div>
                </div>

                <div class="right">
                    <?php if (!empty($fields['key_points']['points'])) : ?>
                        <div class="key-points-widget">
                            <?php if (!empty($fields['key_points']['caption'])) : ?>
                                <h5><?php echo $fields['key_points']['caption']; ?></h5>
                            <?php endif; ?>
                            <?php foreach ($fields['key_points']['points'] as $key_points) : ?>
                                <div><?php echo $key_points['text']; ?></div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?php
                     if (count($fields['additional_buttons']) > 0) : ?>
                        <div class="additional-buttons-widget">
                            <?php foreach ($fields['additional_buttons'] as $additional_button) : ?>
                                <a href="<?php echo $additional_button['link']; ?>" class="" target="<?php echo $additional_button['target']; ?>">
                                    <?php echo $additional_button['label']; ?>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>


                    <?php if (!empty($fields['downloads'])) : ?>
                        <?php foreach ($fields['downloads'] as $download) : ?>
                            <a href="<?php echo $download['download_file']['url']; ?>" download class="download-button">
                                <div class="inner-wrapper cf">
                                    <div class="icon"></div>
                                    <div class="text <?php echo (empty($download['caption']) || empty($download['sub-caption'])) ? 'has-one-caption' : '' ?>">
                                        <?php if (!empty($download['caption'])) : ?>
                                            <span class="caption"><?php echo $download['caption']; ?></span>
                                        <?php endif; ?>
                                        <?php if (!empty($download['sub_caption'])) : ?>
                                            <span class="<?php echo (empty($download['caption']) ? 'caption' : 'sub-caption'); ?>"><?php echo $download['sub_caption']; ?></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>