<?php
/**
 * Template Name: Generic Content Page
 */
get_header();
?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php
	$fields = get_fields();
	$tab_pages = \BlueBeetle\Press\Generic::get_instance()->get_banner_links( $fields['tabs'] );
	?>

    <div id="main" class="">
        <div class="text-content-widget">
            <div class="container">
                <div class="content">
                    <h1><?php echo $fields['caption']; ?></h1>
	                <?php echo $fields['content']; ?>
                </div>
            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer(); ?>