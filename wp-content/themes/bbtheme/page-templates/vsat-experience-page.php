<?php
/**
 * Template Name: VSAT Experience
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $iec_enquire_page = get_config('iec_enquire_page');
    $vas_page = get_config('vas_page');
    $vas_page_link = get_permalink($vas_page);
    ?>

    <div id="main">

        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <div class="container w return_back_container">
                        <a href="<?php echo $vas_page_link ?>" class="back-link">
                            <?php _e('Return Back', 'bbtheme'); ?>
                        </a>
                    </div>
                    <div class="bottom-content">
                        <div class="container m">
                            <h5><?php echo $fields['banner']['caption']; ?></h5>
                            <p><?php echo $fields['banner']['description']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container m">
            <div class="columns cf">
                <div class="left">
                    <div class="text-content-widget">
                        <?php echo $fields['content']; ?>
                    </div>
                    <div class="buttons-columns">
                    <?php if (!empty($fields['downloads'])) : ?>
                        <?php foreach ($fields['downloads'] as $download) : ?>
                            <div class="col_left">
                                <a href="<?php echo $download['download_file']['url']; ?>" download class="download-button">
                                    <div class="inner-wrapper cf">
                                        <div class="icon"></div>
                                        <div class="text <?php echo (empty($download['caption']) || empty($download['sub-caption'])) ? 'has-one-caption' : '' ?>">
                                            <?php if (!empty($download['caption'])) : ?>
                                                <span class="caption"><?php echo $download['caption']; ?></span>
                                            <?php endif; ?>
                                            <?php if (!empty($download['sub_caption'])) : ?>
                                                <span class="<?php echo (empty($download['caption']) ? 'caption' : 'sub-caption'); ?>"><?php echo $download['sub_caption']; ?></span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                            <div class="col_right">
                                <a href="<?php echo get_permalink($iec_enquire_page); ?>" class="link-button"><?php _e('Enquire now', 'bbtheme'); ?></a>
                            </div>
                    </div>
                </div>

                <div class="right">
                    <?php if (!empty($fields['add_on'])) : ?>
                        <?php foreach ($fields['add_on'] as $section) : ?>
                            <div class="add-on add-on-<?php echo $section['block']['columns_number']; ?> <?php echo $section['block']['background_position']; ?>"
                                 style="background-image: url(<?php echo $section['block']['background_image']['url'] ;?>)">
                                <div class="add-on-content">
                                    <div class="label">
                                        <h5>add on: <span><?php echo $section['block']['caption'];?></span></h5>
                                    </div>
                                    <div class="links">
                                        <?php foreach ($section['block']['links'] as $link) : ?>
                                            <div class="link">
                                                <a href="<?php echo $link['url'];?>"><?php echo $link['label'];?></a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if (!empty($fields['icons'])) : ?>
        <div class="container m">
            <div class="icons">
                <?php foreach ($fields['icons'] as $block) : ?>
                    <div class="icon">
                        <div class="img-wrapper">
                            <img src="<?php echo $block['icon']['url']; ?>" alt="<?php echo $block['caption']; ?>">
                        </div>
                        <div class="text">
                            <?php echo $block['caption']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>