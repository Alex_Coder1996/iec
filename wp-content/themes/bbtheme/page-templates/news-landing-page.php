<?php

/**
 * Template Name: News Landing Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php

    $fields = get_fields();
    $news_types = \BlueBeetle\Press\Generic::get_instance()->get_taxonomy_news_type_terms();
    ?>

    <script>
        var CONFIG = {
            newsEventsAPI: '<?php echo get_ajax_url('news', 'search'); ?>'
        }
    </script>

    <div id="main">

        <div class="news-events-widget">
            <div class="container">
                <div class="filters">
                    <form id="filters-form">
                        <input type="hidden" name="start-at" value="0" />
                        <div class="cf">
                            <div class="search-field">
                                <a href="#" class="close"></a>
                                <input type="text" name="keyword" placeholder="<?php _e('Search news', 'bbtheme'); ?>" id="keyword">
                            </div>

                            <?php if (!empty($news_types)) : ?>
                                <div class="categories">
                                    <label for="filter-all">
                                        <input type="checkbox" name="terms[]" value='-1' id="filter-all">
                                        <div><?php echo _e('All', 'bbtheme'); ?></div>
                                    </label>

                                    <?php foreach ($news_types as $term) : ?>
                                        <?php
                                        $term_caption = get_field('caption', $term->taxonomy . '_' . $term->term_id);
                                        ?>
                                        <label for="filter-<?php echo $term->slug; ?>">
                                            <input type="checkbox" name="terms[]" value="<?php echo $term->term_id; ?>" id="filter-<?php echo $term->slug; ?>">
                                            <div><?php echo $term_caption; ?></div>
                                        </label>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </form>
                </div>

                <div class="listing" id="items-listing">
                    <div class="items cf">

                    </div>
                </div>

                <div class="loading-overlay">
                    <div class="loader"></div>
                </div>

                <div class="load-more">
                    <a href="#"><?php _e('Load More', 'bbtheme'); ?></a>
                    <div class="loader"></div>
                </div>

                <div class="no-results"><?php _e('No results found.', 'bbtheme'); ?></div>
            </div>
        </div>

    </div>


<?php endwhile; ?>

<?php get_footer(); ?>