<?php
/**
 * Template Name: VAS Detail V2 Page
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>

    <?php
    $fields = get_fields();
    $vas_page = get_config('vas_page');
    $vas_page_link = get_permalink($vas_page);
    ?>

    <main id="main">
        <div class="banner-widget">
            <div class="container xw n-p">
                <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                    <div class="container w return_back_container">
                        <a href="<?php echo $vas_page_link ?>" class="back-link">
                            <?php _e('Return Back', 'bbtheme'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-content">
            <div class="container m">
                <div class="title-content"><h1><?php echo $fields['content']['title']; ?></h1></div>
                <div class="text-content"><?php echo $fields['content']['content']; ?>
                    <?php if (!empty($fields['content']['file'])) : ?>
                        <div class="button-content"><a href="<?php echo $fields['content']['file']['url']; ?>" download><?php echo $fields['section_boxes']['download_button_text']; ?></a></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="boxes-grid-content">
                <div class="container m box-items">
                    <?php foreach ($fields['section_boxes']['boxes_block'] as $box_item) : ?>
                        <div class="box-item">
                            <div class="box-icon"><img src="<?php echo $box_item['icon']['url']; ?>" alt=""></div>
                            <div class="box-text"><?php echo $box_item['text']; ?></div>
                            <div class="box-button">
                                <?php if (!empty($box_item['file'])) : ?>
                                    <a href="<?php echo $box_item['file']['url']; ?>" download><?php echo $fields['section_boxes']['download_button_text']; ?></a>
                                <?php else : ?>
                                    <span><?php echo $fields['section_boxes']['coming_soon_text']; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </main>

<?php endwhile; ?>

<?php get_footer(); ?>
