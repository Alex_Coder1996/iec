<?php

//Init Theme Local

load_theme_textdomain('bbtheme', get_template_directory() . '/languages');

/**
 * @var $bbTheme \BlueBeetle\Press\Theme
 */
global $bbTheme;

if (class_exists('\\BlueBeetle\\Press\\Theme')) {
    $bbTheme = \BlueBeetle\Press\Theme::get_instance();

    $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://s3.eu-west-1.amazonaws.com/bb.wp.updates/iec-telecom.com/bbtheme/theme.json',
        __FILE__, //Full path to the main plugin file or functions.php.
        'bbpress'
    );

}

function itsme_disable_feed() {
	wp_die( __( 'No feed available, please visit the <a href="'. esc_url( home_url( '/' ) ) .'">homepage</a>!' ) );
}

add_action('do_feed', 'itsme_disable_feed', 1);
add_action('do_feed_rdf', 'itsme_disable_feed', 1);
add_action('do_feed_rss', 'itsme_disable_feed', 1);
add_action('do_feed_rss2', 'itsme_disable_feed', 1);
add_action('do_feed_atom', 'itsme_disable_feed', 1);
add_action('do_feed_rss2_comments', 'itsme_disable_feed', 1);
add_action('do_feed_atom_comments', 'itsme_disable_feed', 1);

include_once('includes' . DIRECTORY_SEPARATOR . 'filters.php');

/*add_filter('wpseo_json_ld_output', '__return_false');*/