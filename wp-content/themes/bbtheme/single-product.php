<?php
get_header();

global $wp;
$current_page_link = home_url( $wp->request );
?>

<?php while (have_posts()) : the_post(); ?>

    <?php

    $fields = get_fields();
    $current_page_id = get_the_ID();
    $countries = \BlueBeetle\Press\Common::get_instance()->get_countries();
    $form_interests = get_config('enquiry_form_interest');
    $success_popup_content = get_config('enquiry_success_popup_content');
    $success_popup_content_key = array_search('product-enquiry', array_column($success_popup_content, 'form_type'));
    $iec_ps_page = get_config('iec_ps_page');
    $iec_ps_page_link = get_permalink($iec_ps_page);
    $iec_enquire_page = get_config('iec_enquire_page');
    ?>

    <script language="JavaScript">
        let error_messages = {
            100: '<?php esc_attr_e('Invalid method', 'bbtheme'); ?>',
            101: '<?php esc_attr_e('Invalid data', 'bbtheme'); ?>',
            102: '<?php esc_attr_e('Invalid email', 'bbtheme'); ?>',
            103: '<?php esc_attr_e('Captcha not validated', 'bbtheme'); ?>'
        };
    </script>

    <div id="main">
        <div class="top-body-pattern"></div>

        <div class="container w">
            <a href="<?php echo $iec_ps_page_link; ?>"
               class="show-on-load back-link <?php echo ($_SERVER['HTTP_REFERER'] == $iec_ps_page_link) ? 'history-back':''; ?>"><?php _e('Return Back', 'bbtheme'); ?></a>
        </div>

        <div class="container m">
            <div class="columns cf">
                <div class="left">

                    <?php if (!empty($fields['product_images'])) : ?>
                        <div class="image-slider-widget">
                            <div class="slider">
                                <?php foreach ($fields['product_images'] as $image_index => $image) : ?>
                                    <?php if ($image_index == 0) : ?>
                                        <div class="slide">
                                            <img src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php else: ?>
                                        <div class="slide">
                                            <img class="lozad" data-src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if (!empty($fields['coverage_images'])) : ?>
                        <div class="image-slider-widget">
                            <div class="slider">
                                <?php foreach ($fields['coverage_images'] as $image_index => $image) : ?>
                                    <?php if ($image_index == 0) : ?>
                                        <div class="slide">
                                            <img src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php else: ?>
                                        <div class="slide">
                                            <img class="lozad" data-src="<?php echo $image['image']['url']; ?>">
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
                <div class="right">
                    <div class="text-content-widget">
                        <h1><?php echo $fields['caption']; ?></h1>
                        <?php echo $fields['content']; ?>
                    </div>

                    <?php if (!empty($fields['key_points']['points'])) : ?>
                        <div class="key-points-widget">
                            <?php if (!empty($fields['key_points']['caption'])) : ?>
                                <h5><?php echo $fields['key_points']['caption']; ?></h5>
                            <?php endif; ?>
                            <?php foreach ($fields['key_points']['points'] as $key_points) : ?>
                                <div><?php echo $key_points['text']; ?></div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="mid-body-pattern"></div>

        <?php if (!empty($fields['banner']['image']['url'])) : ?>
        <div class="container w">
            <div class="banner-widget">
                <div class="container xw n-p">

                    <?php if (!empty($fields['banner']['url'])) : ?>
                    <a href="<?php echo $fields['banner']['url']; ?>" class="" target="_blank">
                    <?php endif; ?>
                    <div class="banner lozad" data-background-image="<?php echo $fields['banner']['image']['url']; ?>">
                        <?php if (!empty($fields['banner']['caption']) || !empty($fields['banner']['description'])) : ?>
                            <div class="overlay-wrapper container w">
                                <div class="overlay">
                                    <?php if (!empty($fields['banner']['caption'])) : ?>
                                        <div class="caption-wrapper"><?php echo $fields['banner']['caption']; ?></div>
                                    <?php endif; ?>
                                    <?php if (!empty($fields['banner']['description'])) : ?>
                                        <?php echo $fields['banner']['description']; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($fields['banner']['caption'])) : ?>
                        <div class="bottom-gradient"></div>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($fields['banner']['url'])) : ?>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="container w">
            <div class="buttons-widget">
                <div class="container w">
                    <div class="wrapper">
                        <div class="container m">
                            <div class="cols cf">
                                <?php if (!empty($fields['custom_button']['title']) && !empty($fields['custom_button']['url'])) : ?>
                                    <div>
                                        <a href="<?php echo $fields['custom_button']['url']; ?>" target="<?php echo $fields['custom_button']['target']; ?>"
                                           class="custom-button"><?php echo $fields['custom_button']['title']; ?></a>
                                    </div>
                                <?php else: ?>
                                    <?php if (!empty($fields['brochure'])) : ?>
                                        <div>
                                            <a href="<?php echo $fields['brochure']['url']; ?>" download
                                               class="download-brochure"><?php _e('Download Brochure', 'bbtheme'); ?></a>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <div>
                                    <a href="<?php echo get_permalink($iec_enquire_page); ?>?interest=<?php echo urlencode($fields['caption']); ?>&referrer=<?php echo urlencode($current_page_link); ?>"><?php _e('Our offices', 'bbtheme'); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!empty($fields['section_form'])) : ?>
        <div class="form-section">
            <div class="container">
                <div class="enquiry-form-widget">
                    <form action="<?php echo get_ajax_url('enquiry', 'save'); ?>" method="post" id="enquiry-form" novalidate>
                        <h5><?php _e('Enquire Now', 'bbtheme'); ?></h5>
                        <div class="form-fields">
                            <div class="error">
                                <?php _e('Found some issues, Try again later.', 'bbtheme'); ?>
                            </div>

                            <div class="s_columns">
                                <div class="s_columns_left">
                                    <div class="form-field">
                                        <input type="text" name="full_name" placeholder="<?php esc_attr_e('Full Name...', 'bbtheme'); ?>" required>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" name="company" placeholder="<?php esc_attr_e('Company...', 'bbtheme'); ?>">
                                    </div>
                                    <div class="form-field">
                                        <input type="email" name="email" placeholder="<?php esc_attr_e('Email...', 'bbtheme'); ?>" required>
                                    </div>
                                </div>
                                <div class="s_columns_right">
                                    <?php if (!empty($countries)) : ?>
                                        <div class="form-field">
                                            <select name="country" id="" required class="select2">
                                                <option value=""><?php _e('Country...', 'bbtheme'); ?></option>
                                                <?php foreach ($countries as $country) : ?>
                                                    <option value="<?php echo $country['country_code']; ?>"><?php echo $country['country_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (!empty($form_interests)):  ?>

                                        <div class="form-field">
                                            <select name="interests[]" required multiple class="select2 select2-interest" data-select2-hide-search="true" data-select2-placeholder="Interest...">
                                                <option value=""><?php _e('Interest...', 'bbtheme');  ?></option>
                                                <?php foreach ($form_interests as $interest): ?>
                                                    <option value="<?php echo $interest['text'];  ?>" <?php echo (in_array($interest['text'], [$fields['caption'], $fields['interest']]))?'selected':'';?>><?php echo $interest['text']; ?></option>
                                                <?php endforeach;  ?>
                                            </select>
                                        </div>
                                    <?php endif;  ?>

                                    <div class="form-field no-border">
                                        <textarea name="message" id="" cols="30" rows="4" placeholder="<?php esc_attr_e('Message...', 'bbtheme'); ?>" class="expand-on-click" required></textarea>
                                    </div>

                                    <input type="hidden" name="form_type" value="product-enquiry">
                                    <input type="hidden" name="page_url" value="<?php the_permalink($current_page_id); ?>">
                                </div>
                            </div>
                            <div class="form-field  no-border google-captcha-wrapper">
                                <div id="google-captcha-holder"
                                     data-sitekey="<?php echo get_config('bbpress_google_captcha_site_key'); ?>">
                                    <div class="g-recaptcha" id="g-recaptcha">
                                    </div>
                                </div>
                            </div>

                            <div class="form-button">
                                <button name="submit"><?php _e('Send', 'bbtheme'); ?></button>
                            </div>
                        </div>
                        <div class="loading-overlay">
                            <div class="loader"></div>
                        </div>
                        <div class="success">
                            <h5><?php _e('Thank You!', 'bbtheme'); ?></h5>
                            <?php _e('Your inquiry has been well received. One of our customer service representatives will contact you shortly.', 'bbtheme'); ?>
                        </div>

                    </form>
                </div>
            </div>

            <div id="success-popup" class="message-popup lity-hide">
                <?php echo $success_popup_content[$success_popup_content_key]['content']; ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if (!empty($fields['section_boxes']['boxes_block'])) : ?>
            <div class="boxes-grid-content">
                <div class="container m">
                    <h5><?php echo $fields['section_boxes']['title']; ?></h5>
                    <p class="description">
                        <?php echo $fields['section_boxes']['description']; ?>
                    </p>
                </div>
                <div class="container m box-items">
                    <?php foreach ($fields['section_boxes']['boxes_block'] as $box_item) : ?>
                        <div class="box-item">
                            <div class="container w">
                                <div class="box-icon"><img src="<?php echo $box_item['icon']['url']; ?>" alt=""></div>
                                <div class="box-text"><?php echo $box_item['text']; ?></div>
                            </div>
                            <div class="button-container">
                                <div class="box-button">
                                    <?php if (!empty($box_item['file'])) : ?>
                                        <a href="<?php echo $box_item['file']['url']; ?>" download><?php echo $fields['section_boxes']['download_button_text']; ?></a>
                                    <?php else : ?>
                                        <span><?php echo $fields['section_boxes']['coming_soon_text']; ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if(!empty($fields['video_section']['video_preview'])) : ?>
        <div class="video-section">
            <div class="container m">
                <div class="s_columns">
                    <div class="s_columns_left">
                        <h5><?php echo $fields['video_section']['title']; ?></h5>
                        <p><?php echo $fields['video_section']['description']; ?></p>
                    </div>
                    <div class="s_columns_right">
                        <?php echo wp_video_shortcode( [
                        'src'      => ($fields['video_section']['video_file']) ? $fields['video_section']['video_file']['url'] : '',
                        'poster'   => $fields['video_section']['video_preview']['url'],
                        'height'   => 320,
                        'width'    => 570,
                        ] ); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if(!empty($fields['tabs_section']['tabs'])) : ?>
            <div class="tabs-section">
                <div class="container m">
                    <h5><?php echo $fields['tabs_section']['title']; ?></h5>
                    <div class="tabs-wrapper">
                        <div class="tabs">
                            <?php foreach ($fields['tabs_section']['tabs'] as $tab) : ?>
                                <div class="tab">
                                    <input type="checkbox" id="<?php echo $tab['title']; ?>" >
                                    <label class="tab-label" for="<?php echo $tab['title']; ?>" ><?php echo $tab['title']; ?></label>
                                    <div class="tab-content">
                                        <?php foreach ($tab['content'] as $list) : ?>
                                            <div class="list">
                                                <h5><?php echo $list['list_label']; ?></h5>
                                                <ul>
                                                    <?php foreach ($list['list_items'] as $items) : ?>
                                                        <li><?php echo $items['item']; ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <?php if (!empty($fields['recommended_solutions'])) : ?>
            <div class="recommended-solutions-widget">
                <div class="container w n-p">
                    <div class="wrapper">
                        <div class="container m">
                            <h5><?php _e('Recommended Solutions', 'bbtheme'); ?></h5>
                            <div class="items cf">
                                <?php foreach ($fields['recommended_solutions'] as $solution) : ?>
                                    <?php
                                    $caption = get_field('caption', $solution->ID);
                                    $summary = get_field('summary', $solution->ID);
                                    $landing_image = get_field('landing_image', $solution->ID);
                                    $applications = get_field('ps_filter_application', $solution->ID);
                                    $app_filters = get_ps_filter_choices('application');
                                    ?>
                                    <div class="item">
                                        <a href="<?php echo get_permalink($solution); ?>" class="inner-wrapper">
                                            <div class="thumb-wrapper">
                                                <div class="img lozad" data-background-image="<?php echo $landing_image['url']; ?>"></div>
                                                <?php if (!empty($applications)) : ?>
                                                    <div class="cats cats-<?php echo count($applications) ?>>">
                                                        <?php foreach ($applications as $application) : ?>
                                                            <div class="<?php echo $application; ?>">
                                                                <div><?php echo $app_filters[$application]; ?></div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <h6><?php echo $caption; ?></h6>
                                            <p><?php echo $summary; ?></p>
                                        </a>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    </div>

<?php endwhile; ?>

<?php get_footer(); ?>
