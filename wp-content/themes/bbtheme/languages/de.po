msgid ""
msgstr ""
"Project-Id-Version: BBPress Theme\n"
"POT-Creation-Date: 2020-07-21 12:45+0530\n"
"PO-Revision-Date: 2020-07-21 12:26+0400\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.3.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.min.js\n"

#: 404.php:9
msgid "Page not found."
msgstr ""

#: 404.php:10
msgid "We are sorry, the page you requested could not be found."
msgstr ""

#: 404.php:11
msgid "Back to Homepage"
msgstr ""

#: functions.php:15 page-templates/market-detail-page.php:69
#: single-office.php:193 single-product.php:136 single-solution.php:137
msgid "Enquire Now"
msgstr "Jetzt anfragenn"

#: includes/filters.php:14 page-templates/sp-landing-page.php:61
msgid "Application"
msgstr ""

#: includes/filters.php:17
msgid "Maritime"
msgstr ""

#: includes/filters.php:18
msgid "Land"
msgstr ""

#: includes/filters.php:22 page-templates/sp-landing-page.php:99
msgid "Operator"
msgstr ""

#: includes/filters.php:25
msgid "GlobalSatar"
msgstr ""

#: includes/filters.php:26
msgid "Intesat"
msgstr ""

#: includes/filters.php:27
msgid "Yahsat"
msgstr ""

#: includes/filters.php:28
msgid "Telenor"
msgstr ""

#: includes/filters.php:29
msgid "Iridium"
msgstr ""

#: includes/filters.php:30
msgid "Inmarsat"
msgstr ""

#: includes/filters.php:31
msgid "Thuraya"
msgstr ""

#: includes/filters.php:35 page-templates/sp-landing-page.php:71
msgid "Set up"
msgstr ""

#: includes/filters.php:38
msgid "Fixed"
msgstr ""

#: includes/filters.php:39
msgid "Mobile"
msgstr ""

#: includes/filters.php:43 page-templates/sp-landing-page.php:80
msgid "Service"
msgstr ""

#: includes/filters.php:46
msgid "Data"
msgstr ""

#: includes/filters.php:47
msgid "Voice"
msgstr ""

#: includes/filters.php:48
msgid "Video"
msgstr ""

#: includes/filters.php:49
msgid "M2M"
msgstr ""

#: includes/filters.php:50
msgid "Email Only"
msgstr ""

#: includes/filters.php:54 page-templates/sp-landing-page.php:89
msgid "Type"
msgstr ""

#: includes/filters.php:57 search.php:48
msgid "Solution"
msgstr ""

#: includes/filters.php:58 search.php:45
msgid "Product"
msgstr ""

#: includes/filters.php:59
msgid "Accessory"
msgstr ""

#: includes/filters.php:63 page-templates/sp-landing-page.php:118
msgid "Speed"
msgstr ""

#: includes/filters.php:66
msgid "Low"
msgstr ""

#: includes/filters.php:67
msgid "Medium"
msgstr ""

#: includes/filters.php:68
msgid "High"
msgstr ""

#: includes/filters.php:72 page-templates/sp-landing-page.php:108
msgid "Market"
msgstr ""

#: includes/filters.php:75
msgid "Energy"
msgstr ""

#: includes/filters.php:76
msgid "Enterprise"
msgstr ""

#: includes/filters.php:77
msgid "Fishing"
msgstr ""

#: includes/filters.php:78
msgid "Government"
msgstr ""

#: includes/filters.php:79
msgid "Humanitarian"
msgstr ""

#: includes/filters.php:80
msgid "Leisure"
msgstr ""

#: includes/filters.php:81
msgid "Media"
msgstr ""

#: includes/filters.php:82
msgid "Offshore"
msgstr ""

#: includes/filters.php:83
msgid "Shipping"
msgstr ""

#: includes/filters.php:87
msgid "Region"
msgstr ""

#: includes/filters.php:90
msgid "Global"
msgstr ""

#: includes/filters.php:91
msgid "Africa"
msgstr ""

#: includes/filters.php:92
msgid "Asia Pacific"
msgstr ""

#: includes/filters.php:93
msgid "Central America"
msgstr ""

#: includes/filters.php:94
msgid "Central Asia"
msgstr ""

#: includes/filters.php:95
msgid "Europe"
msgstr ""

#: includes/filters.php:96
msgid "Middle East"
msgstr ""

#: includes/filters.php:97
msgid "Northern America"
msgstr ""

#: includes/filters.php:98
msgid "Oceania"
msgstr ""

#: includes/filters.php:99
msgid "South America"
msgstr ""

#: includes/header.php:19 page-templates/press-center-page.php:101
#: search.php:14 search.php:16
msgid "Search"
msgstr ""

#: page-templates/about-partners-page.php:88
msgid "Download brochure"
msgstr ""

#: page-templates/about-partners-page.php:94
msgid "View Coverage Map"
msgstr ""

#: page-templates/become-partner-page.php:26 page-templates/contact-page.php:27
#: page-templates/job-landing-page.php:34
#: page-templates/market-detail-page.php:25 single-office.php:59
msgid "Invalid method"
msgstr ""

#: page-templates/become-partner-page.php:27 page-templates/contact-page.php:28
#: page-templates/job-landing-page.php:35
#: page-templates/market-detail-page.php:26 single-office.php:60
msgid "Invalid data"
msgstr ""

#: page-templates/become-partner-page.php:28 page-templates/contact-page.php:29
#: page-templates/job-landing-page.php:36
#: page-templates/market-detail-page.php:27 single-office.php:61
msgid "Invalid email"
msgstr ""

#: page-templates/become-partner-page.php:29 page-templates/contact-page.php:30
#: page-templates/job-landing-page.php:37
#: page-templates/market-detail-page.php:28 single-office.php:62
msgid "Captcha not validated"
msgstr ""

#: page-templates/become-partner-page.php:84
#: page-templates/contact-page.php:121
msgid "Write us a message"
msgstr ""

#: page-templates/become-partner-page.php:91
#: page-templates/contact-page.php:125 page-templates/market-detail-page.php:79
#: single-office.php:204
msgid "Full Name..."
msgstr ""

#: page-templates/become-partner-page.php:96
#: page-templates/contact-page.php:128 page-templates/market-detail-page.php:82
#: single-office.php:207
msgid "Company..."
msgstr ""

#: page-templates/become-partner-page.php:101
#: page-templates/contact-page.php:131 page-templates/market-detail-page.php:85
#: single-office.php:210
msgid "Email..."
msgstr ""

#: page-templates/become-partner-page.php:109
#: page-templates/contact-page.php:137 page-templates/market-detail-page.php:90
#: single-office.php:215
msgid "Country..."
msgstr ""

#: page-templates/become-partner-page.php:122
#: page-templates/contact-page.php:149
#: page-templates/market-detail-page.php:102 single-office.php:227
msgid "Interest..."
msgstr ""

#: page-templates/become-partner-page.php:133
#: page-templates/contact-page.php:158
#: page-templates/market-detail-page.php:111 single-office.php:236
msgid "Message..."
msgstr ""

#: page-templates/become-partner-page.php:149
#: page-templates/contact-page.php:173 page-templates/job-landing-page.php:121
#: page-templates/market-detail-page.php:125 single-office.php:251
msgid "Send"
msgstr ""

#: page-templates/contact-page.php:75
msgid "Find the nearest IEC Telecom office"
msgstr ""

#: page-templates/job-landing-page.php:38
msgid "Please upload only PDF file."
msgstr ""

#: page-templates/job-landing-page.php:80
msgid "All locations"
msgstr ""

#: page-templates/job-landing-page.php:109
msgid "Apply Now"
msgstr ""

#: page-templates/job-landing-page.php:111
msgid "Full Name"
msgstr ""

#: page-templates/job-landing-page.php:114
msgid "Email"
msgstr ""

#: page-templates/job-landing-page.php:117
msgid "No file chosen"
msgstr ""

#: page-templates/job-landing-page.php:118
msgid "Upload Resume"
msgstr ""

#: page-templates/job-landing-page.php:148
msgid "While we do not have any vacancies at the moment, please visit us soon."
msgstr ""

#: page-templates/job-landing-page.php:149
msgid "New opportunities are updated on this page as they become available."
msgstr ""

#: page-templates/market-detail-page.php:73 single-office.php:197
msgid "Found some issues, Try again later."
msgstr ""

#: page-templates/market-detail-page.php:132 single-office.php:259
msgid "Thank You!"
msgstr ""

#: page-templates/market-detail-page.php:133 single-office.php:260
msgid ""
"Your inquiry has been well received. One of our customer service "
"representatives will contact you shortly."
msgstr ""

#: page-templates/market-detail-page.php:191
msgid "Recommended Products"
msgstr ""

#: page-templates/market-detail-page.php:230 single-product.php:150
#: single-solution.php:151
msgid "Recommended Solutions"
msgstr ""

#: page-templates/media-kit-page.php:55 page-templates/press-center-page.php:58
msgid "Press Contacts"
msgstr ""

#: page-templates/media-kit-page.php:81
msgid "Downloads"
msgstr ""

#: page-templates/media-kit-page.php:96 page-templates/media-kit-page.php:113
#: page-templates/press-center-page.php:123
msgid "Download"
msgstr ""

#: page-templates/news-landing-page.php:33
msgid "Search news"
msgstr ""

#: page-templates/news-landing-page.php:40
#: page-templates/press-center-page.php:87
msgid "All"
msgstr ""

#: page-templates/news-landing-page.php:69
#: page-templates/sp-landing-page.php:152
msgid "Load More"
msgstr ""

#: page-templates/news-landing-page.php:73
#: page-templates/sp-landing-page.php:156
msgid "No results found."
msgstr ""

#: page-templates/press-center-page.php:110
msgid "Press releases"
msgstr ""

#: page-templates/press-center-page.php:133
msgid "Files Included"
msgstr ""

#: page-templates/sp-landing-page.php:49
msgid "We will find a tailored solution for you"
msgstr ""

#: page-templates/sp-landing-page.php:67
msgid "By Specifications"
msgstr ""

#: page-templates/sp-landing-page.php:127
msgid "Coverage"
msgstr ""

#: search.php:8
msgid "Search results"
msgstr ""

#: search.php:36
msgid "Page"
msgstr ""

#: search.php:38
msgid "News"
msgstr ""

#: search.php:40
msgid "Press Release"
msgstr ""

#: search.php:42
msgid "Office"
msgstr ""

#: search.php:50
msgid "Job"
msgstr ""

#: search.php:68
msgid "Sorry, no results were found for this query."
msgstr ""

#: single-news.php:28 single-product.php:23 single-solution.php:23
msgid "Return Back"
msgstr ""

#: single-news.php:54
msgid "This news is also available in:"
msgstr ""

#: single-news.php:115
msgid "Featured News"
msgstr ""

#: single-office.php:353
msgid "Regional News"
msgstr ""

#: single-product.php:131 single-solution.php:132
msgid "Download Brochure"
msgstr ""

#. Theme Name of the plugin/theme
msgid "BBPress Theme"
msgstr ""

#. Description of the plugin/theme
msgid "Custom theme built by Blue Beetle"
msgstr ""

#. Author of the plugin/theme
msgid "Blue Beetle"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://www.bluebeetle.ae/"
msgstr ""
