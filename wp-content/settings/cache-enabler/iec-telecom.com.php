<?php
/**
 * Cache Enabler settings for https://iec-telecom.com/en/
 *
 * @since      1.5.0
 * @change     1.5.0
 *
 * @generated  19.04.2021 17:01:00
 */

return array (
  'version' => '1.6.2',
  'permalink_structure' => 'has_trailing_slash',
  'cache_expires' => 0,
  'cache_expiry_time' => 0,
  'clear_site_cache_on_saved_post' => 1,
  'clear_site_cache_on_saved_comment' => 0,
  'clear_site_cache_on_changed_plugin' => 0,
  'compress_cache' => 0,
  'convert_image_urls_to_webp' => 0,
  'excluded_post_ids' => '',
  'excluded_page_paths' => '',
  'excluded_query_strings' => '',
  'excluded_cookies' => '',
  'minify_html' => 1,
  'minify_inline_css_js' => 0,
);